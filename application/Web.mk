# Webファイル関連のMakefile

.PHONY : web
web : $(TARGET_WEB)

html.tar.gz : \
	$(WWW_DIR)/device_ctrl.cgi \
	$(WWW_DIR)/get_device_state.cgi \
	$(WWW_DIR)/bundle.js
	@tar -zcvf $@ $?

get_device_state.cgi : \
	$(SRC_ROOT)/Web/cgi/get_device_state.c \
	$(OBJDIR)/LosShmSem.o
	$(CC) -o $@ $?
	@chmod +x $@
	@mv $@ $(WWW_DIR)

device_ctrl.cgi : \
	$(SRC_ROOT)/Web/cgi/device_ctrl.c \
	$(OBJDIR)/LosShmSem.o \
	$(OBJDIR)/device_ctrl.o
	$(CC) -o $@ $?
	@chmod +x $@
	@mv $@ $(WWW_DIR)

$(OBJDIR)/%.o : $(PUG_DIR)/%.html
	$(OBJCOPY) -B arm -I binary -O elf32-littlearm $< $@
	@rm $<

$(PUG_DIR)/%.html : $(PUG_DIR)/%.pug
	npx pug $<

$(WWW_DIR)/bundle.js : $(JS_DIR)/device_ctrl.js
	npx webpack -o $@ $< -d source-map

