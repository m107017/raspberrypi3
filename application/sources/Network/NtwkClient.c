//*******************************************************************************
//
// ファイル名   ：NtwkClient.c
// 説明         ：ネットワーク通信 サーバー処理関連
//
//*******************************************************************************

#include <stdint.h>

#include <stdio.h>                  // snprintf
#include <unistd.h>                 // close
#include <string.h>                 // memset/strlen

#include <sys/types.h>              // socket/sendto
#include <sys/socket.h>             // socket/sendto/inet_addr
#include <arpa/inet.h>              // htons/inet_addr
#include <netinet/in.h>             // inet_addr

#include "NtwkClient.h"


//==============================================================================
//  ローカル変数
//==============================================================================

//==============================================================================
//  ローカル関数
//==============================================================================

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：ntwk_SendUdpF
// 説明         ：UDP送信
//*******************************************************************************

bool ntwk_SendUdpF(                                 // 戻り値：true=処理成功、false=処理失敗
    char        *p_dst_addr,                        // 送信先IPアドレス（xx.xx.xx.xx形式）
    uint16_t    port_num,                           // ポート番号
    char        *p_msg,                             // 送信メッセージ
    size_t      msg_len                             // 送信メッセージ長（最大1024文字）
)
{
    int                 sock ;
    struct sockaddr_in  dest_addr;

    // ソケット作成
    sock = socket( AF_INET, SOCK_DGRAM, 0 ) ;
    if( sock < 0 )
    {
        return false ;
    }

    // 送信先指定
    dest_addr.sin_family = AF_INET ;
    dest_addr.sin_port = htons( port_num ) ;
    dest_addr.sin_addr.s_addr = inet_addr( p_dst_addr ) ;

    // 送信
    sendto( sock, p_msg, msg_len, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr) ) ;

    // ソケットを閉じる
    close( sock ) ;

    return true ;
}

//*******************************************************************************
// 関数名       ：ntwk_SendSyslogF
// 説明         ：syslog送信
//*******************************************************************************

bool ntwk_SendSyslogF(                              // 戻り値：true=処理成功、false=処理失敗
    int         priority,                           // プライオリティ指定
    int         facility,                           // ファシリティ指定
    char        *p_msg                              // 送信メッセージ
)
{
    char        send_msg[ 128 ] ;
    int         log_type ;

    log_type = ( priority | facility ) ;

    // メッセージの作成
    memset( send_msg, 0, sizeof(send_msg) ) ;
    snprintf( send_msg, 128, "<%d>RaspberryPi3:%s", log_type, p_msg ) ;

    // syslog送信
    if( !ntwk_SendUdpF( NTWK_DST_IP_ADDR, NTWK_SEND_PORT_SLOG, send_msg, strlen( send_msg ) ) )
    {
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：ntwk_SendTcpFromServerF
// 説明         ：サーバーからTCP送信
//*******************************************************************************

void ntwk_SendTcpFromServerF(                       // 戻り値：true=処理成功、false=処理失敗
    int         sock_connect,                       // クライアントと接続しているソケットのディスクリプタ
    char        *p_msg                              // 送信メッセージ
)
{
    send( sock_connect, p_msg, strlen( p_msg ), 0 ) ;
}
