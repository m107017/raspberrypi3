//*******************************************************************************
//
// ファイル名   ：NtwkServer.c
// 説明         ：ネットワーク通信 サーバー処理関連
//
//*******************************************************************************

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>                 // malloc/free
#include <string.h>                 // memset/strstr
#include <unistd.h>                 // close
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>

#include "NtwkServer.h"
#include "../LinuxOs/LosCtrlFd.h"
#include "../Command/CmndDevCtrl.h"

//==============================================================================
//  ローカル変数
//==============================================================================

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：CreateSockTcpF
// 説明         ：TCPソケット生成
//*******************************************************************************

static int CreateSockTcpF(                      // 戻り値：ソケット通信用ファイルディスクリプタ
    uint16_t        port_num,                   // ポート番号
    uint8_t         max_connect                 // 最大接続数
)
{
    struct sockaddr_in  addr ;
    int                 sock ;

    // ソケットの生成
    sock = socket( AF_INET, SOCK_STREAM, 0 ) ;
    if( sock == -1 )
    {
        return -1 ;
    }

    // ソケットの設定
    addr.sin_family = AF_INET ;
    addr.sin_port = htons( port_num ) ;
    addr.sin_addr.s_addr = INADDR_ANY ;

    // 待ち受けポート指定
    bind( sock, (struct sockaddr *)&addr, sizeof(addr) ) ;

    // 待ち受け開始
    listen( sock, max_connect ) ;

    return sock ;
}

//*******************************************************************************
// 関数名       ：InitRecvSockF
// 説明         ：パケット受信用ソケット初期化
//*******************************************************************************

static void InitRecvSockF(
    int             *p_sock_recv,               // パケット受信待ちソケットのディスクリプタ格納ポインタ
    uint8_t         max_connect                 // 最大接続数
)
{
    uint8_t     cnt ;

    for( cnt = 0; cnt < max_connect; cnt++ )
    {
        *p_sock_recv = -1 ;
        p_sock_recv++ ;
    }
}

//*******************************************************************************
// 関数名       ：ConnectClientF
// 説明         ：クライアントとの接続確立
//*******************************************************************************

static void ConnectClientF(
    int             sock_connect,               // 接続要求待ち受け用ソケットのディスクリプタ
    int             *p_sock_recv,               // パケット受信待ちソケットのディスクリプタ格納ポインタ
    uint8_t         max_connect                 // 最大接続数
)
{
    uint8_t     cnt ;

    for( cnt = 0; cnt < max_connect; cnt++ )
    {
        if( *p_sock_recv == -1 )                // まだクライアントとの接続が確立していない？
        {
            struct sockaddr_in client_addr ;
            int     len ;

            // 接続実行
            len = sizeof(client_addr) ;
            *p_sock_recv = accept( sock_connect, (struct sockaddr *)&client_addr, &len ) ;
            printf( "[Sock %d]Connected\n", cnt ) ;

            break ;
        }
        p_sock_recv++ ;                         // 次のインデックスへ
    }
}

//*******************************************************************************
// 関数名       ：RecieveDataF
// 説明         ：クライアントからのデータ受信（文字列コマンド形式）
//*******************************************************************************

static void RecieveDataF(
    int             *p_sock,                    // 受信するソケットのディスクリプタ
    bool            (*p_call_back)( CMND_NTWK_INFO* ),  // データ処理コールバック関数ポインタ
    CMND_NTWK_INFO  *p_arg                      // データ処理コールバック関数の引数
)
{
    char        *p_recv_work ;
    int         recv_cnt ;

    // データ受信用ワークを動的確保
    p_recv_work = malloc( p_arg->CmdBufSize ) ;
    memset( p_recv_work, 0, p_arg->CmdBufSize ) ;

    recv_cnt = recv( *p_sock, p_recv_work, sizeof(p_arg->CmdBufSize), 0 );
    if( recv_cnt > 0 )          // データを受信した？
    {
        // これまで受信したデータと文字列連結する
        strncat( p_arg->pCmdBuf, p_recv_work, p_arg->CmdBufSize ) ;
        printf( "Recieve: %s\n", p_recv_work ) ;

        if( strstr( p_arg->pCmdBuf, p_arg->pTerminate ) != NULL )       // 受信したデータにコマンド終端がある？
        {
            printf( "Command: %s\n", p_arg->pCmdBuf ) ;
            if( p_call_back != NULL )                                   // コールバック関数が指定されている？
            {
                p_call_back( p_arg ) ;
            }
            memset( p_arg->pCmdBuf, 0, sizeof(p_arg->CmdBufSize) ) ;    // コマンド文字列格納バッファ初期化
        }
    }
    else                        // クライアントから切断された、またはエラー？
    {
        printf( "Disconnected\n" ) ;
        close( *p_sock ) ;
        *p_sock = -1 ;
    }

    free( p_recv_work ) ;                       // ワークを解放
}

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：ntwk_ServerDevCtrlF
// 説明         ：デバイス制御用サーバー処理
//*******************************************************************************

void *ntwk_ServerDevCtrlF(
    void        *arg
)
{
    CMND_NTWK_INFO  network_data ;
    char            cmd_buf[ CMND_DEVCTRL_MAX_LEN ] ;
    int             sock_connect ;
    int             sock_recv[ NTWK_DEVCTRL_MAX_CONNECT ] ;
    bool            is_continue ;
    uint8_t         cnt ;

    // コマンド処理関連初期化
    memset( cmd_buf, 0, sizeof(cmd_buf) ) ;
    network_data.pCmdBuf = cmd_buf ;
    network_data.CmdBufSize = sizeof(cmd_buf) ;
    network_data.pDelimiter = CMND_DEVCTRL_DELIM ;
    network_data.pTerminate = CMND_DEVCTRL_TERM ;
    if( !cmnd_DevCtrlInitF( ) )
    {
        return NULL ;
    }

    // パケット受信用ソケット初期化
    InitRecvSockF( sock_recv, NTWK_DEVCTRL_MAX_CONNECT ) ;

    // TCPソケット生成
    sock_connect = CreateSockTcpF( NTWK_DEVCTRL_PORT_NUM, NTWK_DEVCTRL_MAX_CONNECT ) ;
    if( sock_connect == -1 )
    {
        return NULL ;
    }

    // サーバー処理ループ
    printf( "Device Control Server Start.\n" ) ;
    is_continue = true ;
    while( is_continue )
    {
        fd_set      readfds ;
        int         max_fd ;
        int         fd_cnt ;

        // selectで監視するファイルディスクリプタの登録
        // （selectを実行するたびに書き換えられるため、毎回初期化する）
        max_fd = 0 ;
        FD_ZERO( &readfds ) ;
        los_FdSetF( sock_connect, &max_fd, &readfds ) ;
        for( cnt = 0; cnt < NTWK_DEVCTRL_MAX_CONNECT; cnt++ )
        {
            los_FdSetF( sock_recv[ cnt ], &max_fd, &readfds ) ;
        }

        // ファイルディスクリプタ読み出し待ち
        fd_cnt = select( max_fd + 1, &readfds, NULL, NULL, NULL ) ;

        if( fd_cnt > 0 )
        {
            // 接続要求待ち受け用ソケットへの接続チェック
            if( los_FdTestSetF( sock_connect, &readfds ) )
            {
                // クライアントからの接続を確立する
                ConnectClientF( sock_connect, sock_recv, NTWK_DEVCTRL_MAX_CONNECT ) ;
            }

            // パケット受信待ちソケットへの受信チェック
            for( cnt = 0; cnt < NTWK_DEVCTRL_MAX_CONNECT; cnt++ )
            {
                if( los_FdTestSetF( sock_recv[ cnt ], &readfds ) )
                {
                    // コマンド処理用ネットワーク関連情報 受信ソケット設定
                    network_data.SockRecv = sock_recv[ cnt ] ;

                    // データ受信
                    printf( "[Sock %d]", cnt ) ;
                    RecieveDataF( &sock_recv[ cnt ], cmnd_DevCtrlMainF, &network_data ) ;
                }
            }
        }
        else
        {
            if( fd_cnt < 0 )                    // エラーならサーバー処理終了
            {
                is_continue = false ;
            }
        }
    }

    // パケット受信用ソケットクローズ
    los_CloseFdF( sock_recv, NTWK_DEVCTRL_MAX_CONNECT ) ;

    // 接続要求待ち受け用ソケットクローズ
    close( sock_connect ) ;

    printf( "Device Control Server Stop.\n" ) ;

    return NULL ;
}
