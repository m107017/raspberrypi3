//*******************************************************************************
//
// ファイル名   ：NtwkClient.h
// 説明         ：ネットワーク通信 サーバー処理関連
//
//*******************************************************************************

#ifndef     NTWK_CLIENT_H_INCLUDED
#define     NTWK_CLIENT_H_INCLUDED

#include <stdio.h>                  // size_t
#include <stdint.h>
#include <stdbool.h>

//==============================================================================
//  定数
//==============================================================================

#define     NTWK_DST_IP_ADDR            "192.168.1.2"           // 送信先IPアドレス
#define     NTWK_SEND_PORT_SLOG         514                     // syslog送信ポート番号

//==============================================================================
//  グローバル関数
//==============================================================================

bool ntwk_SendUdpF(                                 // 戻り値：true=処理成功、false=処理失敗
    char        *p_dst_addr,                        // 送信先IPアドレス（xx.xx.xx.xx形式）
    uint16_t    port_num,                           // ポート番号
    char        *p_msg,                             // 送信メッセージ
    size_t      msg_len                             // 送信メッセージ長（最大1024文字）
) ;

bool ntwk_SendSyslogF(                              // 戻り値：true=処理成功、false=処理失敗
    int         priority,                           // プライオリティ指定
    int         facility,                           // ファシリティ指定
    char        *p_msg                              // 送信メッセージ
) ;

void ntwk_SendTcpFromServerF(                       // 戻り値：true=処理成功、false=処理失敗
    int         sock_connect,                       // クライアントと接続しているソケットのディスクリプタ
    char        *p_msg                              // 送信メッセージ
) ;

#endif          // NTWK_CLIENT_H_INCLUDED
