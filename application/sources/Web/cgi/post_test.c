//******************************************************************************/
//
// ファイル名   ：post_test.c
// 説明         ：post_test.cgi処理
//
//******************************************************************************/

#include <stdio.h>              // printf/fgets
#include <stdlib.h>             // EXIT_***/getenv/atoi

#define READ_BUF_SIZE           1024            // 受信バッファサイズ

//==============================================================================
//  ローカル変数
//==============================================================================

//==============================================================================
//  ローカル関数
//==============================================================================

//==============================================================================
//  main関数
//==============================================================================

//*******************************************************************************
// 関数名       ：main
// 説明         ：メイン処理
//*******************************************************************************
int main( int argc, char **argv )
{
    char            *p_contents_len ;
    unsigned long   contents_len ;
    char            rbuf[ READ_BUF_SIZE ] ;

    printf( "Content-Type: text/html;charset=utf-8\n\n" ) ;
    printf( "<html><title>Test C-Language CGI Post</title><body>\n" ) ;
    printf( "<h1>Test C-Language CGI Post</h1>\n" ) ;

    // POSTリクエストの文字列は、環境変数CONTENT_LENGTHに格納される
    p_contents_len = getenv( "CONTENT_LENGTH" ) ;
    contents_len = atoi( p_contents_len ) ;

    // CONTENT_LENGTHを取得できた、または受信サイズが受信バッファサイズ以下なら処理継続
    if( ( p_contents_len != NULL ) || ( contents_len < READ_BUF_SIZE ) )
    {
        printf( "<p>\n" ) ;
        printf( "CONTENT_LENGTH = %s<br />\n", p_contents_len ) ;
        printf( "POST MESSAGE : <br />\n" ) ;
        while( fgets( rbuf, READ_BUF_SIZE, stdin ) )
        {
            printf( "%s\n", rbuf ) ;
        }
        printf( "</p>\n" ) ;
    }

    printf("</body></html>\n");

    return EXIT_SUCCESS ;
}
