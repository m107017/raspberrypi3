//******************************************************************************/
//
// ファイル名   ：device_ctrl.c
// 説明         ：device_ctrl.cgi処理
//
//******************************************************************************/

#include <stdint.h>
#include <stdio.h>              // perror/fgets
#include <stdlib.h>             // EXIT_***/getenv/atoi
#include <string.h>             // strncpy/strcmp/memset

#include "../../Device/DevLed.h"
#include "../../LinuxOs/LosShmSem.h"

#define READ_BUF_SIZE           1024            // 受信バッファサイズ
#define POST_NAME_BUF_SIZE      50              // nameの値格納バッファサイズ
#define POST_VAL_BUF_SIZE       50              // valueの値格納バッファサイズ

// POST Query値情報構造体
typedef struct
{
    char        Name[ POST_NAME_BUF_SIZE ] ;    // nameの値格納
    char        Value[ POST_VAL_BUF_SIZE ] ;    // valueの値格納

}   POST_QUERY ;

// POST処理情報構造体
typedef struct
{
    char        *pName ;
    bool        ( *pfProc )( char* ) ;

}   POST_PROC ;

//==============================================================================
//  objcopy埋め込み変数
//==============================================================================
extern char     _binary_sources_Web_pug_device_ctrl_html_start[ ] ;
extern char     _binary_sources_Web_pug_device_ctrl_html_end[ ] ;
extern char     _binary_sources_Web_pug_device_ctrl_html_size[ ] ;

//==============================================================================
//  プロトタイプ宣言
//==============================================================================

static bool PostExeLed1F( char *value ) ;
static bool PostExeLed2F( char *value ) ;

//==============================================================================
//  ローカル変数
//==============================================================================

static LOS_APL_PARAM    *pAplParam ;                            // 共有メモリアドレス
static const POST_PROC  PostProc[ ] =
{
    { "led1"            , PostExeLed1F           },
    { "led2"            , PostExeLed2F           },
    { NULL              , NULL                   }              // 番兵
} ;
static const char       ValueOn[ ] = "on" ;
static const char       ValueOff[ ] = "off" ;

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：PostExeLed1F
// 説明         ：HTTP POSTメッセージ処理（LED1）
//*******************************************************************************

static bool PostExeLed1F(                   // 戻り値：処理成功=true、処理失敗=false
    char            *value                  // valueの値
)
{
    bool        is_success ;

    is_success = true ;

    if( strcmp( value, ValueOn ) == 0 )
    {
        pAplParam->DevParam.Led1On = true ;
    }
    else if( strcmp( value, ValueOff ) == 0 )
    {
        pAplParam->DevParam.Led1On = false ;
    }
    else
    {
        is_success = false ;
    }

    return is_success ;
}

//*******************************************************************************
// 関数名       ：PostExeLed2F
// 説明         ：HTTP POSTメッセージ処理（LED2）
//*******************************************************************************

static bool PostExeLed2F(                   // 戻り値：処理成功=true、処理失敗=false
    char            *value                  // valueの値
)
{
    bool        is_success ;

    is_success = true ;

    if( strcmp( value, ValueOn ) == 0 )
    {
        pAplParam->DevParam.Led2On = true ;
    }
    else if( strcmp( value, ValueOff ) == 0 )
    {
        pAplParam->DevParam.Led2On = false ;
    }
    else
    {
        is_success = false ;
    }

    return is_success ;
}

//*******************************************************************************
// 関数名       ：HttpPostExecuteF
// 説明         ：HTTP POSTメッセージ処理
//*******************************************************************************

static bool HttpPostExecuteF(               // 戻り値：処理成功=true、処理失敗=false
    POST_QUERY      *p_post_query           // POST Query値情報構造体
)
{
    const POST_PROC     *p_post_proc ;
    bool                is_success ;

    p_post_proc = PostProc ;
    is_success = false ;

    while( p_post_proc->pName != NULL )                         // 番兵が来るまでループ
    {
        // nameの値が一致するなら、nameに応じた処理に飛ぶ
        if( strcmp( p_post_query->Name, p_post_proc->pName ) == 0 )
        {
            is_success = p_post_proc->pfProc( p_post_query->Value ) ;
        }
        p_post_proc++ ;                                         // 一致しない場合は次のnameへ
    }

    return is_success ;
}

//*******************************************************************************
// 関数名       ：HttpPostAnalysisF
// 説明         ：HTTP POSTメッセージ解析・処理
//*******************************************************************************

static bool HttpPostAnalysisF(              // 戻り値：処理成功=true、処理失敗=false
    char            *p_msg                  // POSTメッセージ
)
{
    POST_QUERY  post_query ;
    char        *p_next ;

    // POSTメッセージはname=value&name=value&・・・と&で区切られる

    while( 1 )
    {
        // "="の位置を取得
        p_next = strstr( p_msg, "=" ) ;
        if( p_next == NULL )                                // "="がないなら終了
        {
            break ;
        }

        // nameの文字列サイズがバッファサイズより大きい場合はエラー
        if( p_next - p_msg > POST_NAME_BUF_SIZE )
        {
            perror( "name string size error." ) ;
            return false ;
        }

        // name値を取得
        memset( post_query.Name, 0, sizeof(post_query.Name) ) ;
        strncpy( post_query.Name, p_msg, p_next - p_msg ) ;

        // メッセージ先頭を=の次に移動
        p_msg = p_next + 1 ;

        // "&"の位置を取得
        p_next = strstr( p_msg, "&" ) ;
        if( p_next == NULL )                                // "&"がない？
        {
            // fgets関数で読み込んでいるため、終端はNUL
            // 文字列先頭から文字数分ポインタを移動させる
            p_next = p_msg + strlen( p_msg ) ;
        }

        // valueの文字列サイズがバッファサイズより大きい場合はエラー
        if( p_next - p_msg > POST_VAL_BUF_SIZE )
        {
            perror( "value string size error." ) ;
            return false ;
        }

        // value値を取得
        memset( post_query.Value, 0, sizeof(post_query.Value) ) ;
        strncpy( post_query.Value, p_msg, p_next - p_msg ) ;

        // メッセージ先頭を&の次に移動
        p_msg = p_next + 1 ;

        // 各POSTメッセージ処理
        if( !HttpPostExecuteF( &post_query ) )
        {
            return false ;
        }
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：DispEndPageF
// 説明         ：終了時ページ表示
//*******************************************************************************

static void DispEndPageF(
    bool            success             // 処理成功か否か
)
{
    printf( "Content-Type: text/html;charset=utf-8\n\n" ) ;

    if( success )
    {
        printf( "%s", _binary_sources_Web_pug_device_ctrl_html_start ) ;
    }
    else
    {
        printf( "<!DOCTYPE html>" ) ;
        printf( "<html><head><title>Error Page</title></head><body>\n" ) ;
        printf( "<h1>500 - Internal Server Error</h1>\n" ) ;
        printf( "</body></html>\n" ) ;
    }
}

//==============================================================================
//  main関数
//==============================================================================

//*******************************************************************************
// 関数名       ：main
// 説明         ：メイン処理
//*******************************************************************************
int main( int argc, char **argv )
{
    char            *p_req_method ;

    // 共有メモリアドレス取得
    pAplParam = los_InitSharedMemF( false, false ) ;
    if( pAplParam == (LOS_APL_PARAM *)-1 )
    {
        DispEndPageF( false ) ;
        return EXIT_FAILURE ;
    }

    // メソッド取得
    p_req_method = getenv( "REQUEST_METHOD" ) ;
    if( strcmp( p_req_method, "POST" ) == 0 )          // POSTメソッド
    {
        char            *p_contents_len ;
        unsigned long   contents_len ;
        char            rbuf[ READ_BUF_SIZE ] ;
        bool            is_success ;

        // POSTリクエストの文字列は、環境変数CONTENT_LENGTHに格納される
        p_contents_len = getenv( "CONTENT_LENGTH" ) ;
        contents_len = atoi( p_contents_len ) ;
        is_success = false ;

        // CONTENT_LENGTHを取得できた、または受信サイズが受信バッファサイズ以下なら処理継続
        if( ( p_contents_len != NULL ) || ( contents_len < READ_BUF_SIZE ) )
        {
            is_success = true ;
            while( fgets( rbuf, READ_BUF_SIZE, stdin ) && is_success )
            {
                is_success = HttpPostAnalysisF( rbuf ) ;
            }
        }

        // 終了時ページ表示
        DispEndPageF( is_success ) ;
    }
    else if( strcmp( p_req_method, "GET" ) == 0 )           // GETメソッド
    {
        DispEndPageF( true ) ;                              // ページを表示
    }
    else                                                    // POST/GET以外の文字列または取得できない
    {
        DispEndPageF( false ) ;                             // エラーページを表示
    }

    return EXIT_SUCCESS ;
}
