//******************************************************************************/
//
// ファイル名   ：get_device_state.c
// 説明         ：get_device_state.cgi処理
//
//******************************************************************************/

#include <stdint.h>
#include <stdio.h>              // printf
#include <stdlib.h>             // EXIT_***

#include "../../LinuxOs/LosShmSem.h"

#define OBJ_SW_ON               1           // スイッチON
#define OBJ_SW_OFF              0           // スイッチOFF

//==============================================================================
//  ローカル変数
//==============================================================================

static LOS_APL_PARAM    *pAplParam ;                            // 共有メモリアドレス

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：SendJsonObjectSwitchF
// 説明         ：JSON switchオブジェクト送信
//*******************************************************************************

static void SendJsonObjectSwitchF(
    void
)
{
    uint8_t     sw_state ;

    printf( "\"switch\":" ) ;                       // オブジェクト名
    printf( "[" ) ;                                 // 配列開始

    // スイッチ情報
    sw_state = ( pAplParam->DevParam.SwOn ) ? OBJ_SW_ON : OBJ_SW_OFF ;
    printf( "%d", sw_state ) ;

    printf( "]" ) ;                                 // 配列終了
}

//*******************************************************************************
// 関数名       ：SendJsonObjectTopF
// 説明         ：JSON最上位オブジェクト送信
//*******************************************************************************

static void SendJsonTopObjectF(
    void
)
{
    printf( "{" ) ;
    SendJsonObjectSwitchF( ) ;
    printf( "}" ) ;
}

//*******************************************************************************
// 関数名       ：DispEndPageF
// 説明         ：終了時ページ表示
//*******************************************************************************

static void DispEndPageF(
    void
)
{
    printf( "Content-Type: text/html;charset=utf-8\n\n" ) ;
    printf( "<html><title>Send Device State</title><body>\n" ) ;
    printf( "<h1>Send Device State: Error</h1>\n" ) ;
    printf( "</body></html>\n" ) ;
}

//==============================================================================
//  main関数
//==============================================================================

//*******************************************************************************
// 関数名       ：main
// 説明         ：メイン処理
//*******************************************************************************
int main( int argc, char **argv )
{
    // 共有メモリアドレス取得
    pAplParam = los_InitSharedMemF( false, false ) ;
    if( pAplParam == (LOS_APL_PARAM *)-1 )
    {
        DispEndPageF( ) ;
        return EXIT_FAILURE ;
    }

    // JSONデータ送信
    printf( "Content-Type: application/json; charset=utf-8\n\n" ) ;
    SendJsonTopObjectF( ) ;

    return EXIT_SUCCESS ;
}
