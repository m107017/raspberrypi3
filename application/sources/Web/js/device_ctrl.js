//*******************************************************************************
//
// ファイル名   ：device_ctrl.js
// 説明         ：デバイス制御関連JavaScript
//
//*******************************************************************************

var $ = require( "jquery" ) ;
const DEVCTRL_AJAX_INTRVL_MS = 1000 ;

$( function(){

    // ボタンが押されたときの処理
    $( "button" ).click( function(){
        var ledName = $( this ).attr( "name" ) ;            // クリックされたボタンのnameの値を取得
        ledName = ledName.toUpperCase( ) ;                  // 英小文字から英大文字に変換

		var onOff = $( this ).val( ) ;                      // クリックされたボタンのvalueの値を取得
        onOff = onOff.toUpperCase( ) ;                      // 英小文字から英大文字に変換

        alert( ledName + "を" + onOff + "します。" ) ;
    } ) ;

    // Ajaxデータ通信
    setInterval( function(){
        $.ajax( {
            url: "get_device_state.cgi",
            type: "GET",
            dataType: "json"
        } )
        .done( function( data ){
            ajaxSuccess( data ) ;
        } )
        .fail( function( ){
            ajaxError( ) ;
        } )
    }, DEVCTRL_AJAX_INTRVL_MS ) ;

    // Ajax通信（成功時）
    function ajaxSuccess( data ){
        // SWITCH1の状態更新
        switch( data.switch[ 0 ] )
        {
            case 0 :                            // スイッチOFF
                $( "#sw1" ).text( "OFF" ) ;
                break ;

            case 1 :                            // スイッチON
                $( "#sw1" ).text( "ON" ) ;
                break ;

            default :
                $( "#sw1" ).text( "取得エラー" ) ;
                break ;
        }

    } ;

    // Ajax通信（失敗時）
    function ajaxError( ){
        $( "#sw1" ).text( "取得エラー" ) ;
    } ;

} ) ;
