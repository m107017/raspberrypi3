//*******************************************************************************
//
// ファイル名   ：DevAdc.h
// 説明         ：A/D変換関連
//
//*******************************************************************************

#ifndef     DEV_ADC_H_INCLUDED
#define     DEV_ADC_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

//==============================================================================
//  定数定義
//==============================================================================

#define     DEV_ADS1015_CONV_REG_ID             0x00            // ADS1015 Conversion RegisterのID
#define     DEV_ADS1015_CONF_REG_ID             0x01            // ADS1015 Config RegisterのID
#define     DEV_ADS1015_CONF_H_OS_BIT           ( 15 - 8 )      // ADS1015 Config RegisterのOSの上位Byte内ビット番号
#define     DEV_ADS1015_CONF_H_MUX_BIT          ( 12 - 8 )      // ADS1015 Config RegisterのMUXの上位Byte内ビット番号
#define     DEV_ADS1015_CONF_H_PGA_BIT          ( 9 - 8 )       // ADS1015 Config RegisterのPGAの上位Byte内ビット番号
#define     DEV_ADS1015_CONF_H_MODE_BIT         ( 8 - 8 )       // ADS1015 Config RegisterのMODEの上位Byte内ビット番号
#define     DEV_ADS1015_CONF_L_DR_BIT           5               // ADS1015 Config RegisterのDRの上位Byte内ビット番号
#define     DEV_ADS1015_CONF_OS_ADC_START       0x01            // ADS1015 A/D変換開始
#define     DEV_ADS1015_CONF_COMP_DEF           0x03            // ADS1015 Config RegisterのCOMP_xxのデフォルト値
#define     DEV_ADS1015_RES_BITS                12              // ADS1015 A/D変換分解能[bits]
#define     DEV_ADS1015_AD_MAX                  ( ( (int16_t)0x01 << ( DEV_ADS1015_RES_BITS - 1 ) ) - 1 )
                                                                // ADS1015 A/D最大値
#define     DEV_ADS1015_CONV_OFS_BIT            4               // ADS1015 Conversion RegisterのA/D値オフセット[bits]

#define     DEV_ADS1015_CONF_PGA_6144           0x00            // ADS1015 PGA=±6.144
#define     DEV_ADS1015_CONF_PGA_4096           0x01            // ADS1015 PGA=±4.096
#define     DEV_ADS1015_CONF_PGA_2048           0x02            // ADS1015 PGA=±2.048
#define     DEV_ADS1015_CONF_PGA_1024           0x03            // ADS1015 PGA=±1.024
#define     DEV_ADS1015_CONF_PGA_512            0x04            // ADS1015 PGA=±0.512
#define     DEV_ADS1015_CONF_PGA_256            0x05            // ADS1015 PGA=±0.256

#define     DEV_ADS1015_CONF_DR_128             0x00            // ADS1015 DR=128SPS(7813us)
#define     DEV_ADS1015_CONF_DR_250             0x01            // ADS1015 DR=250SPS(4000us)
#define     DEV_ADS1015_CONF_DR_490             0x02            // ADS1015 DR=490SPS(2041us)
#define     DEV_ADS1015_CONF_DR_920             0x03            // ADS1015 DR=920SPS(1087us)
#define     DEV_ADS1015_CONF_DR_1600            0x04            // ADS1015 DR=1600SPS(625us)
#define     DEV_ADS1015_CONF_DR_2400            0x05            // ADS1015 DR=2400SPS(416us)
#define     DEV_ADS1015_CONF_DR_3300            0x06            // ADS1015 DR=3300SPS(303us)

//==============================================================================
//  構造体定義
//==============================================================================

// ADS1015 Config Register設定構造体
typedef struct
{
    uint8_t         Mux ;               // 変換チャネル指定
    uint8_t         Pga ;               // フルスケール指定
    uint8_t         Mode ;              // 変換モード
    uint8_t         Dr ;                // 変換速度

}   DEV_I2C_ADS1015_SETUP ;

//==============================================================================
//  グローバル関数
//==============================================================================

bool dev_StartAdcAds1015F(                      // 戻り値：true=処理成功、false=処理失敗
    DEV_I2C_ADS1015_SETUP   *p_setup            // ADS1015変換設定
) ;

bool dev_ReadAdcAds1015F(                       // 戻り値：true=処理成功、false=処理失敗
    int16_t        *p_rdat                      // 読み出しデータ格納先
) ;

#endif          // DEV_ADC_H_INCLUDED