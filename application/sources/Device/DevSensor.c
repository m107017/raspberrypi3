//*******************************************************************************
//
// ファイル名   ：DevSensor.c
// 説明         ：センサ制御関連
//
//*******************************************************************************

#include "DevSensor.h"
#include "DevAdc.h"
#include "DevConfI2c.h"
#include "../Cpu/CpuI2c.h"
#include "../Network/NtwkClient.h"

#include <stdint.h>
#include <stdbool.h>

#include <stdio.h>                  // snprintf/perror
#include <unistd.h>                 // usleep
#include <string.h>                 // memset
#include <syslog.h>

//==============================================================================
//  定数定義（LM61）
//==============================================================================

// LM61でのフルスケール値[mV]
#define     DEV_ADS1015_CONF_PGA_LM61           DEV_ADS1015_CONF_PGA_2048
#if ( DEV_ADS1015_CONF_PGA_LM61 == DEV_ADS1015_CONF_PGA_6144 )
    #define     DEV_ADS1015_PGA_MV_LM61         6144
#elif ( DEV_ADS1015_CONF_PGA_LM61 == DEV_ADS1015_CONF_PGA_4096 )
    #define     DEV_ADS1015_PGA_MV_LM61         4096
#elif ( DEV_ADS1015_CONF_PGA_LM61 == DEV_ADS1015_CONF_PGA_2048 )
    #define     DEV_ADS1015_PGA_MV_LM61         2048
#elif ( DEV_ADS1015_CONF_PGA_LM61 == DEV_ADS1015_CONF_PGA_1024 )
    #define     DEV_ADS1015_PGA_MV_LM61         1024
#elif ( DEV_ADS1015_CONF_PGA_LM61 == DEV_ADS1015_CONF_PGA_512 )
    #define     DEV_ADS1015_PGA_MV_LM61         512
#elif ( DEV_ADS1015_CONF_PGA_LM61 == DEV_ADS1015_CONF_PGA_256 )
    #define     DEV_ADS1015_PGA_MV_LM61         256
#else
    #error
#endif

// LM61でのA/D変換完了待ち時間[us]
#define     DEV_ADS1015_CONF_DR_LM61            DEV_ADS1015_CONF_DR_1600
#if ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_128 )
    #define     DEV_ADS1015_DR_US_LM61          7813
#elif ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_250 )
    #define     DEV_ADS1015_DR_US_LM61          4000
#elif ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_490 )
    #define     DEV_ADS1015_DR_US_LM61          2041
#elif ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_920 )
    #define     DEV_ADS1015_DR_US_LM61          1087
#elif ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_1600 )
    #define     DEV_ADS1015_DR_US_LM61          625
#elif ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_2400 )
    #define     DEV_ADS1015_DR_US_LM61          416
#elif ( DEV_ADS1015_CONF_DR_LM61 == DEV_ADS1015_CONF_DR_3300 )
    #define     DEV_ADS1015_DR_US_LM61          303
#else
    #error
#endif

#define     DEV_LM61_PROP                       10              // LM61 ℃→mV変換比例係数[mV/℃]
#define     DEV_LM61_INTERCEPT                  600             // LM61 ℃→mV変換切片[mV]
#define     DEV_LM61_MV_TO_AD( MV )             ( (int16_t)( MV ) * ( DEV_ADS1015_AD_MAX + 1 ) / DEV_ADS1015_PGA_MV_LM61 )
                                                                // LM61 mV→A/D値

//==============================================================================
//  ローカル変数
//==============================================================================

static bool     ThermalErrorDisp ;                              // 温度エラーの有無（現在の外部表示）
static bool     ThermalErrorDispOld ;                           // 温度エラーの有無（前回の外部表示）
static bool     ThermalErrorOld ;                               // 温度エラーの有無（前回の内部状態）

// LM61（温度センサ）用ADS1015設定構造体
static const DEV_I2C_ADS1015_SETUP  Ads1015SetupTherm =
{
    0x04,                           // 変換チャネル指定
    DEV_ADS1015_CONF_PGA_LM61,      // フルスケール指定
    0x01,                           // 変換モード
    DEV_ADS1015_CONF_DR_LM61        // 変換速度
} ;

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：SendSyslogThermalInfoF
// 説明         ：syslogでの温度情報送信処理
//*******************************************************************************

static bool SendSyslogThermalInfoF(                 // 戻り値：true=処理成功、false=処理失敗
    uint16_t        degree_10                       // 温度[℃]（10倍した値）
)
{
    char        send_msg[ 16 ] ;
    int         priority ;

    // syslog送信メッセージ生成
    memset( send_msg, 0, sizeof(send_msg) ) ;
    snprintf( send_msg, 16, "%.1f[degree]", (double)degree_10 / 10 ) ;

    // syslog プライオリティの設定
    if( ThermalErrorDisp )                          // エラー状態？
    {
        priority = LOG_ERR ;
    }
    else                                            // エラーなし？
    {
        priority = LOG_INFO ;
    }

    // syslog送信
    if( !ntwk_SendSyslogF( priority, LOG_LOCAL0, send_msg ) )
    {
        perror( "send syslog thermal info error." ) ;
        return false ;
    }

    return true ;
}

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：dev_ReadThermSensorF
// 説明         ：温度センサ読み出し
//*******************************************************************************

bool dev_ReadThermSensorF(              // 戻り値：true=処理成功、false=処理失敗
    int16_t     *p_degree10             // 温度[℃]（10倍した値）
)
{
    int16_t     ad_val ;
    double      degree ;

    // スレーブアドレス設定
    if( !cpu_SetI2cSlaveAddrF( DEV_I2C_ADDR_THERM_SENSOR ) )
    {
        return false ;
    }

    // A/D変換開始
    if( !dev_StartAdcAds1015F( (DEV_I2C_ADS1015_SETUP *)&Ads1015SetupTherm ) )
    {
        return false ;
    }

    // A/D変換完了待ち
    usleep( DEV_ADS1015_DR_US_LM61 ) ;

    // A/D変換結果読み出し
    if( !dev_ReadAdcAds1015F( &ad_val ) )
    {
        return false ;
    }

    // 温度に変換（A/D→温度、温度は10倍した値）
    *p_degree10 = ( ad_val - DEV_LM61_MV_TO_AD( DEV_LM61_INTERCEPT ) ) * 10 / DEV_LM61_MV_TO_AD( DEV_LM61_PROP ) ;

    return true ;
}

//*******************************************************************************
// 関数名       ：dev_InitThermalInfoF
// 説明         ：温度情報監視初期化
//*******************************************************************************

bool dev_InitThermalInfoF(              // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    int16_t     degree ;
    bool        success ;

    success = true ;

    // 温度センサ読み出し（温度を10倍した値が返ってくる）
    if( !dev_ReadThermSensorF( &degree ) )
    {
        return false ;
    }

    // 温度エラー判定（初回は1回のスキャン結果でエラー判定）
    if( degree >= DEV_THERMAL_ERROR_DEG * 10 )          // エラー温度以上？
    {
        ThermalErrorDisp = true ;                       // 現在エラー中とする
    }
    else                                                // エラー温度未満？
    {
        ThermalErrorDisp = false ;                      // エラーなしとする
    }

    // LED点灯/syslog送信
    success = SendSyslogThermalInfoF( degree ) ;

    ThermalErrorDispOld = ThermalErrorDisp ;            // 他の温度エラー関連変数は、初回スキャン値で初期化
    ThermalErrorOld = ThermalErrorDisp ;

    return success ;
}

//*******************************************************************************
// 関数名       ：dev_CheckThermalInfoF
// 説明         ：温度情報監視処理
//*******************************************************************************

bool dev_CheckThermalInfoF(             // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    int16_t     degree ;
    bool        thermal_err_cur ;

    // 温度センサ読み出し（温度を10倍した値が返ってくる）
    if( !dev_ReadThermSensorF( &degree ) )
    {
        return false ;
    }

    // 内部エラー判定
    if( ThermalErrorDisp )                              // 現在エラー報知中？
    {
        if( degree <= DEV_THERMAL_NOERROR_DEG * 10 )    // エラー復帰温度まで下がった？
        {
            thermal_err_cur = false ;                   // 現在温度エラーなしとする
        }
        else                                            // エラー復帰温度まで下がっていない？
        {
            thermal_err_cur = true ;                    // エラー継続中とする
        }
    }
    else                                                // 現在エラー報知中ではない？
    {
        if( degree >= DEV_THERMAL_ERROR_DEG * 10 )      // エラー温度以上になった？
        {
            thermal_err_cur = true ;                    // 現在エラー中とする
        }
        else                                            // エラー温度未満？
        {
            thermal_err_cur = false ;                   // エラーなしとする
        }
    }

    // エラー報知判定
    if( thermal_err_cur == ThermalErrorOld )            // 現在と前回の状態は同じ？（状態が違う場合はエラー状態を変更しない）
    {
        ThermalErrorDisp = thermal_err_cur ;            // エラー状態を現在の値にする
    }

    // LED点灯/syslog送信
    if( ThermalErrorDisp != ThermalErrorDispOld )       // エラー報知状態が変化した？
    {
        SendSyslogThermalInfoF( degree ) ;
    }

    ThermalErrorOld = thermal_err_cur ;
    ThermalErrorDispOld = ThermalErrorDisp ;

    return true ;
}
