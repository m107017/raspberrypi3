//*******************************************************************************
//
// ファイル名   ：DevAdc.c
// 説明         ：A/D変換関連
//
//*******************************************************************************

#include "DevAdc.h"
#include "../Cpu/CpuI2c.h"

#include <stdint.h>
#include <stdbool.h>

#include <stdio.h>                      // perror


//==============================================================================
//  ローカル変数
//==============================================================================

//==============================================================================
//  ローカル関数
//==============================================================================

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：dev_StartAdcAds1015F
// 説明         ：ADS1015のA/D変換開始
//*******************************************************************************

bool dev_StartAdcAds1015F(                      // 戻り値：true=処理成功、false=処理失敗
    DEV_I2C_ADS1015_SETUP   *p_setup            // ADS1015変換設定
)
{
    uint8_t     send_dat[ 3 ] ;

    send_dat[ 0 ] = DEV_ADS1015_CONF_REG_ID ;
    send_dat[ 1 ] = ( ( DEV_ADS1015_CONF_OS_ADC_START << DEV_ADS1015_CONF_H_OS_BIT ) |
                      ( p_setup->Mux << DEV_ADS1015_CONF_H_MUX_BIT ) |
                      ( p_setup->Pga << DEV_ADS1015_CONF_H_PGA_BIT ) |
                      ( p_setup->Mode << DEV_ADS1015_CONF_H_MODE_BIT ) ) ;
    send_dat[ 2 ] = ( ( p_setup->Dr << DEV_ADS1015_CONF_L_DR_BIT ) |
                      DEV_ADS1015_CONF_COMP_DEF ) ;

    if( cpu_WriteI2cF( send_dat, 3 ) < 0 )
    {
        perror( "ADS1015 set configure register error." ) ;
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：dev_ReadAdcAds1015F
// 説明         ：ADS1015のA/D変換結果読み出し
//*******************************************************************************

bool dev_ReadAdcAds1015F(                       // 戻り値：true=処理成功、false=処理失敗
    int16_t        *p_rdat                      // 読み出しデータ格納先
)
{
    uint8_t     send_dat ;
    uint8_t     read_dat[ 2 ] ;

    // 参照レジスタをConversion Registerに変更
    send_dat = DEV_ADS1015_CONV_REG_ID ;
    cpu_WriteI2cF( &send_dat, 1 ) ;

    // A/D値読み出し
    if( cpu_ReadI2cF( read_dat, 2 ) < 0 )
    {
        perror( "ADS1015 read conversion register error." ) ;
        return false ;
    }

    // A/D値に変換
    *p_rdat = (int16_t)( ( ( read_dat[ 0 ] << 8 ) | read_dat[ 1 ] ) >> DEV_ADS1015_CONV_OFS_BIT ) ;

    return true ;
}
