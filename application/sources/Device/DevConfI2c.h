//*******************************************************************************
//
// ファイル名   ：DevConfI2c.h
// 説明         ：I2C設定関連
//
//*******************************************************************************

#ifndef     DEV_CONF_I2C_H_INCLUDED
#define     DEV_CONF_I2C_H_INCLUDED


// スレーブアドレス
#define     DEV_I2C_ADDR_THERM_SENSOR           0x48            // ADS1015のスレーブアドレス

#endif          // DEV_CONF_I2C_H_INCLUDED