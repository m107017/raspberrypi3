//*******************************************************************************
//
// ファイル名   ：DevLed.c
// 説明         ：LED制御関連
//
//*******************************************************************************

#include "DevLed.h"
#include "DevConfGpio.h"
#include "../Cpu/CpuGpio.h"

#include <stdbool.h>

//==============================================================================
//  ローカル変数
//==============================================================================
static int      Led1ValFd ;             // LED1出力値のファイルディスクリプタ
static int      Led2ValFd ;             // LED2出力値のファイルディスクリプタ

//==============================================================================
//  ローカル関数
//==============================================================================

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：dev_InitLed1F
// 説明         ：LED1接続ポート初期化
//*******************************************************************************
bool dev_InitLed1F(                     // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    CPU_GPIO_SETUP  setup_gpio ;

    setup_gpio.num = DEV_CONF_GPIO_LED1 ;
    setup_gpio.dir_out = true ;

    Led1ValFd = cpu_InitGpioF( &setup_gpio ) ;
    if( Led1ValFd == -1 )
    {
        return false ;
    }
    else
    {
        return true ;
    }
    
}

//*******************************************************************************
// 関数名       ：dev_KillLed1F
// 説明         ：LED1接続ポート終了
//*******************************************************************************
bool dev_KillLed1F(                     // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    return cpu_KillGpioF( DEV_CONF_GPIO_LED1, Led1ValFd ) ;
}

//*******************************************************************************
// 関数名       ：dev_CtrlLed1F
// 説明         ：LED1制御
//*******************************************************************************
void dev_CtrlLed1F(
    bool        led_on                  // true=点灯、false=消灯
)
{
    cpu_OutGpioF( led_on, Led1ValFd ) ;
}

//*******************************************************************************
// 関数名       ：dev_InitLed2F
// 説明         ：LED2接続ポート初期化
//*******************************************************************************
bool dev_InitLed2F(                     // 戻り値：true=処理成功、false=処理失敗
    void
)
{
#if defined( DEV_CONF_GPIO_LED2 )

    CPU_GPIO_SETUP  setup_gpio ;

    setup_gpio.num = DEV_CONF_GPIO_LED2 ;
    setup_gpio.dir_out = true ;

    Led2ValFd = cpu_InitGpioF( &setup_gpio ) ;
    if( Led2ValFd == -1 )
    {
        return false ;
    }
    else
    {
        return true ;
    }

#else

    return true ;

#endif
}

//*******************************************************************************
// 関数名       ：dev_KillLed2F
// 説明         ：LED2接続ポート終了
//*******************************************************************************
bool dev_KillLed2F(                     // 戻り値：true=処理成功、false=処理失敗
    void
)
{
#if defined( DEV_CONF_GPIO_LED2 )
    return cpu_KillGpioF( DEV_CONF_GPIO_LED2, Led2ValFd ) ;
#else
    return true ;
#endif
}

//*******************************************************************************
// 関数名       ：dev_CtrlLed2F
// 説明         ：LED2制御
//*******************************************************************************
void dev_CtrlLed2F(
    bool        led_on                  // true=点灯、false=消灯
)
{
#if defined( DEV_CONF_GPIO_LED2 )
    cpu_OutGpioF( led_on, Led2ValFd ) ;
#endif
}

//*******************************************************************************
// 関数名       ：dev_InitLedAllF
// 説明         ：全LED初期化
//*******************************************************************************

bool dev_InitLedAllF(                   // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    // LEDポート初期化
    if( !dev_InitLed1F( ) || !dev_InitLed2F( ) )
    {
        return false ;
    }

    // LED出力初期化
    dev_CtrlLed1F( false ) ;
    dev_CtrlLed2F( false ) ;

    return true ;
}

//*******************************************************************************
// 関数名       ：dev_KillLedAllF
// 説明         ：全LED終了
//*******************************************************************************

void dev_KillLedAllF(
    void
)
{
    // LED消灯
    dev_CtrlLed1F( false ) ;
    dev_CtrlLed2F( false ) ;

    // LEDポート終了
    dev_KillLed1F( ) ;
    dev_KillLed2F( ) ;
}
