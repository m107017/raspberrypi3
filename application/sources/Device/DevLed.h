//*******************************************************************************
//
// ファイル名   ：DevLed.h
// 説明         ：LED制御関連
//
//*******************************************************************************

#ifndef     DEV_LED_H_INCLUDED
#define     DEV_LED_H_INCLUDED

#include <stdbool.h>

//==============================================================================
//  グローバル関数
//==============================================================================

bool dev_InitLed1F(                     // 戻り値：true=処理成功、false=処理失敗
    void
) ;

bool dev_KillLed1F(                     // 戻り値：true=処理成功、false=処理失敗
    void
) ;

void dev_CtrlLed1F(
    bool        led_on                  // true=点灯、false=消灯
) ;

bool dev_InitLed2F(                     // 戻り値：true=処理成功、false=処理失敗
    void
) ;

bool dev_KillLed2F(                     // 戻り値：true=処理成功、false=処理失敗
    void
) ;

void dev_CtrlLed2F(
    bool        led_on                  // true=点灯、false=消灯
) ;

bool dev_InitLedAllF(                   // 戻り値：true=処理成功、false=処理失敗
    void
) ;

void dev_KillLedAllF(
    void
) ;

#endif          // DEV_LED_H_INCLUDED