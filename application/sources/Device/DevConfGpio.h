//*******************************************************************************
//
// ファイル名   ：DevConfGpio.h
// 説明         ：GPIO設定関連
//
//*******************************************************************************

#ifndef     DEV_CONF_GPIO_H_INCLUDED
#define     DEV_CONF_GPIO_H_INCLUDED

#define     DEV_CONF_GPIO_ACTIVE_LO     0           // Active Low
#define     DEV_CONF_GPIO_ACTIVE_HI     1           // Active High

// GPIOポート割り当て
#define     DEV_CONF_GPIO_LED1          20
#define     DEV_CONF_GPIO_LED2          21

#define     DEV_CONF_GPIO_SW            26
#define     DEV_CONF_GPIO_SW_ACTIVE     DEV_CONF_GPIO_ACTIVE_LO

#endif          // DEV_CONF_GPIO_H_INCLUDED
