//*******************************************************************************
//
// ファイル名   ：CmndDevCtrl.h
// 説明         ：デバイス制御関連コマンド処理
//
//*******************************************************************************

#ifndef     CMND_DEV_CTRL_H_INCLUDED
#define     CMND_DEV_CTRL_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#include "CmndCommon.h"
#include "CmndParser.h"

//==============================================================================
//  定数
//==============================================================================

#define     CMND_DEVCTRL_DELIM      ","                 // コマンドパラメータ区切り文字列
#define     CMND_DEVCTRL_TERM       "\x0D\x0A"          // コマンド終端文字列
#define     CMND_DEVCTRL_MAX_LEN    256                 // デバイス制御関連コマンド最大文字数

//==============================================================================
//  構造体
//==============================================================================

//==============================================================================
//  グローバル関数
//==============================================================================

bool cmnd_DevCtrlInitF(                     // 戻り値：成功したか否か
    void
) ;

bool cmnd_DevCtrlMainF(                     // 戻り値：成功したか否か
    CMND_NTWK_INFO  *p_ntwk_info            // コマンド処理用ネットワーク関連情報ポインタ
) ;

#endif          // CMND_DEV_CTRL_H_INCLUDED
