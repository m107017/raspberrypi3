//*******************************************************************************
//
// ファイル名   ：CmndParser.h
// 説明         ：コマンド解析
//
//*******************************************************************************

#ifndef     CMND_PARSER_H_INCLUDED
#define     CMND_PARSER_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

//==============================================================================
//  定数
//==============================================================================

//==============================================================================
//  構造体
//==============================================================================

// コマンド文字列テーブル
typedef struct
{
    char        *pStr ;                         // コマンド文字列
    bool        ( *pfProc )( void* ) ;          // コマンド処理

}   CMND_STR_INFO ;

// コマンド解析情報
typedef struct
{
    char        *pDelimiter ;                   // 区切り文字
    char        *pTerminate ;                   // 終端文字
    uint16_t    MaxCmdLen ;                     // 最大コマンド長

}   CMND_PARSE_INFO ;

//==============================================================================
//  グローバル関数
//==============================================================================

const char *cmnd_StrParamParseF(                    // 戻り値：次文字列パラメータのポインタまたはNULL
    const CMND_PARSE_INFO   *p_parse,               // コマンド解析情報のポインタ
    const char          *p_cmd,                     // コマンド文字列（解析対象文字が先頭にあること）
    char                *p_cmd_work,                // コマンド文字列解析ワーク（0クリアしておくこと）
    uint16_t            cmd_work_size               // コマンド文字列解析ワークのサイズ
) ;

const CMND_STR_INFO *cmnd_CmdParseF(                // 戻り値：ヒットした文字列に対応するテーブルポインタ
    const CMND_PARSE_INFO   *p_parse,               // コマンド解析情報のポインタ
    const char          *p_cmd,                     // コマンド文字列（解析対象文字が先頭にあること）
    const CMND_STR_INFO *p_str_tbl                  // コマンド文字列テーブル
) ;

const char *cmnd_IntParamParseF(                    // 戻り値：次文字列パラメータのポインタまたはNULL
    const CMND_PARSE_INFO   *p_parse,               // コマンド解析情報のポインタ
    const char          *p_cmd,                     // コマンド文字列（解析対象文字が先頭にあること）
    long                *p_val                      // 数値格納ポインタ
) ;

bool cmnd_OnOffStrParseF(                           // 戻り値：true=ON/OFFのどちらか、false=ON/OFFいずれでもない
    const char      *p_str,                         // ONまたはOFFが格納されているポインタ
    bool            *p_on_off                       // ONならtrue、OFFならfalseを格納する
) ;

void cmnd_OnOffStrEditF(
    char            *p_str,                         // ONまたはOFFが格納するポインタ
    bool            on_off_val                      // ONならtrue、OFFならfalseを格納する
) ;

#endif          // CMND_PARSER_H_INCLUDED
