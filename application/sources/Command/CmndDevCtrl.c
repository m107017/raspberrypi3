//*******************************************************************************
//
// ファイル名   ：CmndDevCtrl.h
// 説明         ：デバイス制御関連コマンド処理
//
//*******************************************************************************

#include <stdint.h>
#include <string.h>                 // strlen/memset

#include "CmndDevCtrl.h"
#include "CmndParser.h"
#include "../LinuxOs/LosShmSem.h"
#include "../Network/NtwkClient.h"

//==============================================================================
//  プロトタイプ宣言
//==============================================================================
static bool AnaLedCtrlF( void *p_arg ) ;
static bool AnaSwitchStatusF( void *p_arg ) ;

//==============================================================================
//  ローカル変数
//==============================================================================

static LOS_APL_PARAM    *pAplParam ;                            // 共有メモリアドレス
static char             StrWork[ CMND_DEVCTRL_MAX_LEN ] ;       // 文字列編集用ワーク

static const CMND_STR_INFO DevCtrlCmd[ ] =
{
    { "LED"                 , AnaLedCtrlF           },
    { "SWITCH"              , AnaSwitchStatusF      },
    { NULL                  , NULL                  }
} ;

static const CMND_PARSE_INFO DevCtrlParse =
{
    CMND_DEVCTRL_DELIM,                         // 区切り文字
    CMND_DEVCTRL_TERM,                          // 終端文字
    CMND_DEVCTRL_MAX_LEN                        // 最大コマンド長
} ;

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：StrWorkClearF
// 説明         ：文字列編集用ワーククリア
//*******************************************************************************

static void StrWorkClearF(
    void
)
{
    memset( StrWork, 0, sizeof( StrWork ) ) ;
}

//*******************************************************************************
// 関数名       ：AnaLedCtrlF
// 説明         ：LED制御コマンド解析
//                LED,<番号>,<ON/OFF情報>[CR][LF]
//*******************************************************************************

static bool AnaLedCtrlF(
    void            *p_arg                  // デバイス制御関連コマンド解析・実行コンテキストポインタ
)
{
    CMND_NTWK_INFO      *p_ntwk_info ;
    const char          *p_cmd ;
    long                led_num ;
    char                led_on_off[ 5 ] ;
    bool                is_led_on ;
    bool                *p_led_ctrl_mem ;

    p_ntwk_info = (CMND_NTWK_INFO *)p_arg ;
    p_cmd = p_ntwk_info->pCmdBuf ;

    // LED番号取得
    p_cmd = cmnd_IntParamParseF( &DevCtrlParse, p_cmd, &led_num ) ;
    if( p_cmd == NULL )
    {
        return false ;
    }

    // LED制御メモリの選択
    switch( led_num )
    {
        case 1 :
            p_led_ctrl_mem = &( pAplParam->DevParam.Led1On ) ;
            break ;

        case 2 :
            p_led_ctrl_mem = &( pAplParam->DevParam.Led2On ) ;
            break ;

        default :
            return false ;
    }

    // LED ON/OFF情報取得
    memset( led_on_off, 0, sizeof(led_on_off) ) ;
    p_cmd = cmnd_StrParamParseF( &DevCtrlParse, p_cmd, led_on_off, sizeof( led_on_off ) ) ;
    if( cmnd_OnOffStrParseF( led_on_off, &is_led_on ) && ( p_cmd != NULL ) )
    {
        // 正しく文字列を解析出来たらLED制御
        *p_led_ctrl_mem = is_led_on ;
        return true ;
    }
    else
    {
        // 解析に失敗したら何もしない
        return false ;
    }
}

//*******************************************************************************
// 関数名       ：AnaSwitchStatusF
// 説明         ：スイッチ状態送信コマンド解析
//                SWITCH,<番号>[CR][LF]
//                応答フォーマット：SWITCH,<番号>,<ON/OFF情報>[CR][LF]
//*******************************************************************************

static bool AnaSwitchStatusF(
    void            *p_arg                  // デバイス制御関連コマンド解析・実行コンテキストポインタ
)
{
    CMND_NTWK_INFO      *p_ntwk_info ;
    const char          *p_cmd ;
    char                *p_msg ;
    long                sw_num ;
    char                sw_on_off[ 5 ] ;
    bool                is_sw_on ;

    p_ntwk_info = (CMND_NTWK_INFO *)p_arg ;
    p_cmd = p_ntwk_info->pCmdBuf ;

    // スイッチ番号取得（1つしかないため、何の数字を指定してもよい）
    p_cmd = cmnd_IntParamParseF( &DevCtrlParse, p_cmd, &sw_num ) ;
    if( p_cmd == NULL )
    {
        return false ;
    }

    // スイッチ状態取得
    is_sw_on = pAplParam->DevParam.SwOn ;
    memset( sw_on_off, 0, sizeof(sw_on_off) ) ;
    cmnd_OnOffStrEditF( sw_on_off, is_sw_on ) ;         // ON/OFF文字列編集

    // 返信コマンド文字列編集、送信
    StrWorkClearF( ) ;
    sprintf( StrWork, "SWITCH,%d,%s\x0D\x0A", sw_num, sw_on_off ) ;
    ntwk_SendTcpFromServerF( p_ntwk_info->SockRecv, StrWork ) ;
}

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：cmnd_DevCtrlInitF
// 説明         ：デバイス制御関連コマンド解析初期化
//*******************************************************************************

bool cmnd_DevCtrlInitF(                     // 戻り値：成功したか否か
    void
)
{
    // 共有メモリアドレス取得
    pAplParam = los_InitSharedMemF( false, false ) ;
    if( pAplParam == (LOS_APL_PARAM *)-1 )
    {
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：cmnd_DevCtrlMainF
// 説明         ：デバイス制御関連コマンド解析
//*******************************************************************************

bool cmnd_DevCtrlMainF(                     // 戻り値：成功したか否か
    CMND_NTWK_INFO  *p_ntwk_info            // コマンド処理用ネットワーク関連情報ポインタ
)
{
    const CMND_STR_INFO *p_str_info ;
    bool                is_success ;

    // コマンド解析
    p_str_info = cmnd_CmdParseF( &DevCtrlParse, p_ntwk_info->pCmdBuf, DevCtrlCmd ) ;
    if( p_str_info != NULL )
    {
        p_ntwk_info->pCmdBuf += strlen( p_str_info->pStr ) + 1 ;    // ヒットした文字数+1(区切り文字)分ポインタを進める
        is_success = p_str_info->pfProc( p_ntwk_info ) ;            // パラメータ解析へ
    }
    else
    {
        is_success = false ;
    }

    return is_success ;
}
