//*******************************************************************************
//
// ファイル名   ：CmndParser.c
// 説明         ：コマンド解析
//
//*******************************************************************************

#include <stdint.h>
#include <stdbool.h>

#include <stdio.h>                  // snprintf
#include <stdlib.h>                 // malloc/free
#include <string.h>                 // memset/strlen/strstr/memcpy

#include "CmndParser.h"
#include "CmndCommon.h"

//==============================================================================
//  ローカル関数
//==============================================================================

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：cmnd_StrParamParseF
// 説明         ：文字列パラメータ解析
//*******************************************************************************

const char *cmnd_StrParamParseF(                    // 戻り値：次文字列パラメータのポインタまたはNULL
    const CMND_PARSE_INFO   *p_parse,               // コマンド解析情報のポインタ
    const char          *p_cmd,                     // コマンド文字列（解析対象文字が先頭にあること）
    char                *p_cmd_work,                // コマンド文字列解析ワーク（0クリアしておくこと）
    uint16_t            cmd_work_size               // コマンド文字列解析ワークのサイズ
)
{
    const char      *p_next ;
    uint16_t        cpy_size ;

    if( p_cmd == NULL )                                         // 本関数を連続でも呼び出せるようにNULL判定
    {
        return NULL ;
    }

    p_next = strstr( p_cmd, p_parse->pDelimiter ) ;             // 次区切り文字の位置を取得
    if( p_next == NULL )                                        // 次区切り文字が見つからなかった？
    {
        p_next = strstr( p_cmd, p_parse->pTerminate ) ;         // 終端文字の位置を取得
    }

    if( p_next != NULL )                                        // 次区切り文字または終端文字が見つかった？
    {
        cpy_size = p_next - p_cmd ;                             // コピーするサイズを算出
        if( cpy_size > cmd_work_size )                          // コピーするサイズはワークサイズより大きい？
        {
            cpy_size = cmd_work_size ;                          // ワークサイズ分コピーする
        }
        memcpy( p_cmd_work, p_cmd, cpy_size ) ;                 // パラメータ文字列をコピー
        p_next++ ;                                              // 次のパラメータの先頭へ
    }

    return p_next ;
}

//*******************************************************************************
// 関数名       ：cmnd_CmdParseF
// 説明         ：コマンド解析
//*******************************************************************************

const CMND_STR_INFO *cmnd_CmdParseF(                // 戻り値：ヒットした文字列に対応するテーブルポインタ
    const CMND_PARSE_INFO   *p_parse,               // コマンド解析情報のポインタ
    const char          *p_cmd,                     // コマンド文字列（解析対象文字が先頭にあること）
    const CMND_STR_INFO *p_str_tbl                  // コマンド文字列テーブル
)
{
    char        *cmd_work ;

    // コマンド文字列解析用ワークを動的確保
    cmd_work = malloc( p_parse->MaxCmdLen ) ;
    memset( cmd_work, 0, p_parse->MaxCmdLen ) ;

    // コマンド文字列取得
    p_cmd = cmnd_StrParamParseF( p_parse, p_cmd, cmd_work, p_parse->MaxCmdLen ) ;
    if( p_cmd == NULL )                                     // コマンド文字列を取得できなかった？
    {
        return NULL ;
    }

    while( p_str_tbl->pStr != NULL )
    {
        if( strcmp( cmd_work, p_str_tbl->pStr ) == 0 )      // 文字列が一致するなら検索終了
        {
            break ;
        }
        p_str_tbl++ ;
    }

    if( p_str_tbl->pStr == NULL )                           // 一致する文字列がなかったらNULLを返す
    {
        p_str_tbl = NULL ;
    }

    free( cmd_work ) ;                                      // ワークを解放

    return p_str_tbl ;
}

//*******************************************************************************
// 関数名       ：cmnd_IntParamParseF
// 説明         ：数値パラメータ文字列解析
//*******************************************************************************

const char *cmnd_IntParamParseF(                    // 戻り値：次文字列パラメータのポインタまたはNULL
    const CMND_PARSE_INFO   *p_parse,               // コマンド解析情報のポインタ
    const char          *p_cmd,                     // コマンド文字列（解析対象文字が先頭にあること）
    long                *p_val                      // 数値格納ポインタ
)
{
    char        *cmd_work ;
    char        *endptr ;

    *p_val = 0 ;

    if( p_cmd == NULL )                                         // 本関数を連続でも呼び出せるようにNULL判定
    {
        return NULL ;
    }

    // 数値パラメータ文字列解析用ワークを動的確保
    cmd_work = malloc( p_parse->MaxCmdLen ) ;
    memset( cmd_work, 0, p_parse->MaxCmdLen ) ;

    // 数値パラメータ文字列取得
    p_cmd = cmnd_StrParamParseF( p_parse, p_cmd, cmd_work, p_parse->MaxCmdLen ) ;
    if( p_cmd == NULL )                                         // コマンド文字列を取得できなかった？
    {
        return NULL ;
    }

    *p_val = strtol( cmd_work, &endptr, 10 ) ;                  // 文字列->数値変換
    if( *endptr != CMND_CODE_NUL )                              // 数値以外の文字があった？
    {
        p_cmd = NULL ;
    }

    return p_cmd ;
}

//*******************************************************************************
// 関数名       ：cmnd_OnOffStrParseF
// 説明         ：ON/OFF文字列パラメータ解析
//*******************************************************************************

bool cmnd_OnOffStrParseF(                           // 戻り値：true=ON/OFFのどちらか、false=ON/OFFいずれでもない
    const char      *p_str,                         // ONまたはOFFが格納されているポインタ
    bool            *p_on_off                       // ONならtrue、OFFならfalseを格納する
)
{
    bool    is_on_off ;

    is_on_off = true ;
    if( strcmp( p_str, "ON" ) == 0 )
    {
        *p_on_off = true ;
    }
    else if( strcmp( p_str, "OFF" ) == 0 )
    {
        *p_on_off = false ;
    }
    else
    {
        is_on_off = false ;
    }

    return is_on_off ;
}

//*******************************************************************************
// 関数名       ：cmnd_OnOffStrEditF
// 説明         ：ON/OFF文字列パラメータ作成
//*******************************************************************************

void cmnd_OnOffStrEditF(
    char            *p_str,                         // ONまたはOFFが格納するポインタ
    bool            on_off_val                      // ONならtrue、OFFならfalseを格納する
)
{
    if( on_off_val )
    {
        memcpy( p_str, "ON", 2 ) ;
    }
    else
    {
        memcpy( p_str, "OFF", 3 ) ;
    }
}
