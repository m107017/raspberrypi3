//*******************************************************************************
//
// ファイル名   ：CpuI2c.c
// 説明         ：I2C制御関連
//
//*******************************************************************************

#include "CpuI2c.h"

#include <stdint.h>
#include <stdbool.h>

#include <stdio.h>                      // perror
#include <fcntl.h>                      // open
#include <unistd.h>                     // write/read
#include <sys/ioctl.h>                  // ioctl

#include <linux/i2c-dev.h>


//==============================================================================
//  ローカル変数
//==============================================================================
static int      I2c1DevFd ;             // I2C-1ファイルディスクリプタ

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：cpu_InitI2cF
// 説明         ：I2C初期化
//*******************************************************************************

bool cpu_InitI2cF(                      // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    I2c1DevFd = open( "/dev/i2c-1", O_RDWR ) ;
    if( I2c1DevFd < 0 )
    {
        perror( "I2C initialize error." ) ;
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：cpu_SetI2cSlaveAddrF
// 説明         ：通信するI2Cデバイスのスレーブアドレス指定
//*******************************************************************************

bool cpu_SetI2cSlaveAddrF(              // 戻り値：true=処理成功、false=処理失敗
    uint8_t         slave_addr          // スレーブアドレス
)
{
    if( ioctl( I2c1DevFd, I2C_SLAVE, slave_addr ) < 0 )
    {
        perror( "I2C slave address select error." ) ;
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：cpu_WriteI2cF
// 説明         ：I2Cデータ送信
//*******************************************************************************

bool cpu_WriteI2cF(                     // 戻り値：true=処理成功、false=処理失敗
    uint8_t         *p_send_dat,        // 送信データ
    uint8_t         send_len            // 送信データ長
)
{
    if( write( I2c1DevFd, p_send_dat, send_len ) < 0 )
    {
        perror( "I2C send data error." ) ;
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：cpu_ReadI2cF
// 説明         ：I2Cデータ受信
//*******************************************************************************

bool cpu_ReadI2cF(                      // 戻り値：true=処理成功、false=処理失敗
    uint8_t         *p_read_dat,        // 受信データ
    uint8_t         read_len            // 受信データ長
)
{
    if( read( I2c1DevFd, p_read_dat, read_len ) < 0 )
    {
        perror( "I2C read data error.\n" ) ;
        return false ;
    }

    return true ;
}
