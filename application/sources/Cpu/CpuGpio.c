//*******************************************************************************
//
// ファイル名   ：CpuGpio.c
// 説明         ：GPIO制御関連
//
//*******************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include "CpuGpio.h"

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：cpu_InitGpioF
// 説明         ：GPIO初期化
//*******************************************************************************
int cpu_InitGpioF(                      // 戻り値：GPIO入出力値ファイルディスクリプタ
    CPU_GPIO_SETUP  *p_setup            // GPIO設定構造体ポインタ
)
{
    int     fd ;
    char    gpio_num_str[ CPU_GPIO_NUM_STR_LEN ] ;
    char    file_dir[ 256 ] ;
    size_t  str_len ;

    // GPIO番号を文字列に変換
    snprintf( gpio_num_str, CPU_GPIO_NUM_STR_LEN, "%d", p_setup->num ) ;
    str_len = strlen( gpio_num_str ) ;

    // 指定GPIO使用開始
    fd = open( "/sys/class/gpio/export", O_WRONLY ) ;
    if( fd < 0 )
    {
        printf( "GPIO export open error.\n" ) ;
        return fd ;
    }
    write( fd, gpio_num_str, str_len ) ;
    close( fd ) ;

    // 入出力設定に失敗する時があるため、100ms安定待ち
    usleep( 100 * 1000 ) ;

    // 指定GPIO出力設定
    snprintf( file_dir, 256, "/sys/class/gpio/gpio%d/direction", p_setup->num ) ;
    fd = open( file_dir, O_WRONLY ) ;
    if( fd < 0 )
    {
        printf( "GPIO%d direction open error.\n", p_setup->num ) ;
        return fd ;
    }
    if( p_setup->dir_out )                              // 出力設定？
    {
        write( fd, "out", 3 ) ;
    }
    else                                                // 入力設定？
    {
        write( fd, "in", 2 ) ;
    }
    close( fd ) ;

    // 指定GPIOの値を書き込むファイルのディスクリプタ取得
    snprintf( file_dir, 256, "/sys/class/gpio/gpio%d/value", p_setup->num ) ;
    if( p_setup->dir_out )                              // 出力設定？
    {
        fd = open( file_dir , O_WRONLY ) ;
    }
    else                                                // 入力設定？
    {
        fd = open( file_dir , O_RDONLY ) ;
    }
    if( fd < 0 )
    {
        printf( "GPIO%d value open error.\n", p_setup->num ) ;
    }

    return fd ;
}

//*******************************************************************************
// 関数名       ：cpu_KillGpioF
// 説明         ：GPIO終了
//*******************************************************************************
bool cpu_KillGpioF(                     // 戻り値：true=処理成功、false=処理失敗
    unsigned int    gpio_num,           // GPIO番号
    int             gpio_val_fd         // GPIO入出力値ファイルディスクリプタ
)
{
    int     fd ;
    char    gpio_num_str[ CPU_GPIO_NUM_STR_LEN ] ;
    size_t  str_len ;

    // GPIO番号を文字列に変換
    snprintf( gpio_num_str, CPU_GPIO_NUM_STR_LEN, "%d", gpio_num ) ;
    str_len = strlen( gpio_num_str ) ;

    close( gpio_val_fd ) ;

    fd = open( "/sys/class/gpio/unexport", O_WRONLY ) ;
    if( fd < 0 )
    {
        printf( "GPIO unexport open error.\n" ) ;
        return false ;
    }
    write( fd, gpio_num_str, str_len ) ;
    close( fd ) ;

    return true ;
}

//*******************************************************************************
// 関数名       ：cpu_OutGpioF
// 説明         ：GPIO出力
//*******************************************************************************
void cpu_OutGpioF(
    bool        out_1,                  // true=1出力、false=0出力
    int         gpio_val_fd             // GPIO入出力値ファイルディスクリプタ
)
{
    if( out_1 )
    {
        write( gpio_val_fd, "1", 1 ) ;
    }
    else
    {
        write( gpio_val_fd, "0", 1 ) ;
    }
}

//*******************************************************************************
// 関数名       ：cpu_InGpioF
// 説明         ：GPIO入力
//*******************************************************************************
bool cpu_InGpioF(                       // 戻り値：true=1入力、false=0入力
    int         gpio_val_fd             // GPIO入出力値ファイルディスクリプタ
)
{
    char        read_val ;
    bool        state ;

    lseek( gpio_val_fd, 0, SEEK_SET ) ;
    read( gpio_val_fd, &read_val, 1 ) ;
    if( read_val == '0' )
    {
        state = false ;
    }
    else
    {
        state = true ;
    }

    return state ;
}
