//*******************************************************************************
//
// ファイル名   ：LosShmSem.c
// 説明         ：Linux 共有メモリ/セマフォ関連
//
//*******************************************************************************

#include <stdio.h>                      // perror/fopen/close
#include <sys/ipc.h>                    // shmget/shmctl/ftok
#include <sys/shm.h>                    // shmget/shmctl
#include <sys/types.h>                  // ftok
#include <sys/stat.h>                   // chmod

#include "LosShmSem.h"

//==============================================================================
//  定数
//==============================================================================

#define LOS_SHM_PERMIT          ( S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH )   // 実行権限

//==============================================================================
//  ローカル変数
//==============================================================================

static int      ShmId ;                 // 共有メモリのID
LOS_APL_PARAM   *pAplParam ;            // 共有メモリアドレス

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：InitSharedMemValF
// 説明         ：共有メモリ変数初期化
//*******************************************************************************

static void InitSharedMemValF(
    void
)
{
    pAplParam->DevParam.Led1On = false ;
    pAplParam->DevParam.Led2On = false ;
}

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：los_InitSharedMemF
// 説明         ：共有メモリ生成・初期化
//*******************************************************************************

LOS_APL_PARAM *los_InitSharedMemF(      // 戻り値：共有メモリアドレス
    bool        is_create,              // 共有メモリを生成するか否か
    bool        is_init                 // 共有メモリ内部を初期化するか否か
)
{
    FILE        *p_file_shm ;
    key_t       shm_key ;

    // 共有メモリのファイル生成（なければ作る）
    p_file_shm = fopen( LOS_SHM_FILE_PATH, "r" ) ;
    if( p_file_shm == NULL )
    {
        p_file_shm = fopen( LOS_SHM_FILE_PATH, "w" ) ;
        if( p_file_shm == NULL )
        {
            perror( "failed create shared memory file." ) ;
            return (LOS_APL_PARAM *)-1 ;
        }
        fclose( p_file_shm ) ;
        chmod( LOS_SHM_FILE_PATH, LOS_SHM_PERMIT ) ;
    }

    // 共有メモリ生成
    shm_key = ftok( LOS_SHM_FILE_PATH, LOS_SHM_PRJ_ID ) ;
    if( shm_key == -1 )
    {
        perror( "failed get key number." ) ;
        return (LOS_APL_PARAM *)-1 ;
    }

    if( is_create )
    {
        ShmId = shmget( shm_key, sizeof( LOS_APL_PARAM ), IPC_CREAT | IPC_EXCL | LOS_SHM_PERMIT ) ;
    }
    else
    {
        ShmId = shmget( shm_key, 0, 0 ) ;
    }

    if( ShmId == -1 )
    {
        perror( "failed get shared memory." ) ;
        return (LOS_APL_PARAM *)-1 ;
    }

    // 共有メモリのアドレス取得
    pAplParam = (LOS_APL_PARAM *)shmat( ShmId, NULL, 0 ) ;
    if( pAplParam == (LOS_APL_PARAM *)-1 )
    {
        perror( "failed get shared memory addrress." ) ;
        return (LOS_APL_PARAM *)-1 ;
    }

    // 共有メモリ変数初期化
    if( is_init )
    {
        InitSharedMemValF( ) ;
    }

    return pAplParam ;
}

//*******************************************************************************
// 関数名       ：los_KillSharedMemF
// 説明         ：共有メモリ削除
//*******************************************************************************

bool los_KillSharedMemF(                // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    // 共有メモリのデタッチ
    if( shmdt( pAplParam ) == -1 )
    {
        perror( "failed detach shared memory." ) ;
        return false ;
    }

    // 共有メモリの解放
    if( shmctl( ShmId, IPC_RMID, NULL ) == -1 )
    {
        perror( "failed kill shared memory." ) ;
        return false ;
    }

    return true ;
}

//*******************************************************************************
// 関数名       ：los_GetSharedMemAddrF
// 説明         ：共有メモリアドレス取得
//*******************************************************************************

LOS_APL_PARAM *los_GetSharedMemAddrF(   // 戻り値：共有メモリアドレス
    void
)
{
    return pAplParam ;
}
