//*******************************************************************************
//
// ファイル名   ：LosCtrlFd.c
// 説明         ：Linux ファイルディスクリプタ関連
//
//*******************************************************************************

#include "LosCtrlFd.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>                  // printf
#include <string.h>                 // memset
#include <unistd.h>                 // read
#include <sys/signalfd.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/select.h>

//*******************************************************************************
// 関数名       ：los_CloseFdF
// 説明         ：ファイルディスクリプタclose
//*******************************************************************************

void los_CloseFdF(
    int         *p_fd,              // クローズするファイルディスクリプタ配列
    uint8_t     fd_cnt              // クローズするファイルディスクリプタ配列の要素数
)
{
    while( fd_cnt-- )
    {
        close( *p_fd ) ;
        p_fd++ ;
    }
}

//*******************************************************************************
// 関数名       ：los_InitSignalfdF
// 説明         ：シグナル用ファイルディスクリプタ初期化
//*******************************************************************************

int los_InitSignalfdF(                          // 戻り値：シグナル用ファイルディスクリプタ
    void
)
{
    sigset_t    mask ;

    sigemptyset( &mask ) ;                                          // シグナルマスク初期化
    sigaddset( &mask, SIGINT ) ;                                    // キーボードからの割り込みシグナル追加（Ctrl+C）
    sigaddset( &mask, SIGTERM );                                    // 終了シグナル追加（Killコマンドのデフォルト設定値）
    sigprocmask( SIG_BLOCK, &mask, NULL ) ;                         // シグナルマスク変更（現在のマスク設定に追加する）

    // シグナル用ファイルディスクリプタ生成
    return signalfd( -1, &mask, 0 ) ;
}

//*******************************************************************************
// 関数名       ：los_SignalfdProcF
// 説明         ：シグナル用ファイルディスクリプタ処理
//*******************************************************************************

bool los_SignalfdProcF(                         // 戻り値：true=プロセス終了、false=プロセス継続
    int         sfd                             // シグナル用ファイルディスクリプタ
)
{
    struct signalfd_siginfo     info ;
    bool                        kill_proc ;

    // 捕捉したシグナル情報取得
    read( sfd, &info, sizeof(struct signalfd_siginfo) ) ;

    // シグナルごとの処理
    switch( info.ssi_signo )
    {
        case SIGINT :
            printf( "[Info]SIGINT catched.\n" ) ;
            kill_proc = true ;
            break ;

        case SIGTERM :
            printf( "[Info]SIGTERM catched.\n" ) ;
            kill_proc = true ;
            break ;

        default :
            kill_proc = false ;
            break ;
    }

    return kill_proc ;
}

//*******************************************************************************
// 関数名       ：los_InitEpollF
// 説明         ：epollファイルディスクリプタ初期化
//*******************************************************************************

int los_InitEpollF(                             // 戻り値：epollファイルディスクリプタ
    int         *p_fd,                          // 監視するファイルディスクリプタ配列
    uint8_t     fd_cnt                          // 監視するファイルディスクリプタ数
)
{
    struct epoll_event  epev ;
    int                 ep_fd ;
    unsigned int        loop_cnt ;

    // epollファイルディスクリプタ生成
    ep_fd = epoll_create1( 0 ) ;
    if( ep_fd == -1 )
    {
        return -1 ;
    }

    // epollファイルディスクリプタにファイルディスクリプタを追加
    for( loop_cnt = 0; loop_cnt < fd_cnt; loop_cnt++ )
    {
        memset( &epev, 0, sizeof(epev) ) ;
        epev.events = EPOLLIN ;
        epev.data.fd = *p_fd ;
        if( epoll_ctl( ep_fd, EPOLL_CTL_ADD, *p_fd, &epev ) != 0 )
        {
            return -1 ;
        }
        p_fd++ ;
    }

    return ep_fd ;
}

//*******************************************************************************
// 関数名       ：los_FdSetF
// 説明         ：selectで監視するファイルディスクリプタの追加
//*******************************************************************************

void los_FdSetF(
    int         fd,                             // 追加するファイルディスクリプタ
    int         *p_max_fd,                      // 追加済みのファイルディスクリプタの最大値
    fd_set      *p_fd_set                       // 監視対象のファイルディスクリプタの集合
)
{
    // エラー/未初期化チェック
    if( fd < 0 )
    {
        return ;
    }

    // 監視するファイルディスクリプタ追加
    FD_SET( fd, p_fd_set ) ;

    // 追加済みのファイルディスクリプタの最大値更新
    if( fd > *p_max_fd )
    {
        *p_max_fd = fd ;
    }
}

//*******************************************************************************
// 関数名       ：los_FdTestSetF
// 説明         ：当該ファイルディスクリプタが読み出し集合にあるかチェック
//*******************************************************************************

bool los_FdTestSetF(                            // 戻り値：指定のファイルディスクリプタが含まれているか否か
    int         fd,                             // 読み出すファイルディスクリプタ
    fd_set      *p_fd_set                       // 監視対象のファイルディスクリプタの集合
)
{
    // エラー/未初期化チェック
    if( fd < 0 )
    {
        return false ;
    }

    // 当該ファイルディスクリプタが読み出し集合にあるかチェック
    if( FD_ISSET( fd, p_fd_set ) )
    {
        return true ;
    }
    else
    {
        return false ;
    }
}
