//*******************************************************************************
//
// ファイル名   ：LosTimer.c
// 説明         ：Linuxタイマ制御関連
//
//*******************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sys/timerfd.h>

#include <unistd.h>
#include <fcntl.h>

#include "LosTimer.h"

//==============================================================================
//  グローバル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：los_InitTimerFdF
// 説明         ：タイマ用ファイルディスクリプタ初期化
// 注意事項     ：初回と2回目のタイムアウト値は同じとする。
//*******************************************************************************
int los_InitTimerFdF(                   // 戻り値：タイマ用ファイルディスクリプタ
    unsigned int    timer_ms            // タイムアウト値[ms]
)
{
    int                 tfd ;
    struct itimerspec   itimspec_led ;

    tfd = timerfd_create( CLOCK_MONOTONIC, 0 ) ;
    if( tfd == -1 )
    {
        return -1 ;
    }

    itimspec_led.it_value.tv_sec = timer_ms / 1000 ;                    // 初回のタイマ[s]
    itimspec_led.it_value.tv_nsec = ( timer_ms % 1000 ) * 1000 * 1000 ; //             [ns]
    itimspec_led.it_interval.tv_sec = itimspec_led.it_value.tv_sec ;    // 2回目以降のタイマ[s]
    itimspec_led.it_interval.tv_nsec = itimspec_led.it_value.tv_nsec ;  //                  [ns]
    timerfd_settime( tfd, 0, &itimspec_led, NULL ) ;

    return tfd ;
}

