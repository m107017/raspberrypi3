//******************************************************************************/
//
// ファイル名   ：main.c
// 説明         ：メイン処理
//
//******************************************************************************/

#include <stdio.h>                  // perror
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>                 // memset

#include <unistd.h>                 // close

#include <sys/epoll.h>
#include <pthread.h>

#include "LinuxOs/LosTimer.h"
#include "LinuxOs/LosCtrlFd.h"
#include "LinuxOs/LosShmSem.h"
#include "Network/NtwkClient.h"
#include "Network/NtwkServer.h"
#include "Device/DevLed.h"
#include "Device/DevSw.h"
#include "Device/DevSensor.h"
#include "Cpu/CpuI2c.h"

#define     LOS_EPOLL_MAX_EVENTS    2
#define     LOS_FILE_DESC_SIG_ID    0
#define     LOS_FILE_DESC_TIM1_ID   1
//#define     LOS_FILE_DESC_TIM2_ID   2

//#define     DEV_THERMAL_SENSOR_ENA                  // 温度センサ処理有効

//==============================================================================
//  ローカル変数
//==============================================================================

static int      FileDesc[ LOS_EPOLL_MAX_EVENTS ] ;              // ファイルディスクリプタ
static int      EpFileDesc ;                                    // epollファイルディスクリプタ
static LOS_APL_PARAM    *pAplParam ;                            // 共有メモリアドレス

static bool     SwInputOld ;                                    // 前回のスイッチ入力値

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：CreateThreadDetachedF
// 説明         ：スレッド生成
//               スレッドが終了するとすぐに、そのスレッド識別子とその他のリソースを
//               再利用できる設定(PTHREAD_CREATE_DETACHED)でスレッドを生成する。
//*******************************************************************************

static void CreateThreadDetachedF(
    void    *( *p_start_routine )( void * ),        // スレッド関数
    void    *p_arg                                  // スレッド関数の引数
)
{
    pthread_attr_t  attr ;
    pthread_t       tid ;

    pthread_attr_init( &attr ) ;
    pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_DETACHED ) ;
    pthread_create( &tid, &attr, p_start_routine, p_arg ) ;
}

//*******************************************************************************
// 関数名       ：Timer1IntervalProcF
// 説明         ：タイマ1用インターバル処理
//*******************************************************************************

static bool Timer1IntervalProcF(                    // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    int64_t     read_dat ;
    bool        sw_input ;

    // タイムアウト時にタイマ用ディスクリプタを読み出す
    read( FileDesc[ LOS_FILE_DESC_TIM1_ID ], &read_dat, sizeof(int64_t) ) ;

    // 温度センサ監視処理
#if defined( DEV_THERMAL_SENSOR_ENA )
    if( !dev_CheckThermalInfoF( ) )
    {
        return false ;
    }
#endif

    // LED制御
    dev_CtrlLed1F( pAplParam->DevParam.Led1On ) ;
    dev_CtrlLed2F( pAplParam->DevParam.Led2On ) ;

    // スイッチ入力
    sw_input = dev_ReadSwitchF( ) ;                     // スイッチ入力値取得（論理変換済み）
    if( sw_input == SwInputOld )                        // 前回値と一致する？
    {
        pAplParam->DevParam.SwOn = sw_input ;           // スイッチ状態を変更
    }
    SwInputOld = sw_input ;

    return true ;
}

#if defined( LOS_FILE_DESC_TIM2_ID )
//*******************************************************************************
// 関数名       ：Timer2IntervalProcF
// 説明         ：タイマ2用インターバル処理
//*******************************************************************************

static bool Timer2IntervalProcF(                    // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    int64_t     read_dat ;

    // タイムアウト時にタイマ用ディスクリプタを読み出す
    read( FileDesc[ LOS_FILE_DESC_TIM2_ID ], &read_dat, sizeof(int64_t) ) ;

    return true ;
}
#endif

//*******************************************************************************
// 関数名       ：InitTimerfdF
// 説明         ：タイマ用ファイルディスクリプタ初期化
//*******************************************************************************

static bool InitTimerfdF(                           // 戻り値：true=処理成功、false=処理失敗
    void
)
{
    // タイマ1（50ms）
    FileDesc[ LOS_FILE_DESC_TIM1_ID ] = los_InitTimerFdF( 50 ) ;
    if( FileDesc[ LOS_FILE_DESC_TIM1_ID ] == -1 )
    {
        return false ;
    }

    // タイマ2
#if defined( LOS_FILE_DESC_TIM2_ID )
    FileDesc[ LOS_FILE_DESC_TIM2_ID ] = los_InitTimerFdF( 100 ) ;
    if( FileDesc[ LOS_FILE_DESC_TIM2_ID ] == -1 )
    {
        return false ;
    }
#endif

    return true ;
}

//==============================================================================
//  main関数
//==============================================================================

//*******************************************************************************
// 関数名       ：main
// 説明         ：メイン処理
//*******************************************************************************
int main( int argc, char **argv )
{
    bool                kill_proc ;

    printf( "TCP Communication Main Program Start.\n" ) ;

    // 共有メモリアドレス取得
    pAplParam = los_InitSharedMemF( true, true ) ;
    if( pAplParam == (LOS_APL_PARAM *)-1 )
    {
        return EXIT_FAILURE ;
    }

    // LED初期化
    if( !dev_InitLedAllF( ) )
    {
        printf( "[Error]led port init failed.\n" ) ;
        return EXIT_FAILURE ;
    }

    // スイッチ初期化
    if( !dev_InitSwitchF( ) )
    {
        printf( "[Error]sw port init failed.\n" ) ;
        return EXIT_FAILURE ;
    }
    SwInputOld = dev_ReadSwitchF( ) ;
    pAplParam->DevParam.SwOn = SwInputOld ;

    // I2C初期化
    if( !cpu_InitI2cF( ) )
    {
        printf( "[Error]I2C-1 open error.\n" ) ;
        return EXIT_FAILURE ;
    }

    // タイマ用ファイルディスクリプタ生成
    if( !InitTimerfdF( ) )
    {
        printf( "[Error]timerfd init failed.\n" ) ;
        return EXIT_FAILURE ;
    }

    // シグナル用ファイルディスクリプタ生成
    FileDesc[ LOS_FILE_DESC_SIG_ID ] = los_InitSignalfdF( ) ;
    if( FileDesc[ LOS_FILE_DESC_SIG_ID ] < 0 )
    {
        printf( "[Error]signalfd init failed.\n" ) ;
        return EXIT_FAILURE ;
    }

    // epollファイルディスクリプタ生成
    EpFileDesc = los_InitEpollF( FileDesc, LOS_EPOLL_MAX_EVENTS ) ;
    if( EpFileDesc < 0 )
    {
        printf( "[Error]epoll init failed.\n" ) ;
        return EXIT_FAILURE ;
    }

    // 温度情報監視初期化
#if defined( DEV_THERMAL_SENSOR_ENA )
    dev_InitThermalInfoF( ) ;
#endif

    // デバイス制御コマンド処理用スレッド（サーバー）生成
    CreateThreadDetachedF( ntwk_ServerDevCtrlF, NULL ) ;

    // メインループ
    kill_proc = false ;
    while( kill_proc == false )
    {
        int                 fd_cnt ;
        struct epoll_event  epev_ret[ LOS_EPOLL_MAX_EVENTS ] ;
        unsigned int        loop_cnt ;

        // epollファイルディスクリプタのイベント待ち
        memset( &epev_ret, 0, sizeof(epev_ret) ) ;
        fd_cnt = epoll_wait( EpFileDesc, epev_ret, LOS_EPOLL_MAX_EVENTS, -1 ) ;
        if( fd_cnt <= 0 )
        {
            printf( "[Error]epoll wait failed.\n" ) ;
            return EXIT_FAILURE ;
        }

        // イベントが発生したファイルディスクリプタの検索＆処理
        for( loop_cnt = 0 ; loop_cnt < fd_cnt; loop_cnt++ )
        {
            // シグナル
            if( epev_ret[ loop_cnt ].data.fd == FileDesc[ LOS_FILE_DESC_SIG_ID ] )
            {
                kill_proc = los_SignalfdProcF( FileDesc[ LOS_FILE_DESC_SIG_ID ] ) ;
                break ;
            }
            // タイマ1
            else if( epev_ret[ loop_cnt ].data.fd == FileDesc[ LOS_FILE_DESC_TIM1_ID ] )
            {
                if( !Timer1IntervalProcF( ) )
                {
                    return EXIT_FAILURE ;
                }
            }
            // タイマ2
        #if defined( LOS_FILE_DESC_TIM2_ID )
            else if( epev_ret[ loop_cnt ].data.fd == FileDesc[ LOS_FILE_DESC_TIM2_ID ] )
            {
                if( !Timer2IntervalProcF( ) )
                {
                    return EXIT_FAILURE ;
                }
            }
        #endif
        }
    }

    // タイマ用ファイルディスクリプタクローズ
    los_CloseFdF( FileDesc, LOS_EPOLL_MAX_EVENTS ) ;

    // LED終了
    dev_KillLedAllF( ) ;

    // 共有メモリ解放
    los_KillSharedMemF( ) ;

    return EXIT_SUCCESS ;
}
