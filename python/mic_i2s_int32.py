import pyaudio
import wave
import csv
import itertools
import numpy as np
import time

class MicI2sInt32:
    """I2Sマイク制御モジュール（符号付32ビット）

    I2S MEMSマイクを制御する。データは符号付32ビットとする。
    事前にI2Sを使用できるようにRaspberry Piを設定しておく必要がある。
    モノラル動作のみ確認済み。

    """
    def __init__(self, max_db_spl, valied_bits, offset_raw=0):
        """初期化

        Args:
            max_db_spl (int): 最大入力音圧[dBSPL]
            valied_bits (int): 有効ビット数
            offset_raw (int, optional): 生データに対するオフセット値（生データから本値を減算する）
        """
        self.__audio = pyaudio.PyAudio()
        self.__format = pyaudio.paInt32
        self.__channels = 1
        self.__rate = 48000
        self.__chunk = 4096
        self.__max_db_spl = max_db_spl
        self.__invalied_bits = 32 - valied_bits
        self.__raw_fs = 2**(valied_bits - 1) - 1
        self.__frames = []
        self.__offset_raw = offset_raw

    def __del__(self):
        """デストラクタ
        """
        self.__audio.terminate()

    @property
    def rate(self):
        return self.__rate

    @rate.setter
    def rate(self, rate):
        self.__rate = rate
        self.__frames = []

    @property
    def chunk(self):
        return self.__chunk

    @chunk.setter
    def chunk(self, chunk):
        self.__chunk = chunk
        self.__frames = []

    def __conv_raw_data_from_byte(self, data_4byte_list, signed=True, discard=True):
        """生データのバイト列から整数への変換

        Args:
            data_4byte_list (int): 生データ（1byteごとのリスト）
            signed (bool, optional): 符号付で変換するか否か
            discard (bool, optional): 無効ビットを破棄（左ビットシフト）するか否か

        Returns:
            int: 生データ（整数）
        """
        val_int = int.from_bytes(data_4byte_list, byteorder="little", signed=signed)
        if discard:
            val_int >>= self.__invalied_bits
        return val_int

    def __get_raw_data1_from_list(self, data_1d, pos, signed=True, discard=True):
        """リスト形式の生データのバイト列から整数への変換

        Args:
            data_1d (list): I2Sで取得したデータ列（1次元データ）
            pos (int): 取得したいサンプル番号
            signed (bool): 符号付で変換するか否か
            discard (bool, optional): 無効ビットを破棄（左ビットシフト）するか否か

        Returns:
            int: 生データ（整数）
        """
        sample_size = self.get_sample_size()
        if len(data_1d) <= pos * sample_size:
            return 0
        start_pos = sample_size * pos
        end_pos_next = start_pos + sample_size
        val_int = self.__conv_raw_data_from_byte(data_1d[start_pos:end_pos_next], signed, discard)
        return val_int

    def __conv_dbspl(self, raw_list):
        """音圧[dBSPL]に変換

        Args:
            raw_list (np.ndarray): 生データ（リスト形式）

        Returns:
            float: 音圧[dBSPL]
        """
        raw_list_work = np.where(raw_list == 0, 1e-5, raw_list)
        percent_fs = (np.abs(raw_list_work) - self.__offset_raw) / self.__raw_fs
        percent_fs_work = np.where(percent_fs <= 0, 1e-5, percent_fs)
        percent_fs_work = np.where(percent_fs > 1, 1, percent_fs_work)
        db_fs = 20 * np.log10(percent_fs_work)
        return self.__max_db_spl + db_fs

    def __calc_rms(self, data):
        """RMS計算

        Args:
            data (np.ndarray): RMS計算対象データ

        Returns:
            float: RMS
        """
        rms = np.sqrt(np.mean([elm * elm for elm in data]))
        return rms

    def exe_stream_poll(self, input_device_index=1, record_sec=10):
        """ポーリングによる録音実行

        Args:
            input_device_index (int, optional): デバイスインデックス
            record_sec (int, optional): 録音時間[秒]
        """
        stream = self.__audio.open(
                    format=self.__format, 
                    channels=self.__channels,
                    rate=self.__rate, 
                    input=True,
                    frames_per_buffer=self.__chunk,
                    input_device_index=input_device_index,
                    start=False)
        time.sleep(1)
        stream.start_stream()
        self.__frames = []
        for i in range(0, int(self.__rate / self.__chunk * record_sec)):
            data = stream.read(self.__chunk)
            self.__frames.append(data)
        
        stream.stop_stream()
        stream.close()

    def get_raw_data(self):
        """生データ取得

        Returns:
            np.ndarray: 生データ列
        """
        frames_1d_int = list(itertools.chain.from_iterable(self.__frames))
        sample_size = self.get_sample_size()
        sample_cnt = int(len(frames_1d_int) / sample_size)
        data = np.empty(sample_cnt, dtype=np.int32)
        for i in range(sample_cnt):
            data[i] = self.__get_raw_data1_from_list(frames_1d_int, i)
        return data

    def calc_db_rms(self, chunk=0, file_path=None):
        """dB RMS値の計算

        Args:
            chunk (int, optional): RMS計算データ数
            file_path (str, optional): 書き込みCSVファイル（Noneの場合は書き込まない）

        Returns:
            float: RMS値
        """
        raw_np = self.get_raw_data()
        db_spl_np = self.__conv_dbspl(raw_np)
        if chunk == 0:
            chunk = self.__chunk
        calc_cnt = int(len(db_spl_np) / chunk)
        db_rms_np = np.empty(calc_cnt, dtype=np.float32)
        for i in range(calc_cnt):
            start_pos = i * chunk
            end_pos_next = (i + 1) * chunk
            db_rms_np[i] = self.__calc_rms(db_spl_np[start_pos:end_pos_next])
        if file_path != None:
            with open(file_path, "w") as f:
                writer = csv.writer(f)
                writer.writerows(db_rms_np.reshape(calc_cnt, 1))
        return db_rms_np

    def write_wave_file(self, file_path):
        """WAVファイルへの書き込み

        Args:
            file_path (str): 書き込みファイルパス
        """
        wf = wave.open(file_path, 'wb')
        wf.setnchannels(self.__channels)
        wf.setsampwidth(self.get_sample_size())
        wf.setframerate(self.__rate)
        wf.writeframes(b''.join(self.__frames))
        wf.close()

    def write_csv_file_raw(self, file_path):
        """CSVファイルへの書き込み
        I2Sで受信したデータと音圧をCSVファイルに書き込む。

        Args:
            file_path (str): 書き込みファイルパス
        """
        val_int = self.get_raw_data()
        sample_cnt = len(val_int)
        val_spl = self.__conv_dbspl(val_int)
        val_write = np.hstack([val_int.reshape(sample_cnt, 1), val_spl.reshape(sample_cnt, 1)])
        with open(file_path, "w") as f:
            writer = csv.writer(f)
            writer.writerows(val_write)

    def exec_fft(self, file_path_csv=None):
        """FFT実行

        Args:
            file_path_csv (str, optional): CSVファイルパス（Noneの場合は書き込まない）

        Returns:
            (numpy.ndarray, numpy.ndarray): 周波数, 音のレベル（振幅の大きさ）
        """
        data = self.get_raw_data()
        sample_cnt = len(data)
        y_fft = np.fft.fft(data)
        freq = np.fft.fftfreq(sample_cnt, d=1 / self.__rate)
        level = abs(y_fft / (sample_cnt / 2))
        
        if file_path_csv:
            # 0番目のデータは0Hzなので使用しない。有効データはサンプリング周波数以下のN/2番目まで。
            csv_list = list(zip(freq[1:int(sample_cnt / 2)], level[1:int(sample_cnt / 2)]))
            with open(file_path_csv, "w") as f:
                writer = csv.writer(f)
                writer.writerows(csv_list)

        return freq, level

    def get_sample_size(self):
        """サンプルサイズ取得

        Returns:
            int: サンプルサイズ
        """
        return self.__audio.get_sample_size(self.__format)

    def get_device_info_by_index(self):
        """デバイスインデックス取得
        """
        print (f"device count: {self.__audio.get_device_count()}")
        for i in range(self.__audio.get_device_count()):
            print(self.__audio.get_device_info_by_index(i))
