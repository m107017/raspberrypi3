from dev_gpio import DevGpio

class CmdAnalysis:
    """コマンド解析

    入力されたコマンドを解析し、コマンドに応じた処理を行う。
    コマンドと対応する処理は予めクラス内に定義しておくこととする。
    """

    def __init__(self):
        """コンストラクタ
        """
        self.gpio = DevGpio()
        self.term = "\r\n"
        self.param_delimiter = " "

    def __is_term(self, cmd_str):
        """コマンド終端（[CR][LF]）チェック

        Args:
            cmd_str (str): コマンド文字列

        Returns:
            bool: コマンド終端が正しいか否か
        """
        size = len(cmd_str)
        if size < 2:
            return False
        term = cmd_str[-2:]
        return term == self.term

    def execute(self, cmd_str):
        """コマンド解析実行

        Args:
            cmd_str (str): コマンド文字列

        Returns:
            bool: コマンド解析が正しく行われたか否か
        """
        # コマンド終端チェック
        if not self.__is_term(cmd_str):
            print("Terminator Error.")
            return False
        
        # コマンド終端削除＆パラメータ分解
        params = cmd_str.replace(self.term, "").split(self.param_delimiter)
        is_sucess = False
        param1_dict = {
            "LED": self.__proc_led
        }

        # 第1パラメータ解析
        for key in param1_dict:
            if params[0] == key:
                is_sucess = param1_dict[key](params)
        return is_sucess
        
    def __proc_led(self, params):
        """LEDコマンド解析

        Args:
            params (list): パラメータリスト

        Returns:
            bool: コマンド解析が正しく行われたか否か
        """
        if all([params[1] != "ON", params[1] != "OFF"]):
            return False
        self.gpio.led_set(params[1] == "ON")
        return True
