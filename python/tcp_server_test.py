import socket
from cmd_analysis import CmdAnalysis

IP_ADDR = "192.168.0.10"
PORT = 50000
RECV_SIZE = 1024

sock_sv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock_sv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock_sv.bind((IP_ADDR, PORT))
sock_sv.listen()

cmd = CmdAnalysis()

print("Start TCP Server.")
sock_cl = None
try:
    while True:
        sock_cl, addr = sock_sv.accept()
        print("Connected TCP Client.")
        while True:
            rdata = sock_cl.recv(RECV_SIZE)
            if len(rdata) > 0:
                is_success = cmd.execute(str(rdata, encoding="utf-8"))
                if not is_success:
                    sock_cl.send(bytes("Error\r\n", encoding="utf-8"))
            else:
                sock_cl.close()
                print("Disonnected TCP Client.")
                break

except KeyboardInterrupt:
    pass

if sock_cl != None:
    sock_cl.close()

print("Exit TCP Server.")
