# BME280制御テストプログラム
import spidev
import RPi.GPIO as GPIO
from time import sleep
from bme280 import Bme280

GPIO_CS_TH_SENS = 8
SPI_BUS_NUM = 0
SPI_CS_NUM = 0

spi = spidev.SpiDev()
spi.open(SPI_BUS_NUM, SPI_CS_NUM)
spi.no_cs = True
spi.max_speed_hz = 1000000
spi.mode = 0
th_sens = Bme280(spi, GPIO_CS_TH_SENS, False)

# 単一スキャン
print("Single Scan Start")
th_sens.setup(1)
while not th_sens.is_measuring():               # 計測開始待ち
    sleep(0.001)
while th_sens.is_measuring():                   # 計測終了待ち
    sleep(0.001)
th_sens.read_data(True)                         # 計測値読み出し

# 連続スキャン
print("Continuous Scan Start")
th_sens.setup(3)
try:
    while True:
        sleep(1)
        th_sens.read_data(True)

except KeyboardInterrupt:
    pass

GPIO.cleanup()
spi.close()
