import asyncio
import aiocoap
import aiocoap.resource as resource
from dev_gpio import DevGpio

class WhoAmI(resource.Resource):
    """/whoamiリソース処理

    送信元・宛先IPアドレスを応答する。
    """

    async def render_get(self, request):
        """GETリクエスト処理
        """
        text = ["Used protocol: %s." % request.remote.scheme]
        text.append("Request came from %s." % request.remote.hostinfo)
        text.append("The server address used %s." % request.remote.hostinfo_local)
        return aiocoap.Message(content_format=0, payload="\n".join(text).encode('utf8'))

class DevLed1(resource.Resource):
    """/device/led1リソース処理

    LED1の点灯制御、状態取得を行う。
    """
    def __init__(self, gpio):
        """コンストラクタ

        Args:
            gpio (DevGpio): GPIOデバイス制御クラスオブジェクト
        """
        super().__init__()
        self.gpio = gpio

    async def render_get(self, request):
        """GETリクエスト処理

        LED1の状態を取得する。
        """
        if self.gpio.led_get():
            content = b"ON"
        else:
            content = b"OFF"
        return aiocoap.Message(payload=content)

    async def render_put(self, request):
        """PUTリクエスト処理

        LED1の状態を変更する。
        """
        if request.payload == b"ON":
            self.gpio.led_set(True)
            ret_code = aiocoap.CHANGED
        elif request.payload == b"OFF":
            self.gpio.led_set(False)
            ret_code = aiocoap.CHANGED
        else:
            ret_code = aiocoap.BAD_REQUEST
        return aiocoap.Message(code=ret_code)

async def main(gpio):
    """CoAPサーバー起動

    Args:
        gpio (DevGpio): GPIOデバイス制御クラスオブジェクト
    """
    root = resource.Site()
    root.add_resource(["whoami"], WhoAmI())
    root.add_resource(["device", "led1"], DevLed1(gpio))
    await aiocoap.Context.create_server_context(root)
    await asyncio.get_running_loop().create_future()

print("Start CoAP Server.")
asyncio.run(main(DevGpio()))
