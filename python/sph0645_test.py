# SPH0645制御テストプログラム
from mic_i2s_int32 import MicI2sInt32

DEVICE_INDEX = 1
RECORD_SECONDS = 10
WAV_FILENAME = "output.wav"
CSV_FILENAME_RAW = "output_raw.csv"
CSV_FILENAME_RMS = "output_rms.csv"
CSV_FILENAME_FFT = "output_fft.csv"
MAX_DBSPL = 120
RAW_BIT = 18
OFFSET_RAW = (2**(RAW_BIT - 1) - 1) * 0.05

mic = MicI2sInt32(MAX_DBSPL, RAW_BIT, OFFSET_RAW)
mic.exe_stream_poll(DEVICE_INDEX, RECORD_SECONDS)
mic.calc_db_rms(0, CSV_FILENAME_RMS)
print("Save WAV file.")
mic.write_wave_file(WAV_FILENAME)
print("Save CSV file.")
mic.write_csv_file_raw(CSV_FILENAME_RAW)
print("Save FFT result.")
mic.exec_fft(CSV_FILENAME_FFT)
