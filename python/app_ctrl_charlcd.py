import tkinter as tk
from smbus2 import SMBus
from mi2clcd import Mi2clcd, Mi2clcdIcon

# ボタン操作時の処理
def btn_update_clicked():
    line_str = []
    for txt in txt_line:
        txt_len = len(txt.get())
        txt_space = " " * ((16 - txt_len) if txt_len < 16 else 0)
        line_str.append(txt.get() + txt_space)
    lcd.ref_all_char(line_str[0], line_str[1])
    for i, val in enumerate(chk_icon_val):
        lcd.set_icon_int(i, val.get())

def btn_clear_clicked():
    for txt in txt_line:
        txt.delete(0, tk.END)
    for chk in chk_icon_val:
        chk.set(False)
    btn_update_clicked()

# MI2CLCD制御モジュール初期化
I2C_BUS_NUM = 1
i2c_bus = SMBus(I2C_BUS_NUM)
lcd = Mi2clcd(i2c_bus, True)

# ウィンドウの作成
root = tk.Tk()
root.geometry("400x350")
root.title("Character LCD Controller")

# フレームの作成
frame_char = tk.LabelFrame(root, text="Chracters")
frame_icon = tk.LabelFrame(root, text="Icons")
frame_update = tk.Frame(root)

# 文字列表示用ラベル＆エントリー
LINE_CNT = 2
lbl_line = []
txt_line = []
for i in range(LINE_CNT):
    lbl_line.append(tk.Label(frame_char, text="Line" + str(i + 1)))
    txt_line.append(tk.Entry(frame_char, width=20))

# アイコン制御用チェックボタン
icon_str =[
    "Anntena", "Telephone", "Wireless", "Arrow",
    "Triangle1", "Triangle2", "Key", "Mute",
    "BatteryL1", "BatteryL2", "BatteryL3", "Battery",
    "Number 1"
]
chk_icon = []
chk_icon_val = []
for i, str in enumerate(icon_str):
    chk_icon_val.append(tk.BooleanVar())
    chk_icon.append(
        tk.Checkbutton(
            frame_icon, text=str, 
            variable=chk_icon_val[i]))

# 更新ボタン＆クリアボタン
btn_update = tk.Button(frame_update, text="Update", command=btn_update_clicked)
btn_clear = tk.Button(frame_update, text="Clear", command=btn_clear_clicked)

# フレームの配置
FRAME_PAD = 10
FRAME_IPAD = 10
frame_char.grid(
            row=0, column=0,
            padx=FRAME_PAD, pady=FRAME_PAD,
            ipadx=FRAME_IPAD, ipady=FRAME_IPAD,
            sticky=tk.W)
frame_icon.grid(
            row=1, column=0,
            padx=FRAME_PAD, pady=FRAME_PAD,
            ipadx=FRAME_IPAD, ipady=FRAME_IPAD,
            sticky=tk.W)
frame_update.grid(
            row=2, column=0,
            padx=FRAME_PAD, pady=FRAME_PAD,
            ipadx=FRAME_IPAD, ipady=FRAME_IPAD)

# 文字列表示フレーム内の配置
for i in range(LINE_CNT):
    lbl_line[i].grid(row=i, column=0)
    txt_line[i].grid(row=i, column=1, sticky=tk.W)

# アイコン制御フレーム内の配置
for i, chk in enumerate(chk_icon):
    chk.grid(row=int(i / 4), column=i % 4, sticky=tk.W)

# 表示更新実行フレーム内の配置
btn_update.grid(row=0, column=0)
btn_clear.grid(row=0, column=1)

root.mainloop()             # ウィンドウの表示
