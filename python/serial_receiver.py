import serial
from cmd_analysis import CmdAnalysis

UART_DEV_FILE = "/dev/serial0"
UART_BAUD = 115200
UART_TMO_SEC = 0.5

ser = serial.Serial(UART_DEV_FILE, UART_BAUD, timeout=UART_TMO_SEC)
cmd = CmdAnalysis()

print("Start UART Receiver.")
try:
    while True:
        line = ser.readline()
        if len(line) != 0:
            is_success = cmd.execute(str(line, encoding="utf-8"))
            if not is_success:
                ser.write(bytes("Error\r\n", encoding="utf-8"))

except KeyboardInterrupt:
    pass

ser.close()
print("Exit UART Receiver.")
