import spidev
import RPi.GPIO as GPIO
from time import sleep

GPIO_CS_TH_SENS = 8
SPI_BUS_NUM = 0
SPI_CS_NUM = 0

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_CS_TH_SENS, GPIO.OUT, initial=GPIO.HIGH)

spi = spidev.SpiDev()
spi.open(SPI_BUS_NUM, SPI_CS_NUM)
spi.no_cs = True
spi.max_speed_hz = 1000000
spi.mode = 0

digT = []
digP = []
digH = []

t_fine = 0.0

def spi_xfer2(xfer_list):
    GPIO.output(GPIO_CS_TH_SENS, GPIO.LOW)
    rdata = spi.xfer2(xfer_list)
    GPIO.output(GPIO_CS_TH_SENS, GPIO.HIGH)
    return rdata

def writeReg(reg_addr, val):
    spi_xfer2([reg_addr & ~0x80, val])

def readReg(reg_addr):
    rdata = spi_xfer2([reg_addr, 0x00])
    return rdata[1]

def get_calib_param():
    calib = []
    
    for i in range(0x88, 0x88 + 24):
        calib.append(readReg(i))
    calib.append(readReg(0xa1))
    for i in range(0xe1, 0xe1 + 7):
        calib.append(readReg(i))

    digT.append((calib[1] << 8) | calib[0])
    digT.append((calib[3] << 8) | calib[2])
    digT.append((calib[5] << 8) | calib[4])
    digP.append((calib[7] << 8) | calib[6])
    digP.append((calib[9] << 8) | calib[8])
    digP.append((calib[11]<< 8) | calib[10])
    digP.append((calib[13]<< 8) | calib[12])
    digP.append((calib[15]<< 8) | calib[14])
    digP.append((calib[17]<< 8) | calib[16])
    digP.append((calib[19]<< 8) | calib[18])
    digP.append((calib[21]<< 8) | calib[20])
    digP.append((calib[23]<< 8) | calib[22])
    digH.append( calib[24] )
    digH.append((calib[26]<< 8) | calib[25])
    digH.append( calib[27] )
    digH.append((calib[28]<< 4) | (0x0f & calib[29]))
    digH.append((calib[30]<< 4) | ((calib[29] >> 4) & 0x0f))
    digH.append( calib[31] )
    
    for i in range(1, 2):
        if digT[i] & 0x8000:
            digT[i] = (-digT[i] ^ 0xffff) + 1

    for i in range(1, 8):
        if digP[i] & 0x8000:
            digP[i] = (-digP[i] ^ 0xffff) + 1

    for i in range(0, 6):
        if digH[i] & 0x8000:
            digH[i] = (-digH[i] ^ 0xffff) + 1

def readData():
    data = []
    for i in range (0xf7, 0xf7 + 8):
        data.append(readReg(i))
    pres_raw = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
    temp_raw = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)
    hum_raw  = (data[6] << 8)  |  data[7]

    compensate_T(temp_raw)
    compensate_P(pres_raw)
    compensate_H(hum_raw)

def compensate_P(adc_P):
    global t_fine
    pressure = 0.0
    
    v1 = (t_fine / 2.0) - 64000.0
    v2 = (((v1 / 4.0) * (v1 / 4.0)) / 2048) * digP[5]
    v2 = v2 + ((v1 * digP[4]) * 2.0)
    v2 = (v2 / 4.0) + (digP[3] * 65536.0)
    v1 = (((digP[2] * (((v1 / 4.0) * (v1 / 4.0)) / 8192)) / 8)  + ((digP[1] * v1) / 2.0)) / 262144
    v1 = ((32768 + v1) * digP[0]) / 32768
    
    if v1 == 0:
        return 0
    pressure = ((1048576 - adc_P) - (v2 / 4096)) * 3125
    if pressure < 0x80000000:
        pressure = (pressure * 2.0) / v1
    else:
        pressure = (pressure / v1) * 2
    v1 = (digP[8] * (((pressure / 8.0) * (pressure / 8.0)) / 8192.0)) / 4096
    v2 = ((pressure / 4.0) * digP[7]) / 8192.0
    pressure = pressure + ((v1 + v2 + digP[6]) / 16.0)
    print(f"pressure    : {pressure / 100:7.2f} [hPa]")

def compensate_T(adc_T):
    global t_fine
    v1 = (adc_T / 16384.0 - digT[0] / 1024.0) * digT[1]
    v2 = (adc_T / 131072.0 - digT[0] / 8192.0) ** 2 * digT[2]
    t_fine = v1 + v2
    temperature = t_fine / 5120.0
    print(f"temperature : {temperature:-6.2f} [deg. C]")

def compensate_H(adc_H):
    global t_fine
    var_h = t_fine - 76800.0
    if var_h != 0:
        var_h = (adc_H - (digH[3] * 64.0 + digH[4] / 16384.0 * var_h)) \
                * (digH[1] / 65536.0 \
                    * (1.0 + digH[5] / 67108864.0 * var_h \
                        * (1.0 + digH[2] / 67108864.0 * var_h)))
    else:
        return 0
    var_h = var_h * (1.0 - digH[0] * var_h / 524288.0)
    if var_h > 100.0:
        var_h = 100.0
    elif var_h < 0.0:
        var_h = 0.0
    print(f"humidity    : {var_h:6.2f} [%]")

def setup():
    osrs_t = 1          # Temperature oversampling x 1
    osrs_p = 1          # Pressure oversampling x 1
    osrs_h = 1          # Humidity oversampling x 1
    mode = 3            # Sensor mode -> Normal
    t_sb = 5            # Tstandby 1000ms
    filter_en = 0       # Filter off
    spi3w_en = 0        # 3-wire SPI Disable

    ctrl_meas_reg = (osrs_t << 5) | (osrs_p << 2) | mode
    config_reg    = (t_sb << 5) | (filter_en << 2) | spi3w_en
    ctrl_hum_reg  = osrs_h

    writeReg(0xf2, ctrl_hum_reg)
    writeReg(0xf4, ctrl_meas_reg)
    writeReg(0xf5, config_reg)

setup()
get_calib_param()

try:
    while True:
        readData()
        sleep(1)

except KeyboardInterrupt:
    pass

GPIO.cleanup()
spi.close()
