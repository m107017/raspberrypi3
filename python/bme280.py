import RPi.GPIO as GPIO

class Bme280:
    """BME280制御モジュール

    BME280を制御する。
    通信方式はSPI 4線式とする。
    SPIのCS端子はソフトウェア制御する。
    """

    def __init__(self, spi_bus, cs_gpio, is_setup_cont):
        """BME280制御モジュール初期化

        Args:
            spi_bus (_type_): SPIバスオブジェクト
            cs_gpio (_type_): CS端子のGPIO番号
            is_setup_cont (bool): Normal（連続スキャン）でセットアップするか否か
                                  Falseの場合はSleep状態を維持する。
        
        Remarks:
            本モジュールでCS端子として使用したGPIOの、使用後の初期化cleanupは、外部で行うこと。
        """
        self.spi = spi_bus
        self.cs = cs_gpio
        self.dig_t = []
        self.dig_p = []
        self.dig_h = []
        self.t_fine = 0.0

        self.osrs_t = 1          # Temperature oversampling x 1
        self.osrs_p = 1          # Pressure oversampling x 1
        self.osrs_h = 1          # Humidity oversampling x 1
        self.t_sb = 5            # Tstandby 1000ms
        self.filter_en = 0       # Filter off
        self.spi3w_en = 0        # 3-wire SPI Disable

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.cs, GPIO.OUT, initial=GPIO.HIGH)

        mode = 3 if is_setup_cont else 0
        self.setup(mode)
        self.__get_calib_param()

    def __xfer2(self, xfer_list):
        """SPI転送

        Args:
            xfer_list (_type_): 送信データリスト（リストは1バイトずつ指定する）

        Returns:
            _type_: 読み出しデータ（マスタ送信中の読み出しデータも含む）
        """
        GPIO.output(self.cs, GPIO.LOW)
        rdata = self.spi.xfer2(xfer_list)
        GPIO.output(self.cs, GPIO.HIGH)
        return rdata

    def __write_reg(self, addr, val):
        """レジスタ書き込み

        Args:
            addr (int): レジスタアドレス
            val (int): レジスタ書き込み値
        """
        self.__xfer2([addr & ~0x80, val])

    def __read_reg(self, addr):
        """レジスタ読み出し

        Args:
            addr (int): レジスタアドレス

        Returns:
            int: レジスタ読み出し値
        """
        rdata = self.__xfer2([addr, 0x00])
        return rdata[1]

    def reset(self):
        """リセット実行
        """
        self.__write_reg(0xe0, 0xb6)

    def read_id(self, is_disp):
        """ID読み出し

        Args:
            is_disp (bool): 読み出したIDを標準出力するか否か

        Returns:
            int: ID読み出し値
        """
        id_val = self.__read_reg(0xd0)
        if is_disp:
            print(f"ID = 0x{id_val:x}")
        return id_val

    def setup(self, mode):
        """制御セットアップ

        Args:
            mode (int): モード（0: Sleep、1 or 2: Forced、3: Normal）
        """
        ctrl_meas_reg = (self.osrs_t << 5) | (self.osrs_p << 2) | (mode & 0x03)
        config_reg    = (self.t_sb << 5) | (self.filter_en << 2) | self.spi3w_en
        ctrl_hum_reg  = self.osrs_h

        self.__write_reg(0xf2, ctrl_hum_reg)
        self.__write_reg(0xf4, ctrl_meas_reg)
        self.__write_reg(0xf5, config_reg)

    def __get_calib_param(self):
        """キャリブレーションデータ読み出し
        """
        calib = []
        
        for i in range(0x88, 0x88 + 24):
            calib.append(self.__read_reg(i))
        calib.append(self.__read_reg(0xa1))
        for i in range(0xe1, 0xe1 + 7):
            calib.append(self.__read_reg(i))

        self.dig_t.append((calib[1] << 8) | calib[0])
        self.dig_t.append((calib[3] << 8) | calib[2])
        self.dig_t.append((calib[5] << 8) | calib[4])
        self.dig_p.append((calib[7] << 8) | calib[6])
        self.dig_p.append((calib[9] << 8) | calib[8])
        self.dig_p.append((calib[11]<< 8) | calib[10])
        self.dig_p.append((calib[13]<< 8) | calib[12])
        self.dig_p.append((calib[15]<< 8) | calib[14])
        self.dig_p.append((calib[17]<< 8) | calib[16])
        self.dig_p.append((calib[19]<< 8) | calib[18])
        self.dig_p.append((calib[21]<< 8) | calib[20])
        self.dig_p.append((calib[23]<< 8) | calib[22])
        self.dig_h.append( calib[24] )
        self.dig_h.append((calib[26]<< 8) | calib[25])
        self.dig_h.append( calib[27] )
        self.dig_h.append((calib[28]<< 4) | (0x0f & calib[29]))
        self.dig_h.append((calib[30]<< 4) | ((calib[29] >> 4) & 0x0f))
        self.dig_h.append( calib[31] )
        
        for i in range(1, 2):
            if self.dig_t[i] & 0x8000:
                self.dig_t[i] = (-self.dig_t[i] ^ 0xffff) + 1

        for i in range(1, 8):
            if self.dig_p[i] & 0x8000:
                self.dig_p[i] = (-self.dig_p[i] ^ 0xffff) + 1

        for i in range(0, 6):
            if self.dig_h[i] & 0x8000:
                self.dig_h[i] = (-self.dig_h[i] ^ 0xffff) + 1

    def __compensate_t(self, adc_t):
        """温度データ補正

        Args:
            adc_t (int): レジスタから読み出した温度データ（20ビット値）

        Returns:
            float: 温度[℃]
        """
        v1 = (adc_t / 16384.0 - self.dig_t[0] / 1024.0) * self.dig_t[1]
        v2 = (adc_t / 131072.0 - self.dig_t[0] / 8192.0) ** 2 * self.dig_t[2]
        self.t_fine = v1 + v2
        temperature = self.t_fine / 5120.0
        return temperature

    def __compensate_p(self, adc_p):
        """気圧データ補正

        Args:
            adc_p (int): レジスタから読み出した気圧データ（20ビット値）

        Returns:
            float: 気圧[Pa]
        """
        v1 = (self.t_fine / 2.0) - 64000.0
        v2 = (((v1 / 4.0) * (v1 / 4.0)) / 2048) * self.dig_p[5]
        v2 = v2 + ((v1 * self.dig_p[4]) * 2.0)
        v2 = (v2 / 4.0) + (self.dig_p[3] * 65536.0)
        v1 = (((self.dig_p[2] * (((v1 / 4.0) * (v1 / 4.0)) / 8192)) / 8) + ((self.dig_p[1] * v1) / 2.0)) / 262144
        v1 = ((32768 + v1) * self.dig_p[0]) / 32768    
        if v1 == 0:
            return 0
        pressure = ((1048576 - adc_p) - (v2 / 4096)) * 3125
        if pressure < 0x80000000:
            pressure = (pressure * 2.0) / v1
        else:
            pressure = (pressure / v1) * 2
        v1 = (self.dig_p[8] * (((pressure / 8.0) * (pressure / 8.0)) / 8192.0)) / 4096
        v2 = ((pressure / 4.0) * self.dig_p[7]) / 8192.0
        pressure = pressure + ((v1 + v2 + self.dig_p[6]) / 16.0)
        return pressure

    def __compensate_h(self, adc_h):
        """湿度データ補正

        Args:
            adc_h (int): レジスタから読み出した湿度データ

        Returns:
            float: 湿度[%]
        """
        var_h = self.t_fine - 76800.0
        if var_h != 0:
            var_h = (adc_h - (self.dig_h[3] * 64.0 + self.dig_h[4] / 16384.0 * var_h)) \
                    * (self.dig_h[1] / 65536.0 \
                        * (1.0 + self.dig_h[5] / 67108864.0 * var_h \
                            * (1.0 + self.dig_h[2] / 67108864.0 * var_h)))
        else:
            return 0
        var_h = var_h * (1.0 - self.dig_h[0] * var_h / 524288.0)
        if var_h > 100.0:
            var_h = 100.0
        elif var_h < 0.0:
            var_h = 0.0
        return var_h

    def read_data(self, is_disp):
        """センサデータ読み出し

        Args:
            is_disp (bool): 読み出し結果を標準出力するか否か

        Returns:
            float: 温度[℃]、気圧[Pa]、湿度[%]
        """
        data = []
        for i in range (0xf7, 0xf7 + 8):
            data.append(self.__read_reg(i))
        pres_raw = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
        temp_raw = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)
        hum_raw  = (data[6] << 8)  |  data[7]

        temp_val = self.__compensate_t(temp_raw)
        pres_val = self.__compensate_p(pres_raw)
        hum_val = self.__compensate_h(hum_raw)

        if is_disp:
            print(f"temperature : {temp_val:-6.2f} [deg. C]")
            print(f"pressure    : {pres_val / 100:7.2f} [hPa]")
            print(f"humidity    : {hum_val:6.2f} [%]")

        return temp_val, pres_val, hum_val

    def is_measuring(self):
        """計測状態取得

        Returns:
            bool: 現在計測中か否か
        """
        status = self.__read_reg(0xf3)
        return status & 0x08
        
