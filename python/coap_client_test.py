import asyncio
import aiocoap

SVR_ADDR = "raspberrypi.local"
SRC_REQ = aiocoap.PUT
SRC_URI = "device/led1"
SRC_PAYLOAD = b"ON"

async def main():
    protocol = await aiocoap.Context.create_client_context()
    request = aiocoap.Message(code=SRC_REQ, uri=f"coap://{SVR_ADDR}/{SRC_URI}", payload=SRC_PAYLOAD)
    try:
        response = await protocol.request(request).response
    except Exception as e:
        print(f"Failed to fetch resource: {e}")
    else:
        print(f"Result: {response.code}")
        print(f"Payload:\r\n{response.payload}")

asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
asyncio.run(main())
