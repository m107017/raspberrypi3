from smbus2 import i2c_msg
from time import sleep

class Eeprom24fc64:
    """24FC64制御モジュール

    EEPROM 24FC64を制御する。
    WPピンは外部でLow固定とする。
    """

    def __init__(self, i2c_bus, addr=0x50):
        """24FC64初期化

        Args:
            i2c_bus (smbus2.smbus2.SMBus): I2Cバスオブジェクト
            addr (hexadecimal, optional): スレーブアドレス
        """
        self.i2c_bus = i2c_bus
        self.slave_addr = addr
        self.mem_sz = 8 * 1024
        self.page_sz = 32

    def write(self, addr, data_list):
        """書き込み

        Args:
            addr (int): メモリアドレス
            data_list (list): 書き込みデータ（1バイト値のリスト）
        """
        data_list.insert(0, addr & 0x00ff)              # 下位アドレスを先頭に追加
        data_list.insert(0, (addr >> 8) & 0x00ff)       # さらに上位アドレスを先頭に追加
        write = i2c_msg.write(self.slave_addr, data_list)
        self.i2c_bus.i2c_rdwr(write)
        sleep(0.005)

    def read(self, addr, read_cnt):
        """読み出し

        Args:
            addr (int): メモリアドレス
            read_cnt (int): 読み出しアドレス数

        Returns:
            list: 読み出しデータ（1バイト値のリスト）
        """
        addr_list = [(addr >> 8) & 0x00ff, addr & 0x00ff]
        write = i2c_msg.write(self.slave_addr, addr_list)
        read = i2c_msg.read(self.slave_addr, read_cnt)
        self.i2c_bus.i2c_rdwr(write, read)
        return list(read)

    def erase(self, value, is_verify=False):
        """全領域消去

        Args:
            value (int): 消去値（1バイト値）
            is_verify (bool, optional): 消去チェックを行うか否か

        Returns:
            bool: 消去が正しく行われたか否か（チェックを行わない場合はTrue）
        """
        # 全領域消去
        ref_data = [value] * self.page_sz
        for addr in range(0, self.mem_sz, self.page_sz):
            self.write(addr, ref_data)

        if is_verify:
            # ページ単位で正常に消去できたかを確認する
            for addr in range(0, self.mem_sz, self.page_sz):
                rdata = self.read(addr, self.page_sz)
                if rdata != ref_data:
                    return False

        return True
