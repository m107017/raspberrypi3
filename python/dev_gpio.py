import RPi.GPIO as GPIO

class DevGpio:
    """GPIOデバイス制御

    LED、スイッチといった、GPIO制御を行うデバイスの制御を行う。
    GPIO番号や入出力設定は予めクラス内に記述しておくものとする。
    """

    def __init__(self, is_cleanup=True):
        """コンストラクタ

        Args:
            is_cleanup (bool, optional): デストラクタでGPIO初期化を行うか否か
        """
        self.gpio_led = 20
        self.led_is_active_high = True
        self.is_cleanup = is_cleanup
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.gpio_led, GPIO.OUT, initial=GPIO.LOW)

    def __del__(self):
        """デストラクタ
        """
        if self.is_cleanup:
            GPIO.cleanup()

    def led_set(self, is_on):
        """LED制御

        Args:
            is_on (bool): True=点灯、False=消灯
        """
        if not self.led_is_active_high:
            is_on = not is_on           # Active Lowの場合は引数の点灯状態を反転する
        if is_on:
            GPIO.output(self.gpio_led, GPIO.HIGH)
        else:
            GPIO.output(self.gpio_led, GPIO.LOW)
    
    def led_get(self):
        """LED状態取得

        Returns:
            bool: True=点灯、False=消灯
        """
        state = GPIO.input(self.gpio_led)
        is_on = True if state == GPIO.HIGH else False
        if not self.led_is_active_high:
            is_on = not is_on           # Active Lowの場合は点灯状態を反転する
        return is_on
