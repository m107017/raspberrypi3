import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def plot_data_1d(file_path, col, start=0, end=0, title=None, xlabel=None, ylabel=None):
    """1次元データプロット

    Args:
        file_path (str): ファイルパス
        col (int): 列番号
        start (int, optional): 開始行
        end (int, optional): 終了行（0の場合、最終行）
        title (str, optional): グラフタイトル
        xlabel (str, optional): X軸のラベル
        ylabel (str, optional): Y軸のラベル
    """
    df = pd.read_csv(file_path, index_col=None, usecols=[col])
    if end == 0:
        end = len(df)
    x = np.arange(start, end)
    y = df.iloc[start:end, 0]
    plt.plot(x, y)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()
