# MI2CLCD制御テストプログラム
from smbus2 import SMBus
from time import sleep
from mi2clcd import Mi2clcd, Mi2clcdIcon

I2C_BUS_NUM = 1

i2c_bus = SMBus(I2C_BUS_NUM)
lcd = Mi2clcd(i2c_bus, True)
lcd.ref_all_char("12345", "ABCDE")
lcd.set_charcode(0, 6, 0x1e)
lcd.set_charcode(1, 6, 0x1f)

a = 50
lcd.set_char(0, 8, str(a))

b = 3.14
lcd.set_char(1, 8, str(b))

lcd.set_icon_enum(Mi2clcdIcon.ANTENNA, True)

try:
    while True:
        sleep(0.01)

except KeyboardInterrupt:
    pass

lcd.clear_all_char()
lcd.clear_all_icon()
