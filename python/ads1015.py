from smbus2 import i2c_msg
from time import sleep

class Ads1015:
    """ADS1015制御モジュール

    ADS1015を制御する。
    A/D変換の設定は固定とする。
    ・AIN0のみ変換
    ・FSR=±4.096V
    ・単一変換モード
    ・DR=1600SPS
    ・ALRTピンは使用しない

    """

    def __init__(self, i2c_bus, addr="GND"):
        """ADS1015初期化

        Args:
            i2c_bus (smbus2.smbus2.SMBus): I2Cバスオブジェクト
            addr (str, optional): ADDRピン接続先
        """
        self.i2c_bus = i2c_bus
        if addr == "VDD":
            self.slave_addr = 0x49
        elif addr == "SDA":
            self.slave_addr = 0x4a
        elif addr == "SCL":
            self.slave_addr = 0x4b
        else:
            self.slave_addr = 0x48
        self.pga_full_mv = 4096

    def start_adc(self):
        """A/D変換開始
        """
        self.i2c_bus.write_i2c_block_data(self.slave_addr, 0x01, [0xc3, 0x83])

    def read_ad_val(self):
        """A/D変換結果取得

        Returns:
            int: A/D変換結果（12ビット符号付き）
        """
        write = i2c_msg.write(self.slave_addr, [0x00])
        read = i2c_msg.read(self.slave_addr, 2)
        self.i2c_bus.i2c_rdwr(write, read)
        ad_val_list = list(read)
        return ((ad_val_list[0] << 8) + ad_val_list[1]) >> 4

    def start_and_read_ad_val(self):
        """A/D変換開始 & 結果取得

        Returns:
            int: A/D変換結果（12ビット符号付き）
        """
        self.start_adc()
        sleep(0.001)
        return self.read_ad_val()

    def calc_volt(self, ad_val):
        """A/D値から電圧値に変換

        Args:
            ad_val (int): A/D値

        Returns:
            float: 電圧値[mV]
        """
        return self.pga_full_mv * ad_val / 2**11
