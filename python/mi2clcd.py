from time import sleep
from enum import Enum

class Mi2clcd:
    """MI2CLCD制御モジュール

    MI2CLCDを制御する。
    スレーブアドレスは0x3e固定とする。
    初期化処理は、MI2CLCDに搭載されているLCDドライバICであるST7032の初期化例をベースに
    一部変更した処理を行っている。初期化内容は固定とする。
    """

    def __init__(self, i2c_bus, is_init_lcd):
        """MI2CLCD制御モジュール初期化

        Args:
            i2c_bus (smbus2.smbus2.SMBus): I2Cバスオブジェクト
            is_init_lcd (bool): LCDを初期化するか否か（初期化しない場合は、別途初期化メソッドを実行する）
        """
        self.i2c_bus = i2c_bus
        self.is_init_lcd = False
        self.slave_addr = 0x3e
        self.icon_info = [
            # Mi2clcdIconクラスのEnumの並び順に合わせること
            # addr, bit
            [0x00, 0x01 << 4],
            [0x02, 0x01 << 4],
            [0x04, 0x01 << 4],
            [0x06, 0x01 << 4],
            [0x07, 0x01 << 4],
            [0x07, 0x01 << 3],
            [0x09, 0x01 << 4],
            [0x0b, 0x01 << 4],
            [0x0d, 0x01 << 4],
            [0x0d, 0x01 << 3],
            [0x0d, 0x01 << 2],
            [0x0d, 0x01 << 1],
            [0x0f, 0x01 << 4],
        ]
        # アイコン用バッファ。このバッファに表示状態を保持しておく。
        self.icon_buf = [0] * 16
        if is_init_lcd:
            self.init()        

    def __usleep(self, us):
        """usec待ち

        Args:
            us (int): 待ち時間[usec]
        """
        sleep(us / 1000000)

    def __msleep(self, ms):
        """msec待ち

        Args:
            ms (int): 待ち時間[msec]
        """
        sleep(ms / 1000)

    def __send_cmd(self, rs, data):
        """コマンド送信

        Args:
            rs (int): control byteのRSビットの値（0 or 1）
            data (int): data byteの値
        """
        self.i2c_bus.write_byte_data(self.slave_addr, rs << 6, data)
        self.__usleep(27)

    def __set_normal_mode(self):
        """Normal Mode Instruction設定
        """
        self.__send_cmd(0, 0x38)

    def __set_ext_mode(self):
        """Extension Mode Instruction設定
        """
        self.__send_cmd(0, 0x39)

    def __set_char_data(self, addr, data):
        """キャラクタ表示コマンド送信

        Args:
            addr (int): 表示アドレス
            data (int): 表示データ（文字コード）
        """
        self.__send_cmd(0, 0x80 | addr)
        self.__send_cmd(1, data)

    def __set_icon_data(self, addr, data):
        """アイコン表示コマンド送信

        Args:
            addr (int): 表示アドレス
            data (int): 表示データ
        """
        self.__set_ext_mode()
        self.__send_cmd(0, 0x40 | addr)
        self.__send_cmd(1, data)
        self.__set_normal_mode()

    def init(self):
        """表示初期化
        """
        if not self.is_init_lcd:
            cmd1 = [[0, 0x38], [0, 0x39], [0, 0x14], [0, 0x7f],
                    [0, 0x5e], [0, 0x6b]]
            cmd2 = [[0, 0x38], [0, 0x0c], [0, 0x06], [0, 0x01]]
            for cmd in cmd1:
                self.__send_cmd(cmd[0], cmd[1])
            self.__msleep(200)
            for cmd in cmd2:
                self.__send_cmd(cmd[0], cmd[1])
            self.__msleep(2)
            self.is_init_lcd = True

    def clear_all_char(self):
        """表示クリア
        """
        self.__send_cmd(0, 0x01)
        self.__msleep(2)

    def set_charcode(self, line, pos, code):
        """キャラクタ表示（文字コードで指定）

        Args:
            line (int): 表示行（1行目=0）
            pos (int): 表示位置（1列目=0）
            code (int): 文字コード（1バイト）
        """
        if any([line > 1, pos > 15]):
            return
        addr = pos
        if line == 1:
            addr += 0x40
        self.__set_char_data(addr, code)

    def set_char(self, line, pos, str):
        """キャラクタ表示（文字列で指定）

        Args:
            line (int): 表示行（1行目=0）
            pos (int): 表示位置（1列目=0）
            str (str): 表示文字列（半角文字、カタカナ文字除く）
        """
        char_list = list(str)
        for i, char in enumerate(char_list):
            disp_pos = pos + i
            if disp_pos > 15:
                break
            self.set_charcode(line, disp_pos, ord(char))

    def ref_all_char(self, line0_str, line1_str):
        """キャラクタ表示（2行分文字列で指定）

        Args:
            line0_str (str): 1行目に表示する文字列（半角文字、カタカナ文字除く）
            line1_str (str): 2行目に表示する文字列（半角文字、カタカナ文字除く）
        """
        char_list = [ord(char) for char in line0_str]
        for i, char in enumerate(char_list):
            if i > 15:
                break
            self.__set_char_data(i, char)
        char_list = [ord(char) for char in line1_str]
        for i, char in enumerate(char_list):
            if i > 15:
                break
            self.__set_char_data(i + 0x40, char)

    def set_icon_enum(self, id_enum, is_on):
        """アイコン表示（Mi2clcdIconクラスの値で指定）

        Args:
            id_enum (Mi2clcdIcon): アイコン種別（Mi2clcdIconクラスの値）
            is_on (bool): True=表示、False=クリア
        """
        addr = self.icon_info[id_enum.value][0]
        bit = self.icon_info[id_enum.value][1]
        if is_on:
            self.icon_buf[addr] |= bit
        else:
            self.icon_buf[addr] &= ~bit
        self.__set_icon_data(addr, self.icon_buf[addr])

    def set_icon_int(self, id_int, is_on):
        """アイコン表示

        Args:
            id (int): アイコン種別（int型の値）
            is_on (bool): True=表示、False=クリア
        """
        addr = self.icon_info[id_int][0]
        bit = self.icon_info[id_int][1]
        if is_on:
            self.icon_buf[addr] |= bit
        else:
            self.icon_buf[addr] &= ~bit
        self.__set_icon_data(addr, self.icon_buf[addr])

    def set_all_icon(self):
        """アイコン全表示
        """
        for id in Mi2clcdIcon:
            self.set_icon_enum(id, True)

    def clear_all_icon(self):
        """アイコン全クリア
        """
        for id in Mi2clcdIcon:
            self.set_icon_enum(id, False)

class Mi2clcdIcon(Enum):
    """アイコン種別クラス

    Mi2clcdクラスの、アイコン制御メソッドの引数として用いる。
    """
    ANTENNA         = 0         # アンテナアイコン
    TEL             = 1         # 電話アイコン
    WIRELESS        = 2         # 無線アイコン
    ARROW           = 3         # 矢印アイコン
    TRIANGLE_UP     = 4         # 上三角アイコン
    TRIANGLE_DW     = 5         # 下三角アイコン
    KEY             = 6         # 鍵アイコン
    MUTE            = 7         # ミュートアイコン
    BAT_LVL1        = 8         # 電池レベル1アイコン
    BAT_LVL2        = 9         # 電池レベル2アイコン
    BAT_LVL3        = 10        # 電池レベル3アイコン
    BAT             = 11        # 電池アイコン
    ONE             = 12        # 1番アイコン
