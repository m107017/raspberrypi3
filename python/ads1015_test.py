# ADS1015制御テストプログラム

from smbus2 import SMBus
from time import sleep
from ads1015 import Ads1015

I2C_BUS_NUM = 1

i2c_bus = SMBus(I2C_BUS_NUM)
adc = Ads1015(i2c_bus)

try:
    while True:
        ad_val = adc.start_and_read_ad_val()
        print("AD Value =", hex(ad_val), "(", ad_val, ")")
        print("Voltage = ", int(adc.calc_volt(ad_val)), "[mV]")
        print()
        sleep(1)

except KeyboardInterrupt:
    pass
