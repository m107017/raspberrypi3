from smbus2 import SMBus
from eeprom_24fc64 import Eeprom24fc64

def marching_rw(eeprom, read_val, write_val):
    error_cnt = 0
    for addr in range(0, eeprom.mem_sz):
        rdata = eeprom.read(addr, 1)
        if rdata[0] != read_val:
            print(f"addr={addr:4x} data={rdata[0]} Error")
            error_cnt += 1
        eeprom.write(addr, [write_val])
        if addr % 0x0100 == 0:
            print(f"addr={addr:4x}")
    return error_cnt

I2C_BUS_NUM = 1
i2c_bus = SMBus(I2C_BUS_NUM)
eeprom = Eeprom24fc64(i2c_bus)
error_cnt = 0

try:
    # Step1: 全メモリアドレスに0書き込み
    print("Step1: Write 0 in all address.")
    eeprom.erase(0x00)

    # Step2: 0読み出し→1書き込み
    print("Step2: Read 0 and Write 1 in all address.")
    error_cnt += marching_rw(eeprom, 0x00, 0xff)

    # Step3: 1読み出し→0書き込み
    print("Step3: Read 1 and Write 0 in all address.")
    error_cnt += marching_rw(eeprom, 0xff, 0x00)

    # Step4: 全メモリアドレスから0読み出し
    print("Step4: Read 0 in all address.")
    for addr in range(0, eeprom.mem_sz):
        rdata = eeprom.read(addr, 1)
        if rdata[0] != 0x00:
            print(f"Error addr={addr:4x} data={rdata[0]}")
            error_cnt += 1
        if addr % 0x0100 == 0:
            print(f"addr={addr:4x}")

except KeyboardInterrupt:
    pass

print(f"Done. Error Count = {error_cnt:d}")
