//******************************************************************************/
//
// ファイル名   ：main.c
// 説明         ：メイン処理
//
//******************************************************************************/

#include <stdio.h>              // perror
#include <stdlib.h>             // EXIT_***
#include <fcntl.h>              // open
#include <unistd.h>             // close/sleep
#include <string.h>             // memset
#include <sys/ioctl.h>          // ioctl
#include <string.h>             // strlen

#include "../mi2clcd.h"

#define LCD_DEV_FILE_NAME       "/dev/mi2clcd0"

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：send_ioctl_cmd
// 説明         ：IOCTLコマンド送信
//*******************************************************************************

static void send_ioctl_cmd(
    int             fd,                 // ファイルディスクリプタ
    unsigned long   cmd                 // IOCTLコマンド
)
{
    if( ioctl( fd, cmd, NULL ) < 0 )
    {
        perror( "ioctl error" ) ;
    }
}

//==============================================================================
//  main関数
//==============================================================================

//*******************************************************************************
// 関数名       ：main
// 説明         ：メイン処理
//*******************************************************************************
int main( int argc, char **argv )
{
    int     fd ;
    char    charbuf1[ ] = "TEST" ;
    char    charbuf2[ ] = "test" ;
    char    charbuf3[ ] = "0123" ;
    char    charbuf4[ ] = "9876" ;

    // デバイスドライバオープン
    fd = open( LCD_DEV_FILE_NAME, O_RDWR ) ;
    if( fd < 0 )
    {
        perror( "Test Driver Open" ) ;
        return fd ;
    }

    // 表示位置初期化->キャラクタLCDへの文字表示
    lseek( fd, 0, SEEK_SET ) ;
    write( fd, charbuf1, strlen( charbuf1 ) ) ;

    // 表示位置変更->キャラクタLCDへの文字表示
    lseek( fd, 14, SEEK_SET ) ;
    write( fd, charbuf2, strlen( charbuf2 ) ) ;

    send_ioctl_cmd( fd, MI2CLCD_ICON_ANTENNA_ON ) ;         // アンテナアイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_TEL_ON ) ;             // 電話アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_WIRELESS_ON ) ;        // 無線アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_ARROW_ON ) ;           // 矢印アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_TRIANGLE_UP_ON ) ;     // 上三角アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_TRIANGLE_DW_ON ) ;     // 下三角アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_KEY_ON ) ;             // 鍵アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_MUTE_ON ) ;            // ミュートアイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_LVL1_ON ) ;        // 電池レベル1アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_LVL2_ON ) ;        // 電池レベル2アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_LVL3_ON ) ;        // 電池レベル3アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_ON ) ;             // 電池アイコンON
    send_ioctl_cmd( fd, MI2CLCD_ICON_ONE_ON ) ;             // 1番アイコンON
    sleep( 5 ) ;                                            // 5秒待ち

    send_ioctl_cmd( fd, MI2CLCD_DISP_CLR ) ;                // ディスプレイクリア

    // 表示位置変更->キャラクタLCDへの文字表示
    lseek( fd, 2, SEEK_CUR ) ;
    write( fd, charbuf3, strlen( charbuf3 ) ) ;

    // 表示位置変更->キャラクタLCDへの文字表示
    lseek( fd, -3, SEEK_END ) ;
    write( fd, charbuf4, strlen( charbuf4 ) ) ;

    send_ioctl_cmd( fd, MI2CLCD_ICON_ANTENNA_OFF ) ;        // アンテナアイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_TEL_OFF ) ;            // 電話アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_WIRELESS_OFF ) ;       // 無線アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_ARROW_OFF ) ;          // 矢印アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_TRIANGLE_UP_OFF ) ;    // 上三角アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_TRIANGLE_DW_OFF ) ;    // 下三角アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_KEY_OFF ) ;            // 鍵アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_MUTE_OFF ) ;           // ミュートアイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_LVL1_OFF ) ;       // 電池レベル1アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_LVL2_OFF ) ;       // 電池レベル2アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_LVL3_OFF ) ;       // 電池レベル3アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_BAT_OFF ) ;            // 電池アイコンOFF
    send_ioctl_cmd( fd, MI2CLCD_ICON_ONE_OFF ) ;            // 1番アイコンOFF

    // デバイスドライバクローズ
    close( fd ) ;

    return EXIT_SUCCESS ;
}
