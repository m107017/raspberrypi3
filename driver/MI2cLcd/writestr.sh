#!/bin/sh

# Raspberry Pi 3向けに作成したI2CキャラクタLCD用ドライバの文字書き込みスクリプト
# 第1引数：表示したい文字列（32文字以内）

# デバイスドライバload
sudo insmod mi2clcd.ko

# 文字列表示
bash -c "echo -n $1 > /dev/mi2clcd0"

# デバイスドライバunload
sudo rmmod mi2clcd.ko
