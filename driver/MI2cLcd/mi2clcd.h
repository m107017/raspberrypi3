//*******************************************************************************
//
// ファイル名   ：mi2clcd.h
// 説明         ：キャラクタLCD MI2CLCD用デバイスドライバ
//
//*******************************************************************************

#ifndef     MI2CLCD_H_INCLUDED
#define     MI2CLCD_H_INCLUDED

#include <linux/ioctl.h>

//==============================================================================
//  定数
//==============================================================================

// ioctl用コマンド(request, 第2引数)の定義
#define MI2CLCD_IOC_TYPE            'C'

// ioctl用コマンド
#define MI2CLCD_DISP_CLR                _IO( MI2CLCD_IOC_TYPE, 1 )      // ディスプレイクリア
#define MI2CLCD_ICON_ANTENNA_ON         _IO( MI2CLCD_IOC_TYPE, 2 )      // アンテナアイコンON
#define MI2CLCD_ICON_ANTENNA_OFF        _IO( MI2CLCD_IOC_TYPE, 3 )      // アンテナアイコンOFF
#define MI2CLCD_ICON_TEL_ON             _IO( MI2CLCD_IOC_TYPE, 4 )      // 電話アイコンON
#define MI2CLCD_ICON_TEL_OFF            _IO( MI2CLCD_IOC_TYPE, 5 )      // 電話アイコンOFF
#define MI2CLCD_ICON_WIRELESS_ON        _IO( MI2CLCD_IOC_TYPE, 6 )      // 無線アイコンON
#define MI2CLCD_ICON_WIRELESS_OFF       _IO( MI2CLCD_IOC_TYPE, 7 )      // 無線アイコンOFF
#define MI2CLCD_ICON_ARROW_ON           _IO( MI2CLCD_IOC_TYPE, 8 )      // 矢印アイコンON
#define MI2CLCD_ICON_ARROW_OFF          _IO( MI2CLCD_IOC_TYPE, 9 )      // 矢印アイコンOFF
#define MI2CLCD_ICON_TRIANGLE_UP_ON     _IO( MI2CLCD_IOC_TYPE, 10 )     // 上三角アイコンON
#define MI2CLCD_ICON_TRIANGLE_UP_OFF    _IO( MI2CLCD_IOC_TYPE, 11 )     // 上三角アイコンOFF
#define MI2CLCD_ICON_TRIANGLE_DW_ON     _IO( MI2CLCD_IOC_TYPE, 12 )     // 下三角アイコンON
#define MI2CLCD_ICON_TRIANGLE_DW_OFF    _IO( MI2CLCD_IOC_TYPE, 13 )     // 下三角アイコンOFF
#define MI2CLCD_ICON_KEY_ON             _IO( MI2CLCD_IOC_TYPE, 14 )     // 鍵アイコンON
#define MI2CLCD_ICON_KEY_OFF            _IO( MI2CLCD_IOC_TYPE, 15 )     // 鍵アイコンOFF
#define MI2CLCD_ICON_MUTE_ON            _IO( MI2CLCD_IOC_TYPE, 16 )     // ミュートアイコンON
#define MI2CLCD_ICON_MUTE_OFF           _IO( MI2CLCD_IOC_TYPE, 17 )     // ミュートアイコンOFF
#define MI2CLCD_ICON_BAT_LVL1_ON        _IO( MI2CLCD_IOC_TYPE, 18 )     // 電池レベル1アイコンON
#define MI2CLCD_ICON_BAT_LVL1_OFF       _IO( MI2CLCD_IOC_TYPE, 19 )     // 電池レベル1アイコンOFF
#define MI2CLCD_ICON_BAT_LVL2_ON        _IO( MI2CLCD_IOC_TYPE, 20 )     // 電池レベル2アイコンON
#define MI2CLCD_ICON_BAT_LVL2_OFF       _IO( MI2CLCD_IOC_TYPE, 21 )     // 電池レベル2アイコンOFF
#define MI2CLCD_ICON_BAT_LVL3_ON        _IO( MI2CLCD_IOC_TYPE, 22 )     // 電池レベル3アイコンON
#define MI2CLCD_ICON_BAT_LVL3_OFF       _IO( MI2CLCD_IOC_TYPE, 23 )     // 電池レベル3アイコンOFF
#define MI2CLCD_ICON_BAT_ON             _IO( MI2CLCD_IOC_TYPE, 24 )     // 電池アイコンON
#define MI2CLCD_ICON_BAT_OFF            _IO( MI2CLCD_IOC_TYPE, 25 )     // 電池アイコンOFF
#define MI2CLCD_ICON_ONE_ON             _IO( MI2CLCD_IOC_TYPE, 26 )     // 1番アイコンON
#define MI2CLCD_ICON_ONE_OFF            _IO( MI2CLCD_IOC_TYPE, 27 )     // 1番アイコンOFF

//==============================================================================
//  列挙型
//==============================================================================

typedef enum
{
    MI2CLCD_ICON_ID_ANTENNA     = 0,
    MI2CLCD_ICON_ID_TEL,
    MI2CLCD_ICON_ID_WIRELESS,
    MI2CLCD_ICON_ID_ARROW,
    MI2CLCD_ICON_ID_TRIANGLE_UP,
    MI2CLCD_ICON_ID_TRIANGLE_DW,
    MI2CLCD_ICON_ID_KEY,
    MI2CLCD_ICON_ID_MUTE,
    MI2CLCD_ICON_ID_BAT_LVL1,
    MI2CLCD_ICON_ID_BAT_LVL2,
    MI2CLCD_ICON_ID_BAT_LVL3,
    MI2CLCD_ICON_ID_BAT,
    MI2CLCD_ICON_ID_ONE

}       MI2CLCD_ICON_ID ;

#endif          // MI2CLCD_H_INCLUDED