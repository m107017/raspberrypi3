//******************************************************************************/
//
// ファイル名   ：mi2clcd.c
// 説明         ：キャラクタLCD MI2CLCD用デバイスドライバ
//
//******************************************************************************/

#include <linux/module.h>
#include <linux/fs.h>                   // file_operations/alloc_chrdev_region/unregister_chrdev_region
#include <linux/cdev.h>                 // cdev_***
#include <linux/uaccess.h>              // copy_from_user/copy_to_user
#include <linux/delay.h>                // msleep/*delay
#include <linux/i2c.h>

#include "mi2clcd.h"

MODULE_LICENSE( "Dual BSD/GPL" ) ;

#define DRV_NAME                    "mi2clcd"       // ドライバ名
#define MINOR_BASE                  0               // マイナー番号の割り当て開始番号
#define MINOR_CNT                   1               // マイナー番号の割り当て可能数

#define I2C_DEV_NAME_0              DRV_NAME        // I2Cデバイス0のデバイス名
#define I2C_DEV_ID_0                0               // I2Cデバイス0のID

// キャラクタLCDコマンド
#define I2C_CLCD_CMD_CLEAR          0x01            // Display Clear
#define I2C_CLCD_CMD_RET_HOME       0x02            // Return Home
#define I2C_CLCD_CMD_ENTRY_MODE     0x04            // Entry Mode Set
#define I2C_CLCD_CMD_DISP_SFT_NONE  0x00            // Entry Mode Set:1文字表示後アドレスを移動しない
#define I2C_CLCD_DISP_SFT_R         0x02            // Entry Mode Set:1文字表示後アドレスを右移動
#define I2C_CLCD_DISP_SFT_L         0x03            // Entry Mode Set:1文字表示後アドレスを左移動
#define I2C_CLCD_CMD_DISP_CTRL      0x08            // Display Control
#define I2C_CLCD_DISP_ON            0x04            // Display Control:ディスプレイON
#define I2C_CLCD_CURSOR_ON          0x02            // Display Control:カーソルON
#define I2C_CLCD_CMD_CURSOR_SFT     0x10            // Cursor Shift
#define I2C_CLCD_CMD_FUNC_SET       0x20            // Function Set
#define I2C_CLCD_DL_8               0x10            // Function Set:送信データ長=8ビット
#define I2C_CLCD_LINE_2             0x08            // Function Set:2行表示
#define I2C_CLCD_IS_NML             0x00            // Function Set:Normal Mode Instruction
#define I2C_CLCD_IS_EXT             0x01            // Function Set:Extension Mode Instruction
#define I2C_CLCD_CMD_SET_CGRAM_ADDR 0x40            // Set CGRAM Address
#define I2C_CLCD_CMD_SET_DDRAM_ADDR 0x80            // Set DDRAM Address

#define I2C_CLCD_CMD_OSC            0x10            // Internal OSC frequency
#define I2C_CLCD_OSC_183            0x04            // Internal OSC frequency:OSC = 183Hz
#define I2C_CLCD_BS_DIV_5           0x00            // Internal OSC frequency:Bias = 1/5

#define I2C_CLCD_CMD_SET_ICON_ADDR  0x40            // Set ICON RAM Address
#define I2C_CLCD_CMD_POW_ICON       0x50            // Power/ICON control/Contrast set(high byte)
#define I2C_CLCD_CMD_ICON_ON        0x08            // Power/ICON control/Contrast:ICON Display ON
#define I2C_CLCD_CMD_BOOST_ON       0x04            // Power/ICON control/Contrast:Boost Circuit ON
#define I2C_CLCD_CMD_FOLLOWER_CTRL  0x60            // Follower control
#define I2C_CLCD_CMD_FOLLOWER_ON    0x08            // Follower control:Follower Curcuit ON
#define I2C_CLCD_CMD_CONTRAST_L     0x70            // Contrast set(low byte)

// キャラクタLCDコマンド設定値
#define I2C_CLCD_AMP_RATIO          0x03            // Follower control:Rab
#define I2C_CLCD_CONTRAST_H         0x02            // Power/ICON control/Contrast:Contrast set(high byte)
#define I2C_CLCD_CONTRAST_L         0x0f            // Contrast set(low byte)

#define I2C_CLCD_WAIT_CMD           50              // キャラクタLCDコマンド待ち時間[us]
#define I2C_CLCD_WAIT_CLEAR         2               // Display Clear待ち[ms]
#define I2C_CLCD_WAIT_POWER         500             // 電源安定化待ち[ms]

#define I2C_CLCD_DISP_LINE_CNT      2               // 表示行数
#define I2C_CLCD_DISP_CHAR_CNT      16              // 1行当たりの表示文字数
#define I2C_CLCD_DISP_CHAR_TTL      ( I2C_CLCD_DISP_CHAR_CNT * I2C_CLCD_DISP_LINE_CNT )
                                                    // 表示文字数
#define I2C_CLCD_ICON_RAM_CNT       16              // ICON RAMの数
#define I2C_CLCD_START_POS11        0x00            // 1行目第1エリアの先頭アドレス
#define I2C_CLCD_START_POS12        0x10            // 1行目第2エリアの先頭アドレス
#define I2C_CLCD_START_POS13        0x20            // 1行目第3エリアの先頭アドレス
#define I2C_CLCD_START_POS21        0x40            // 2行目第1エリアの先頭アドレス
#define I2C_CLCD_START_POS22        0x50            // 2行目第2エリアの先頭アドレス
#define I2C_CLCD_START_POS23        0x60            // 2行目第3エリアの先頭アドレス

#define I2C_CLCD_RS_BIT             6               // RSのビット番号
#define I2C_CLCD_RS_REG_INST        0x00            // Instruction等Data Register以外へのアクセス
#define I2C_CLCD_RS_REG_DATA        0x01            // Data Registerへのアクセス

//==============================================================================
//  構造体
//==============================================================================

// キャラクタLCD情報構造体
typedef struct
{
    char                char_buf[ I2C_CLCD_DISP_CHAR_TTL ] ;    // 表示内容格納バッファ
    uint8_t             icon_buf[ I2C_CLCD_ICON_RAM_CNT ] ;     // アイコン表示バッファ
    
}   CLCD_INFO ;

// デバイス情報構造体
typedef struct
{
    struct cdev         cdev ;              // キャラクタデバイスのオブジェクト
    struct i2c_client   *client ;           // I2C Client構造体
    unsigned int        dev_major ;	        // 動的確保されたメジャー番号
    struct class        *mydev_class ;	    // デバイスドライバのクラスオブジェクト
    CLCD_INFO           clcd_info ;         // キャラクタLCD情報構造体

}   DEVICE_INFO ;

// アイコン情報構造体
typedef struct
{
    uint8_t             addr ;              // アイコンアドレス
    uint8_t             bit ;               // ビット

}   CLCD_ICON_INFO ;


//==============================================================================
//  ローカル変数
//==============================================================================

// デバイスドライバのマッチテーブル
// デバイスドライバが対象とするcompatibleなノードを設定する
static const struct of_device_id mi2clcd_of_match_table[ ] =
{
                    // matches,node
    { .compatible = "mycompany,mi2clcd" },
    { }
} ;
MODULE_DEVICE_TABLE( of, mi2clcd_of_match_table ) ;

// デバイスドライバが対象とするI2Cデバイス識別テーブル
static struct i2c_device_id mi2clcd_i2c_idtable[ ] =
{
    // name               driver_data
	{ I2C_DEV_NAME_0    , I2C_DEV_ID_0 } ,
	{ }
} ;
MODULE_DEVICE_TABLE( i2c, mi2clcd_i2c_idtable ) ;

// キャラクタLCD初期化コマンドテーブル
static unsigned char charlcd_init_cmd1[ ] =
{
    I2C_CLCD_CMD_FUNC_SET | I2C_CLCD_DL_8 | I2C_CLCD_LINE_2 | I2C_CLCD_IS_NML,
    I2C_CLCD_CMD_FUNC_SET | I2C_CLCD_DL_8 | I2C_CLCD_LINE_2 | I2C_CLCD_IS_EXT,
    I2C_CLCD_CMD_OSC | I2C_CLCD_OSC_183 | I2C_CLCD_BS_DIV_5,
    I2C_CLCD_CMD_CONTRAST_L | I2C_CLCD_CONTRAST_L,
    I2C_CLCD_CMD_POW_ICON | I2C_CLCD_CMD_ICON_ON | I2C_CLCD_CMD_BOOST_ON | I2C_CLCD_CONTRAST_H,
    I2C_CLCD_CMD_FOLLOWER_CTRL | I2C_CLCD_CMD_FOLLOWER_ON | I2C_CLCD_AMP_RATIO,
    0
} ;

static unsigned char charlcd_init_cmd2[ ] =
{
    I2C_CLCD_CMD_FUNC_SET | I2C_CLCD_LINE_2 | I2C_CLCD_IS_NML,
    I2C_CLCD_CMD_DISP_CTRL | I2C_CLCD_DISP_ON,
    I2C_CLCD_CMD_ENTRY_MODE | I2C_CLCD_DISP_SFT_R,
    0
} ;

// アイコン情報（MI2CLCD_ICON_IDの並びに合わせること）
static const CLCD_ICON_INFO charlcd_icon_info[ ] =
{
    // addr     // bit
    { 0x00      , ( 0x01 << 4 ) },          // S1：アンテナ
    { 0x02      , ( 0x01 << 4 ) },          // S11：電話
    { 0x04      , ( 0x01 << 4 ) },          // S21：無線
    { 0x06      , ( 0x01 << 4 ) },          // S31：矢印
    { 0x07      , ( 0x01 << 4 ) },          // S36：上三角
    { 0x07      , ( 0x01 << 3 ) },          // S37：下三角
    { 0x09      , ( 0x01 << 4 ) },          // S46：鍵
    { 0x0b      , ( 0x01 << 4 ) },          // S56：ミュート
    { 0x0d      , ( 0x01 << 4 ) },          // S66：電池レベル1
    { 0x0d      , ( 0x01 << 3 ) },          // S67：電池レベル2
    { 0x0d      , ( 0x01 << 2 ) },          // S68：電池レベル3
    { 0x0d      , ( 0x01 << 1 ) },          // S69：電池
    { 0x0f      , ( 0x01 << 4 ) },          // S76：
} ;

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：create_cdev
// 説明         ：キャラクタデバイスのカーネルへの登録
//*******************************************************************************

static int create_cdev(
    DEVICE_INFO             *p_dev_info,        // デバイス情報構造体ポインタ
    struct file_operations  *p_fop              // システムコールハンドラテーブル
)
{
    dev_t   dev ;
    int     alloc_ret ;
    int     cdev_err ;
    int     minor ;

    // ドライバの登録（動的な方法）
    // 1. メジャー番号の動的確保
    alloc_ret = alloc_chrdev_region( &dev, MINOR_BASE, MINOR_CNT, DRV_NAME );
    if( alloc_ret != 0 )
    {
        printk( KERN_ERR "%s: alloc_chrdev_region = %d\n", DRV_NAME, alloc_ret );
        return -1;
    }
    p_dev_info->dev_major = MAJOR( dev ) ;
    printk( KERN_INFO "%s: dev_major = %d\n", DRV_NAME, p_dev_info->dev_major ) ;

    // 2. システムコールハンドラの登録
    cdev_init( &p_dev_info->cdev, p_fop ) ;
    p_dev_info->cdev.owner = THIS_MODULE ;

    // 3. カーネルへドライバの登録
    cdev_err = cdev_add( &p_dev_info->cdev, dev, MINOR_CNT ) ;
    if( cdev_err != 0 )
    {
        printk( KERN_ERR "%s: cdev_add = %d\n", DRV_NAME, cdev_err ) ;
        unregister_chrdev_region( dev, MINOR_CNT ) ;
        return -1 ;
    }

    // 4. ドライバのクラス登録(/sys/class/クラス名) 
    p_dev_info->mydev_class = class_create( THIS_MODULE, DRV_NAME ) ;
    if( IS_ERR( p_dev_info->mydev_class ) )
    {
        printk( KERN_ERR "%s: class_create\n", DRV_NAME ) ;
        cdev_del( &p_dev_info->cdev ) ;
        unregister_chrdev_region( dev, MINOR_CNT ) ;
        return -1 ;
    }

    // 5. デバイスファイル生成（/sys/class/クラス名/クラス名*）
    for ( minor = MINOR_BASE; minor < MINOR_BASE + MINOR_CNT; minor++ )
    {
        device_create( p_dev_info->mydev_class,
                       NULL,
                       MKDEV( p_dev_info->dev_major, minor ),
                       NULL,
                       "%s%d",
                       DRV_NAME,
                       minor ) ;
    }

    return 0 ;
}

//*******************************************************************************
// 関数名       ：delete_cdev
// 説明         ：キャラクタデバイスのカーネルからの削除
//*******************************************************************************

static void delete_cdev(
    DEVICE_INFO         *p_dev_info         // デバイス情報構造体ポインタ
)
{
    dev_t   dev ;
    int     minor ;

    // ドライバの削除（動的な方法）
    // 1. /sys/class/クラス名/クラス名*削除
    for( minor = MINOR_BASE; minor < MINOR_BASE + MINOR_CNT; minor++ )
    {
        device_destroy( p_dev_info->mydev_class, MKDEV( p_dev_info->dev_major, minor ) ) ;
    }

    // 2. デバイスのクラス削除（/sys/class/クラス名)
    class_destroy( p_dev_info->mydev_class ) ;

    // 3. カーネルからドライバを取り外す
    cdev_del( &p_dev_info->cdev ) ;

    // 4. メジャー番号の削除
    dev = MKDEV( p_dev_info->dev_major, MINOR_BASE ) ;
    unregister_chrdev_region( dev, MINOR_CNT ) ;
}

//*******************************************************************************
// 関数名       ：charlcd_send_cmd
// 説明         ：キャラクタLCD コマンド送信
//*******************************************************************************

static void charlcd_send_cmd(
    struct i2c_client   *p_client,          // I2Cスレーブ情報構造体ポインタ
    unsigned char       rs,                 // RSビットの値（I2C_CLCD_RS_REG_***を指定）
    char                data                // 転送コマンド or データ
)
{
    char        cmd[ 2 ] ;

    cmd[ 0 ] = rs << I2C_CLCD_RS_BIT ;
    cmd[ 1 ] = data ;
    i2c_master_send( p_client, cmd, 2 ) ;
    udelay( I2C_CLCD_WAIT_CMD ) ;
}

//*******************************************************************************
// 関数名       ：charlcd_set_nml_cmd
// 説明         ：キャラクタLCD Normal Mode Instruction設定
//*******************************************************************************

static void charlcd_set_nml_cmd(
    struct i2c_client   *p_client           // I2Cスレーブ情報構造体ポインタ
)
{
    charlcd_send_cmd( p_client,
                      I2C_CLCD_RS_REG_INST,
                      I2C_CLCD_CMD_FUNC_SET | I2C_CLCD_DL_8 | I2C_CLCD_LINE_2 | I2C_CLCD_IS_NML ) ;
}

//*******************************************************************************
// 関数名       ：charlcd_set_ext_cmd
// 説明         ：キャラクタLCD Extension Mode Instruction設定
//*******************************************************************************

static void charlcd_set_ext_cmd(
    struct i2c_client   *p_client           // I2Cスレーブ情報構造体ポインタ
)
{
    charlcd_send_cmd( p_client,
                      I2C_CLCD_RS_REG_INST,
                      I2C_CLCD_CMD_FUNC_SET | I2C_CLCD_DL_8 | I2C_CLCD_LINE_2 | I2C_CLCD_IS_EXT ) ;
}

//*******************************************************************************
// 関数名       ：charlcd_clear
// 説明         ：キャラクタLCD ディスプレイクリア
//*******************************************************************************

static void charlcd_clear(
    struct i2c_client   *p_client           // I2Cスレーブ情報構造体ポインタ
)
{
    charlcd_send_cmd( p_client, I2C_CLCD_RS_REG_INST, I2C_CLCD_CMD_CLEAR ) ;
    msleep( I2C_CLCD_WAIT_CLEAR ) ;
}

//*******************************************************************************
// 関数名       ：charlcd_init
// 説明         ：キャラクタLCD初期化
//*******************************************************************************

static void charlcd_init(
    struct i2c_client   *p_client           // I2Cスレーブ情報構造体ポインタ
)
{
    unsigned char       *p_cmd ;

    // 最初に40msの電源安定待ちが必要だが、Linuxの起動で40msは経過しているため
    // デバイスドライバで待ちは入れない

    // コマンドテーブル1実行
    p_cmd = charlcd_init_cmd1 ;
    while( *p_cmd != 0 )
    {
        charlcd_send_cmd( p_client, I2C_CLCD_RS_REG_INST, *p_cmd ) ;
        p_cmd++ ;
    }

    // 電源安定化待ち
    msleep( I2C_CLCD_WAIT_POWER ) ;

    // コマンドテーブル2実行
    p_cmd = charlcd_init_cmd2 ;
    while( *p_cmd != 0 )
    {
        charlcd_send_cmd( p_client, I2C_CLCD_RS_REG_INST, *p_cmd ) ;
        p_cmd++ ;
    }

    // ディスプレイクリア
    charlcd_clear( p_client ) ;
}

//*******************************************************************************
// 関数名       ：charlcd_disp
// 説明         ：キャラクタLCD 文字列表示
//*******************************************************************************

static loff_t charlcd_disp(
    DEVICE_INFO         *p_dev_info,        // デバイス情報構造体ポインタ
    char                *disp_work,         // 表示用ワーク
    unsigned char       data_cnt,           // 書き込みデータ数
    unsigned char       pos                 // 表示用ワーク内データ位置
)
{
    unsigned char       clcd_pos ;
    
    // キャラクタLCDのデータ位置更新
    if( pos < I2C_CLCD_DISP_CHAR_CNT )              // 1行目の位置ならそのままのアドレス
    {
        clcd_pos = pos ;
    }
    else                                            // 2行目なら2行目のアドレスに変換する
    {
        clcd_pos = I2C_CLCD_START_POS21 + ( pos - I2C_CLCD_DISP_CHAR_CNT ) ;
    }
    charlcd_send_cmd( p_dev_info->client, I2C_CLCD_RS_REG_INST, I2C_CLCD_CMD_SET_DDRAM_ADDR | clcd_pos ) ;

    disp_work += pos ;                              // 表示ワーク位置を移動

    // 終了位置まで書き込むか、指定データ数書き込み完了までループ
    while( ( pos < I2C_CLCD_DISP_CHAR_TTL ) && ( data_cnt != 0 ) )
    {
        // 1文字表示
        charlcd_send_cmd( p_dev_info->client, I2C_CLCD_RS_REG_DATA, *disp_work ) ;

        // 次の文字表示へ
        pos++ ;
        disp_work++ ;
        data_cnt-- ;

        // 改行位置に来たら、次の行にデータ位置を移動する
        if( pos == I2C_CLCD_DISP_CHAR_CNT )
        {
            charlcd_send_cmd( p_dev_info->client,
                              I2C_CLCD_RS_REG_INST,
                              I2C_CLCD_CMD_SET_DDRAM_ADDR | I2C_CLCD_START_POS21 ) ;
        }
    }
    
    // 表示用ワーク内データ位置チェック
    // 最終位置を超えたら1行目の先頭に戻す
    if( pos >= I2C_CLCD_DISP_CHAR_TTL )
    {
        pos = 0 ;
    }

    return (loff_t)pos ;
}

//*******************************************************************************
// 関数名       ：charlcd_disp_icon
// 説明         ：キャラクタLCD アイコン表示
//*******************************************************************************

static void charlcd_disp_icon(
    DEVICE_INFO         *p_dev_info,        // デバイス情報構造体ポインタ
    MI2CLCD_ICON_ID     icon_id,            // アイコンID
    bool                is_on               // ON/OFF
)
{
    const CLCD_ICON_INFO    *p_icon_info ;
    uint8_t                 *p_icon_buf ;

    // アイコンIDに対応するアイコンアドレスとビット位置を取得
    p_icon_info = &charlcd_icon_info[ icon_id ] ;

    // アイコンアドレスに対応するアイコン表示バッファポインタを取得
    p_icon_buf = &p_dev_info->clcd_info.icon_buf[ p_icon_info->addr ] ;

    // Extension Mode Instruction
    charlcd_set_ext_cmd( p_dev_info->client ) ;

    // アイコンアドレス
    charlcd_send_cmd( p_dev_info->client,
                      I2C_CLCD_RS_REG_INST,
                      I2C_CLCD_CMD_SET_ICON_ADDR | p_icon_info->addr ) ;
    
    // アイコン表示バッファ編集
    if( is_on )
    {
        *p_icon_buf |= p_icon_info->bit ;
    }
    else
    {
        *p_icon_buf &= ~p_icon_info->bit ;
    }
    
    // アイコン表示バッファの内容を書き込み
    charlcd_send_cmd( p_dev_info->client,
                      I2C_CLCD_RS_REG_DATA,
                      *p_icon_buf ) ;

    // Normal Mode Instruction
    charlcd_set_nml_cmd( p_dev_info->client ) ;
}

//==============================================================================
//  システムハンドラ
//==============================================================================

//*******************************************************************************
// 関数名       ：mi2clcd_open
// 説明         ：openハンドラ
//*******************************************************************************

static int mi2clcd_open(                    // 戻り値：0=処理成功
    struct inode    *inode,                 // inode情報構造体ポインタ
    struct file     *file                   // file構造体ポインタ
)
{
    DEVICE_INFO         *p_dev_info ;

    printk( KERN_INFO "%s: %s\n", DRV_NAME, __func__ ) ;

    // openハンドラを持つcdev（inode->i_cdev）を含んでいるDEVICE_INFO構造体ポインタを探す
    // container_ofで「変数が入っている構造体へのポインタ」をコンパイル時に計算してくれる
    // inode->i_cdevへのポインタから、DEVICE_INFO構造体へのポインタを逆算する
    // cdev構造体をDEVICE_INFO構造体に含めたため、inode->i_cdevはDEVICE_INFO構造体のメンバである
    p_dev_info = container_of( inode->i_cdev, DEVICE_INFO, cdev ) ;
    if( p_dev_info == NULL || p_dev_info->client == NULL )
    {
        printk( KERN_ERR "%s : %s container_of\n", DRV_NAME, __func__ ) ;
        return -EFAULT ;
    }

    // DEVICE_INFO構造体ポインタをfile構造体のプライベートデータに指定
    // システムコールハンドラ内で参照できるため、システムコールハンドラでもI2Cスレーブ情報構造体を参照できる
    file->private_data = p_dev_info ;

    return 0 ;
}

//*******************************************************************************
// 関数名       ：mi2clcd_close
// 説明         ：closeハンドラ
//*******************************************************************************

static int mi2clcd_close(
    struct inode    *inode,                 // inode情報構造体ポインタ
    struct file     *file                   // file構造体ポインタ
)
{
    printk( KERN_INFO "%s: %s\n", DRV_NAME, __func__ ) ;

    return 0 ;
}

//*******************************************************************************
// 関数名       ：mi2clcd_read
// 説明         ：readハンドラ
//*******************************************************************************

static ssize_t mi2clcd_read(                // 戻り値：0以上=読み込みデータ数、負の数=エラー
    struct file         *filp,              // open時に作成したfile構造体ポインタ
    char __user         *buf,               // ユーザ空間でのデータ格納ポインタ
    size_t              count,              // bufのサイズ
    loff_t              *f_pos              // オフセット
)
{
    return 0 ;
}

//*******************************************************************************
// 関数名       ：mi2clcd_write
// 説明         ：writeハンドラ
//*******************************************************************************

static ssize_t mi2clcd_write(               // 戻り値：0以上=書き込みデータ数、負の数=エラー
    struct file         *filp,              // open時に作成したfile構造体ポインタ
    const char __user   *buf,               // ユーザ空間でのデータ格納ポインタ
    size_t              count,              // bufのサイズ
    loff_t              *f_pos              // オフセット
)
{
    DEVICE_INFO         *p_dev_info ;
    
    p_dev_info = (DEVICE_INFO *)filp->private_data ;

    // 書き込みデータサイズのチェック
    if( count + *f_pos > I2C_CLCD_DISP_CHAR_TTL )               // データ書き込み先位置がバッファサイズを超える？
    {
        count = I2C_CLCD_DISP_CHAR_TTL - *f_pos ;               // バッファサイズ分書き込む
    }

    // 書き込みデータの取得
    if( copy_from_user( &( p_dev_info->clcd_info.char_buf[ *f_pos ] ), buf, count ) )
    {
        return -EFAULT ;
    }

    // キャラクタLCDへ表示
    *f_pos = charlcd_disp( p_dev_info, p_dev_info->clcd_info.char_buf, count, (unsigned char)*f_pos ) ;

    return count ;
}

//*******************************************************************************
// 関数名       ：mi2clcd_llseek
// 説明         ：llseekハンドラ
//*******************************************************************************

static loff_t mi2clcd_llseek(               // 戻り値：0以上=移動後のオフセット位置、負の数=エラー
    struct file         *filp,              // open時に作成したfile構造体ポインタ
    loff_t              off,                // オフセット
    int                 whence              // 位置指定方法
)
{
    DEVICE_INFO         *p_dev_info ;
    loff_t              newpos ;
    
    p_dev_info = (DEVICE_INFO *)filp->private_data ;
    
    switch( whence )
    {
        case SEEK_SET :                                                 // 絶対位置指定？
            newpos = off ;
            break ;
        
        case SEEK_CUR :                                                 // 現在位置からの相対値指定？
            newpos = filp->f_pos + off ;
            break ;
        
        case SEEK_END :                                                 // 最終位置からの相対値指定？
            newpos = ( I2C_CLCD_DISP_CHAR_TTL - 1 ) + off ;
            break ;
        
        default :
            return -EINVAL ;
    }
    
    // 先頭位置より前、または最終位置より後の場合はエラーを返す
    if( ( newpos < I2C_CLCD_START_POS11 ) || ( newpos >= I2C_CLCD_DISP_CHAR_TTL ) )
    {
        return -EINVAL ;
    }
    
    // カーソル位置更新
    // キャラクタLCDのカーソル位置は変更しない。書き込み時に変更する。
    filp->f_pos = newpos ;
    
    return newpos ;
}

//*******************************************************************************
// 関数名       ：mi2clcd_ioctl
// 説明         ：ioctlハンドラ
//*******************************************************************************

static long mi2clcd_ioctl(
    struct file     *filp,                  // open時に作成したfile構造体ポインタ
    unsigned int    cmd,                    // ioctl用コマンド
    unsigned long   arg                     // ユーザ空間へのポインタ（設定パラメータR/W先）
)
{
    DEVICE_INFO         *p_dev_info ;

    p_dev_info = (DEVICE_INFO *)filp->private_data ;

    switch( cmd )
    {
        case MI2CLCD_DISP_CLR :             // ディスプレイクリア
            charlcd_clear( p_dev_info->client ) ;
            break ;

        case MI2CLCD_ICON_ANTENNA_ON :      // アンテナアイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_ANTENNA, true ) ;
            break ;

        case MI2CLCD_ICON_ANTENNA_OFF :     // アンテナアイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_ANTENNA, false ) ;
            break ;

        case MI2CLCD_ICON_TEL_ON :          // 電話アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_TEL, true ) ;
            break ;

        case MI2CLCD_ICON_TEL_OFF :         // 電話アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_TEL, false ) ;
            break ;

        case MI2CLCD_ICON_WIRELESS_ON :     // 無線アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_WIRELESS, true ) ;
            break ;

        case MI2CLCD_ICON_WIRELESS_OFF :    // 無線アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_WIRELESS, false ) ;
            break ;

        case MI2CLCD_ICON_ARROW_ON :        // 矢印アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_ARROW, true ) ;
            break ;

        case MI2CLCD_ICON_ARROW_OFF :       // 矢印アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_ARROW, false ) ;
            break ;

        case MI2CLCD_ICON_TRIANGLE_UP_ON :  // 上三角アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_TRIANGLE_UP, true ) ;
            break ;

        case MI2CLCD_ICON_TRIANGLE_UP_OFF : // 上三角アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_TRIANGLE_UP, false ) ;
            break ;

        case MI2CLCD_ICON_TRIANGLE_DW_ON :  // 下三角アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_TRIANGLE_DW, true ) ;
            break ;

        case MI2CLCD_ICON_TRIANGLE_DW_OFF : // 下三角アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_TRIANGLE_DW, false ) ;
            break ;

        case MI2CLCD_ICON_KEY_ON :          // 鍵アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_KEY, true ) ;
            break ;

        case MI2CLCD_ICON_KEY_OFF :         // 鍵アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_KEY, false ) ;
            break ;

        case MI2CLCD_ICON_MUTE_ON :         // ミュートアイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_MUTE, true ) ;
            break ;

        case MI2CLCD_ICON_MUTE_OFF :        // ミュートアイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_MUTE, false ) ;
            break ;

        case MI2CLCD_ICON_BAT_LVL1_ON :     // 電池レベル1アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT_LVL1, true ) ;
            break ;

        case MI2CLCD_ICON_BAT_LVL1_OFF :    // 電池レベル1アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT_LVL1, false ) ;
            break ;

        case MI2CLCD_ICON_BAT_LVL2_ON :     // 電池レベル2アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT_LVL2, true ) ;
            break ;

        case MI2CLCD_ICON_BAT_LVL2_OFF :    // 電池レベル2アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT_LVL2, false ) ;
            break ;

        case MI2CLCD_ICON_BAT_LVL3_ON :     // 電池レベル3アイコンON
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT_LVL3, true ) ;
            break ;

        case MI2CLCD_ICON_BAT_LVL3_OFF :    // 電池レベル3アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT_LVL3, false ) ;
            break ;

        case MI2CLCD_ICON_BAT_ON :          // 電池アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT, true ) ;
            break ;

        case MI2CLCD_ICON_BAT_OFF :         // 電池アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_BAT, false ) ;
            break ;

        case MI2CLCD_ICON_ONE_ON :          // 1番アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_ONE, true ) ;
            break ;

        case MI2CLCD_ICON_ONE_OFF :         // 1番アイコンOFF
            charlcd_disp_icon( p_dev_info, MI2CLCD_ICON_ID_ONE, false ) ;
            break ;

        default :
            printk( KERN_WARNING "unsupported command %d\n", cmd ) ;
            return -EFAULT ;
    }

    return 0 ;
}

//*******************************************************************************
// 関数名       ：mi2clcd_fops
// 説明         ：各種システムコールに対応するハンドラテーブル
//*******************************************************************************

struct file_operations mi2clcd_fops =
{
    .open           = mi2clcd_open,
    .release        = mi2clcd_close,
    .unlocked_ioctl = mi2clcd_ioctl,            // 64bits向け
    .compat_ioctl   = mi2clcd_ioctl,            // 32bits向け
    .read           = mi2clcd_read,
    .write          = mi2clcd_write,
    .llseek         = mi2clcd_llseek,
} ;

//==============================================================================
//  I2Cドライバ関連
//==============================================================================

//*******************************************************************************
// 関数名       ：mi2clcd_i2c_probe_new
// 説明         ：対象とするI2Cデバイスが認識されたときに呼ばれる処理
//*******************************************************************************

static int mi2clcd_i2c_probe_new(
    struct i2c_client   *client             // I2Cスレーブ情報構造体ポインタ
)
{
    DEVICE_INFO         *p_dev_info ;
    uint8_t             cnt ;

    // 各種情報の表示
    printk( KERN_INFO "%s: %s\n", DRV_NAME, __func__ ) ;
    printk( KERN_INFO "%s: slave address = 0x%02X\n", DRV_NAME, client->addr ) ;

	// システムコールハンドラでi2c_client構造体を参照するため、領域を動的確保し、保持する
    // devm_kzallocを使用すると、デバイスが取り外されたときにメモリを自動で解放してくれる
	p_dev_info = (DEVICE_INFO *)devm_kzalloc( &client->dev, sizeof(DEVICE_INFO), GFP_KERNEL ) ;
	if( p_dev_info == NULL )
	{
		printk( KERN_ERR "%s: %s no memory\n", DRV_NAME, __func__ ) ;
		return -ENOMEM ;
	}

    // I2Cスレーブ情報構造体ポインタを保持
	p_dev_info->client = client ;

    // I2Cスレーブ情報とデバイス情報を紐づける
	i2c_set_clientdata( client, p_dev_info ) ;

    // デバイスファイル生成
    if( create_cdev( p_dev_info, &mi2clcd_fops ) != 0 )
    {
		return -ENOMEM ;
    }

    // キャラクタLCD初期化
    charlcd_init( client ) ;

    // 変数初期化
    for( cnt = 0; cnt < I2C_CLCD_DISP_CHAR_TTL; cnt++ )
    {
        p_dev_info->clcd_info.char_buf[ cnt ] = 0x00 ;
    }
    for( cnt = 0; cnt < I2C_CLCD_ICON_RAM_CNT; cnt++ )
    {
        p_dev_info->clcd_info.icon_buf[ cnt ] = 0x00 ;
    }

    return 0;
}

//*******************************************************************************
// 関数名       ：mi2clcd_i2c_remove
// 説明         ：対象とするI2Cデバイスが取り外されたときに呼ばれる処理
//*******************************************************************************

static int mi2clcd_i2c_remove(
    struct i2c_client   *client             // I2Cスレーブ情報構造体ポインタ
)
{
    DEVICE_INFO         *p_dev_info ;

    printk( KERN_INFO "%s: %s\n", DRV_NAME, __func__ ) ;

    // I2Cスレーブ情報からデバイス情報を取り出す
	p_dev_info = (DEVICE_INFO *)i2c_get_clientdata( client ) ;

    // デバイスファイル削除
	delete_cdev( p_dev_info ) ;

	return 0;
}

//*******************************************************************************
// 関数名       ：mi2clcd_i2cdrv
// 説明         ：I2Cデバイスドライバ情報
//*******************************************************************************

static struct i2c_driver mi2clcd_i2cdrv =
{
    .driver =
    {
        .name	    = DRV_NAME,
        .owner      = THIS_MODULE,
	},
    .id_table       = mi2clcd_i2c_idtable,
    .probe_new      = mi2clcd_i2c_probe_new,
    .remove         = mi2clcd_i2c_remove,
};

module_i2c_driver( mi2clcd_i2cdrv ) ;
