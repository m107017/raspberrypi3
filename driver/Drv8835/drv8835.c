//******************************************************************************/
//
// ファイル名   ：drv8835.c
// 説明         ：ステッピングモータドライバDRV8835用デバイスドライバ
//
//******************************************************************************/

#include <linux/module.h>
#include <linux/fs.h>                   // file_operations/alloc_chrdev_region/unregister_chrdev_region
#include <linux/cdev.h>                 // cdev_***
#include <linux/delay.h>                // msleep
#include <asm/io.h>                     // ioremap_nocache/iounmap/ioread*/iowrite*
#include <linux/uaccess.h>              // copy_from_user/copy_to_user

#include "drv8835.h"

MODULE_LICENSE( "Dual BSD/GPL" ) ;

#define DRV_NAME            "drv8835"           // ドライバ名
#define MINOR_BASE          0                   // マイナー番号の割り当て開始番号
#define MINOR_CNT           2                   // マイナー番号の割り当て可能数

#define MOTOR_INDEX_CNT     8                   // 相切り替えテーブルインデックス数
#define MOTOR_BIT_MSK       BIN8( 0, 0, 0, 1, 1, 1, 1, 1 )
                                                // モータ出力信号ビットマスク

#define MOTOR_BIT_NUM_L_0   20                  // モータ出力信号の最下位GPIO番号（マイナー番号0）
#define MOTOR_BIT_NUM_L_1   16                  // モータ出力信号の最下位GPIO番号（マイナー番号1）

#define MOTOR_ROT_CW        0                   // 正転
#define MOTOR_ROT_CCW       1                   // 逆転

// DRV8835用コンテキスト初期値
#define MOTOR_PHASE_TYPE_DEF        DRV8835_PHASE_TYPE_2
#define MOTOR_START_EXCITE_TIM_DEF  10
#define MOTOR_ROT_TIM_DEF           7
#define MOTOR_STOP_EXCITE_TIM_DEF   10

// マイコンレジスタアドレス
#define REG_ADDR_BASE       0x3f000000          // ペリフェラルレジスタベースアドレス（物理アドレス）
#define REG_ADDR_OFS_GPIO   0x00200000          // GPIOレジスタオフセット
#define REG_ADDR_GPIO_BASE  ( REG_ADDR_BASE + REG_ADDR_OFS_GPIO )
                                                // GPIOレジスタベースアドレス（物理アドレス）

#define GPFSEL0             0x0000              // GPIO Function Select0
#define GPFSEL_SIZE         4                   // GPFSELのレジスタサイズ（byte）
#define GPFSEL_INPUT        0                   // GPIO入力設定
#define GPFSEL_OUTPUT       1                   // GPIO出力設定
#define GPFSEL_BIT_WID      3                   // GPFSEL 1チャネル分のビット幅
#define GPFSEL_BIT_MSK      0x07                // GPFSEL 1チャネル分のビットマスク

#define GPSET0              0x001c              // GPIO Pin Output Set 0
#define GPCLR0              0x0028              // GPIO Pin Output Clear 0

// 2進数を16進数に変換（1Byte）
#define BIN8( B7, B6, B5, B4, B3, B2, B1, B0 )  (unsigned char)( ( (B7) << 7 ) | ( (B6) << 6 ) | ( (B5) << 5 ) | ( (B4) << 4 ) |    \
                                                                 ( (B3) << 3 ) | ( (B2) << 2 ) | ( (B1) << 1 ) | (B0) )

//==============================================================================
//  構造体
//==============================================================================

// マイナー番号に応じたパラメータ構造体
typedef struct
{
    struct drv8835_context  context ;           // DRV8835用コンテキスト

    int             minor ;                     // マイナー番号
    void            *p_gpio_reg ;               // GPIOレジスタへのポインタ（仮想空間）
    unsigned char   gpio_pin_l ;                // モータ出力信号の最下位GPIO番号
    unsigned char   phase_idx ;                 // 現在の相制御インデックス
    
}   DRV8835_PARAM ;


//==============================================================================
//  ローカル変数
//==============================================================================

static unsigned int     dev_major ;                         // 動的確保されたメジャー番号
static struct cdev      my_cdev ;                           // キャラクタデバイスのオブジェクト
static struct class     *mydev_class ;                      // デバイスドライバのクラスオブジェクト
DRV8835_PARAM           param[ MINOR_CNT ] ;                // マイナー番号に応じたパラメータ構造体

// モータ出力最下位GPIOピン配列
static unsigned char const gpio_pin_l_tbl[ MINOR_CNT ] =
{
    MOTOR_BIT_NUM_L_0, MOTOR_BIT_NUM_L_1
} ;

// ステッピングモータ 2相制御時相出力テーブル
static unsigned char const phase_tbl2[ MOTOR_INDEX_CNT ] =
{
    //    DMY DMY DMY MODE B2  B1  A2  A1
    BIN8(  0,  0,  0,  0,  0,  1,  0,  1  ),                // 1:A相 100% / B相 100%
    BIN8(  0,  0,  0,  0,  0,  1,  0,  1  ),                // 1:A相 100% / B相 100%
    BIN8(  0,  0,  0,  0,  0,  1,  1,  0  ),                // 2:A相-100% / B相 100%
    BIN8(  0,  0,  0,  0,  0,  1,  1,  0  ),                // 2:A相-100% / B相 100%
    BIN8(  0,  0,  0,  0,  1,  0,  1,  0  ),                // 3:A相-100% / B相-100%
    BIN8(  0,  0,  0,  0,  1,  0,  1,  0  ),                // 3:A相-100% / B相-100%
    BIN8(  0,  0,  0,  0,  1,  0,  0,  1  ),                // 4:A相 100% / B相-100%
    BIN8(  0,  0,  0,  0,  1,  0,  0,  1  )                 // 4:A相 100% / B相-100%
} ;

// ステッピングモータ 1-2相制御時相出力テーブル
static unsigned char const phase_tbl12[ MOTOR_INDEX_CNT ] =
{
    //    DMY DMY DMY MODE B2  B1  A2  A1
    BIN8(  0,  0,  0,  0,  0,  1,  0,  1  ),                // 1:A相 100% / B相 100%
    BIN8(  0,  0,  0,  0,  0,  1,  0,  0  ),                // 2:A相   0% / B相 100%
    BIN8(  0,  0,  0,  0,  0,  1,  1,  0  ),                // 3:A相-100% / B相 100%
    BIN8(  0,  0,  0,  0,  0,  0,  1,  0  ),                // 4:A相-100% / B相   0%
    BIN8(  0,  0,  0,  0,  1,  0,  1,  0  ),                // 5:A相-100% / B相-100%
    BIN8(  0,  0,  0,  0,  1,  0,  0,  0  ),                // 6:A相   0% / B相-100%
    BIN8(  0,  0,  0,  0,  1,  0,  0,  1  ),                // 7:A相 100% / B相-100%
    BIN8(  0,  0,  0,  0,  0,  0,  0,  1  )                 // 8:A相 100% / B相   0%
} ;

//==============================================================================
//  ローカル関数
//==============================================================================

//*******************************************************************************
// 関数名       ：input_motor_bit
// 説明         ：モータ出力ビット入力設定
//*******************************************************************************

static void input_motor_bit(
    DRV8835_PARAM       *p_param            // マイナー番号に応じたパラメータ構造体ポインタ
)
{
    void            *p_gpfsel ;
    unsigned char   bit ;
    unsigned char   gpio_pin, gpio_pin_l ;

    // モータ出力信号の最下位GPIO番号より、設定するGPFSELレジスタアドレスを算出
    p_gpfsel = p_param->p_gpio_reg + GPFSEL0 + GPFSEL_SIZE * ( p_param->gpio_pin_l / 10 ) ;

    // 全モータ出力ピンに対して入力設定
    bit = MOTOR_BIT_MSK ;
    gpio_pin = p_param->gpio_pin_l ;
    gpio_pin_l = p_param->gpio_pin_l ;
    while( bit != 0 )
    {
        unsigned int    gpfsel_cur, gpfsel_msk ;
        
        gpfsel_cur = ioread32( p_gpfsel ) ;
        gpfsel_msk = 0 ;

        // 全モータ出力ピンの設定完了またはGPIOピン番号の10の位が変わるまで、GPFSEL設定値を編集
        while( ( bit != 0 ) || ( gpio_pin / 10 != gpio_pin_l / 10 ) )
        {
            gpfsel_msk <<= GPFSEL_BIT_WID ;
            gpfsel_msk |= GPFSEL_BIT_MSK ;

            bit >>= 1 ;
            gpio_pin++ ;
        }

        // モータ出力ピンに対するビットをクリア（入力設定）
        gpfsel_cur &= ~( gpfsel_msk << ( ( gpio_pin_l % 10 ) * GPFSEL_BIT_WID ) ) ;
        iowrite32( gpfsel_cur, p_gpfsel ) ;

        // 次のGPFSELレジスタ設定の準備
        gpio_pin_l = 0 ;
        p_gpfsel += GPFSEL_SIZE ;
    }
}

//*******************************************************************************
// 関数名       ：output_motor_bit
// 説明         ：モータ出力ビット出力設定
//*******************************************************************************

static void output_motor_bit(
    DRV8835_PARAM       *p_param            // マイナー番号に応じたパラメータ構造体ポインタ
)
{
    void            *p_gpfsel ;
    unsigned char   bit ;
    unsigned char   gpio_pin, gpio_pin_l ;

    // モータ出力信号の最下位GPIO番号より、設定するGPFSELレジスタアドレスを算出
    p_gpfsel = p_param->p_gpio_reg + GPFSEL0 + GPFSEL_SIZE * ( p_param->gpio_pin_l / 10 ) ;

    // 全モータ出力ピンに対して出力設定
    bit = MOTOR_BIT_MSK ;
    gpio_pin = p_param->gpio_pin_l ;
    gpio_pin_l = p_param->gpio_pin_l ;
    while( bit != 0 )
    {
        unsigned int    gpfsel_cur, gpfsel_msk, gpfsel_set ;
        
        gpfsel_cur = ioread32( p_gpfsel ) ;
        gpfsel_msk = 0 ;
        gpfsel_set = 0 ;

        // 全モータ出力ピンの設定完了またはGPIOピン番号の10の位が変わるまで、GPFSEL設定値を編集
        while( ( bit != 0 ) || ( gpio_pin / 10 != gpio_pin_l / 10 ) )
        {
            // ビットクリア用のマスク編集
            gpfsel_msk <<= GPFSEL_BIT_WID ;
            gpfsel_msk |= GPFSEL_BIT_MSK ;

            // 出力設定用の設定値編集
            gpfsel_set <<= GPFSEL_BIT_WID ;
            gpfsel_set |= GPFSEL_OUTPUT ;

            bit >>= 1 ;
            gpio_pin++ ;
        }

        // モータ出力ピンに対するビットをクリア
        gpfsel_cur &= ~( gpfsel_msk << ( ( gpio_pin_l % 10 ) * GPFSEL_BIT_WID ) ) ;

        // 当該ビットを出力設定に変更
        gpfsel_cur |= gpfsel_set << ( ( gpio_pin_l % 10 ) * GPFSEL_BIT_WID ) ;
        iowrite32( gpfsel_cur, p_gpfsel ) ;

        // 次のGPFSELレジスタ設定の準備
        gpio_pin_l = 0 ;
        p_gpfsel += GPFSEL_SIZE ;
    }
}

//*******************************************************************************
// 関数名       ：output_motor
// 説明         ：ステッピングモータ出力
//*******************************************************************************

static void output_motor(
    DRV8835_PARAM       *p_param            // マイナー番号に応じたパラメータ構造体ポインタ
)
{
    unsigned char const     *p_phase_tbl ;
    
    if( p_param->context.phase_type == DRV8835_PHASE_TYPE_2 )   // 2相制御？
    {
        p_phase_tbl = phase_tbl2 ;                              // 2相制御時の相出力テーブルを読み出す
    }
    else                                                        // 1-2相制御？
    {
        p_phase_tbl = phase_tbl12 ;                             // 1-2相制御時の相出力テーブルを読み出す
    }

    // High出力
    iowrite32( p_phase_tbl[ p_param->phase_idx ] << p_param->gpio_pin_l,
               p_param->p_gpio_reg + GPSET0 ) ;

    // Low出力
    iowrite32( ( ~p_phase_tbl[ p_param->phase_idx ] & MOTOR_BIT_MSK ) << p_param->gpio_pin_l,
               p_param->p_gpio_reg + GPCLR0 ) ;
}

//*******************************************************************************
// 関数名       ：update_phase_index
// 説明         ：相制御インデックス更新
//*******************************************************************************

static void update_phase_index(
    DRV8835_PARAM       *p_param,           // マイナー番号に応じたパラメータ構造体ポインタ
    unsigned char       cw_ccw              // 回転方向
)
{
    if( p_param->context.phase_type == DRV8835_PHASE_TYPE_2 )   // 2相励磁制御？
    {
        if( cw_ccw == MOTOR_ROT_CW )                            // 正転？
        {
            p_param->phase_idx += 2 ;
        }
        else                                                    // 逆転？
        {
            p_param->phase_idx -= 2 ;
        }
    }
    else                                                        // 1-2相励磁制御？
    {
        if( cw_ccw == MOTOR_ROT_CW )                            // 正転？
        {
            p_param->phase_idx++ ;
        }
        else                                                    // 逆転？
        {
            p_param->phase_idx-- ;
        }
    }

    p_param->phase_idx &= ( MOTOR_INDEX_CNT - 1 ) ;
}

//*******************************************************************************
// 関数名       ：excitation_off
// 説明         ：ステッピングモータの励磁を切る
//*******************************************************************************

static void excitation_off(
    DRV8835_PARAM       *p_param            // マイナー番号に応じたパラメータ構造体ポインタ
)
{
    iowrite32( MOTOR_BIT_MSK << p_param->gpio_pin_l, p_param->p_gpio_reg + GPCLR0 ) ;
}

//==============================================================================
//  システムハンドラ
//==============================================================================

//*******************************************************************************
// 関数名       ：drv8835_open
// 説明         ：openハンドラ
//*******************************************************************************

static int drv8835_open(                    // 戻り値：0=処理成功
    struct inode    *inode,                 // inode情報構造体ポインタ
    struct file     *file                   // file構造体ポインタ
)
{
    int             minor ;
    DRV8835_PARAM   *p_param ;

    // マイナー番号とパラメータ構造体アドレス取得
    minor = iminor( inode ) ;
    if( minor >= MINOR_CNT )
    {
        return -EFAULT ;
    }
    p_param = &param[ minor ] ;
    file->private_data = p_param ;
    p_param->minor = minor ;

    // DRV8835用コンテキスト初期化
    p_param->context.phase_type = DRV8835_PHASE_TYPE_2 ;
    p_param->context.start_excite_tim = MOTOR_START_EXCITE_TIM_DEF ;
    p_param->context.motor_time = MOTOR_ROT_TIM_DEF ;
    p_param->context.stop_excite_tim = MOTOR_STOP_EXCITE_TIM_DEF ;

    // モータ制御関連変数初期化
    p_param->gpio_pin_l = gpio_pin_l_tbl[ minor ] ;
    p_param->phase_idx = 0 ;
    p_param->p_gpio_reg = ioremap_nocache( REG_ADDR_GPIO_BASE, sizeof(unsigned int) ) ;

    // モータ出力ピンを出力に設定する
    output_motor_bit( p_param ) ;

    return 0 ;
}

//*******************************************************************************
// 関数名       ：drv8835_close
// 説明         ：closeハンドラ
//*******************************************************************************

static int drv8835_close(
    struct inode    *inode,                 // inode情報構造体ポインタ
    struct file     *file                   // file構造体ポインタ
)
{
    int             minor ;
    DRV8835_PARAM   *p_param ;

    // マイナー番号とパラメータ構造体アドレス取得
    minor = iminor( inode ) ;
    if( minor >= MINOR_CNT )
    {
        return -EFAULT ;
    }
    p_param = &param[ minor ] ;

    // モータの励磁を切る
    excitation_off( p_param ) ;

    // モータ出力ピンを入力に設定する
    input_motor_bit( p_param ) ;

    // GPIOレジスタ解放
    iounmap( p_param->p_gpio_reg ) ;

    return 0 ;
}

//*******************************************************************************
// 関数名       ：drv8835_ioctl
// 説明         ：ioctlハンドラ
//*******************************************************************************

static long drv8835_ioctl(
    struct file     *filp,                  // open時に作成したfile構造体ポインタ
    unsigned int    cmd,                    // ioctl用コマンド
    unsigned long   arg                     // ユーザ空間へのポインタ（設定パラメータR/W先）
)
{
    DRV8835_PARAM       *p_param ;

    p_param = (DRV8835_PARAM *)filp->private_data ;

    switch( cmd )
    {
        case DRV8835_SET_VALUES :           // 値設定？
            if( copy_from_user( &p_param->context, (void __user *)arg, sizeof(p_param->context) ) )
            {
                return -EFAULT ;
            }
            break ;

        case DRV8835_GET_VALUES :           // 値取得？
            if( copy_to_user( (void __user *)arg, &p_param->context, sizeof(p_param->context) ) )
            {
                return -EFAULT ;
            }
            break ;

        default :
            printk( KERN_WARNING "unsupported command %d\n", cmd ) ;
            return -EFAULT ;
    }

    return 0 ;
}

//*******************************************************************************
// 関数名       ：drv8835_write
// 説明         ：writeハンドラ
//*******************************************************************************

static ssize_t drv8835_write(               // 戻り値：0以上=書き込みデータ数、負の数=エラー
    struct file         *filp,              // open時に作成したfile構造体ポインタ
    const char __user   *buf,               // ユーザ空間でのデータ格納ポインタ
    size_t              count,              // bufのサイズ
    loff_t              *f_pos              // オフセット
)
{
    DRV8835_PARAM       *p_param ;
    char                mode ;

    p_param = (DRV8835_PARAM *)filp->private_data ;

    // ユーザ空間のアドレスのデータを書き込み
    if( copy_from_user( &mode, buf, 1 ) )
    {
        return -EFAULT ;
    }

    // モード別処理
    if( mode == DRV8835_MODE_EXCITE_ON )                // 励磁ON？
    {
        output_motor( p_param ) ;
        msleep( p_param->context.start_excite_tim ) ;   // 起動時ホールドを入れる
    }
    else                                                // 励磁OFF？
    {
        excitation_off( p_param ) ;
    }

    return 1 ;                                          // 必ず1を返す
}

//*******************************************************************************
// 関数名       ：drv8835_llseek
// 説明         ：llseekハンドラ
//*******************************************************************************

static loff_t drv8835_llseek(               // 戻り値：0以上=移動後のオフセット位置、負の数=エラー
    struct file         *filp,              // open時に作成したfile構造体ポインタ
    loff_t              off,                // オフセット
    int                 whence              // 位置指定方法
)
{
    DRV8835_PARAM       *p_param ;
    unsigned char       cw_ccw ;

    p_param = (DRV8835_PARAM *)filp->private_data ;

    // 回転方向判別
    if( off < 0 )
    {
        cw_ccw = MOTOR_ROT_CCW ;
        off *= -1 ;
    }
    else
    {
        cw_ccw = MOTOR_ROT_CW ;
    }
    
    // モータ回転処理
    while( off-- )
    {
        // 相出力
        output_motor( p_param ) ;

        // 相出力インデックス更新
        update_phase_index( p_param, cw_ccw ) ;

        // モータ速度制御/停止処理
        if( off == 1 )                                  // 今回でモータ停止？
        {
            msleep( p_param->context.stop_excite_tim ) ;
        }
        else                                            // モータ回転継続？
        {
            msleep( p_param->context.motor_time ) ;     // 停止時ホールドを入れる
        }
    }

	return 0 ;
}

//*******************************************************************************
// 関数名       ：drv8835_fops
// 説明         ：各種システムコールに対応するハンドラテーブル
//*******************************************************************************

struct file_operations drv8835_fops =
{
    .open           = drv8835_open,
    .release        = drv8835_close,
    .unlocked_ioctl = drv8835_ioctl,            // 64bits向け
    .compat_ioctl   = drv8835_ioctl,            // 32bits向け
    .write          = drv8835_write,
    .llseek         = drv8835_llseek,
} ;

//*******************************************************************************
// 関数名       ：drv8835_init
// 説明         ：モジュール初期化処理
//*******************************************************************************

static int drv8835_init(
    void
)
{
    dev_t   dev ;
    int     alloc_ret ;
    int     cdev_err ;
    int     minor ;

    printk( KERN_INFO "%s: Initialize module.\n", DRV_NAME ) ;

    // ドライバの登録（動的な方法）
    // 1. メジャー番号の動的確保
    alloc_ret = alloc_chrdev_region( &dev, MINOR_BASE, MINOR_CNT, DRV_NAME );
    if( alloc_ret != 0 )
    {
        printk( KERN_ERR "%s: alloc_chrdev_region = %d\n", DRV_NAME, alloc_ret );
        return -1;
    }
    dev_major = MAJOR( dev ) ;
    printk( KERN_INFO "%s: dev_major = %d\n", DRV_NAME, dev_major ) ;

    // 2. システムコールハンドラの登録
    cdev_init( &my_cdev, &drv8835_fops ) ;
    my_cdev.owner = THIS_MODULE ;

    // 3. カーネルへドライバの登録
    cdev_err = cdev_add( &my_cdev, dev, MINOR_CNT ) ;
    if( cdev_err != 0 )
    {
        printk( KERN_ERR "%s: cdev_add = %d\n", DRV_NAME, cdev_err ) ;
        unregister_chrdev_region( dev, MINOR_CNT ) ;
        return -1 ;
    }

    // 4. ドライバのクラス登録(/sys/class/クラス名) 
    mydev_class = class_create( THIS_MODULE, DRV_NAME ) ;
    if( IS_ERR( mydev_class ) )
    {
        printk( KERN_ERR "%s: class_create\n", DRV_NAME ) ;
        cdev_del( &my_cdev ) ;
        unregister_chrdev_region( dev, MINOR_CNT ) ;
        return -1 ;
    }

    // 5. デバイスファイル生成（/sys/class/クラス名/クラス名*）
    for ( minor = MINOR_BASE; minor < MINOR_BASE + MINOR_CNT; minor++ )
    {
        device_create( mydev_class, NULL, MKDEV( dev_major, minor ), NULL, "%s%d", DRV_NAME, minor ) ;
    }

    return 0 ;
}

//*******************************************************************************
// 関数名       ：drv8835_exit
// 説明         ：モジュール終了処理
//*******************************************************************************

static void drv8835_exit(
    void
)
{
    dev_t   dev ;
    int     minor ;

    // ドライバの削除（動的な方法）
    // 1. /sys/class/クラス名/クラス名*削除
    for( minor = MINOR_BASE; minor < MINOR_BASE + MINOR_CNT; minor++ )
    {
        device_destroy( mydev_class, MKDEV( dev_major, minor ) ) ;
    }

    // 2. デバイスのクラス削除（/sys/class/クラス名)
    class_destroy( mydev_class ) ;

    // 3. カーネルからドライバを取り外す
    cdev_del( &my_cdev ) ;

    // 4. メジャー番号の削除
    dev = MKDEV( dev_major, MINOR_BASE ) ;
    unregister_chrdev_region( dev, MINOR_CNT ) ;

    printk( KERN_INFO "%s: Exit module.\n", DRV_NAME ) ;
}

module_init( drv8835_init ) ;
module_exit( drv8835_exit ) ;
