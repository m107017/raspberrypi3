//******************************************************************************/
//
// ファイル名   ：main.c
// 説明         ：メイン処理
//
//******************************************************************************/

#include <stdio.h>              // perror
#include <stdlib.h>             // EXIT_***
#include <fcntl.h>              // open
#include <unistd.h>             // close
#include <string.h>             // memset
#include <sys/ioctl.h>          // ioctl

#include "../drv8835.h"

//#define DRV8835_DEV_FILE_NAME   "/dev/drv88350"
#define DRV8835_DEV_FILE_NAME   "/dev/drv88351"

//==============================================================================
//  main関数
//==============================================================================

//*******************************************************************************
// 関数名       ：main
// 説明         ：メイン処理
//*******************************************************************************
int main( int argc, char **argv )
{
    struct drv8835_context context ;
    int     fd ;
    char    mode ;

    // DRV8835用コンテキスト初期化
    context.phase_type = DRV8835_PHASE_TYPE_2 ;
    context.start_excite_tim = 10 ;
    context.motor_time = 5 ;
    context.stop_excite_tim = 10 ;

    // デバイスドライバオープン
    fd = open( DRV8835_DEV_FILE_NAME, O_RDWR ) ;
    if( fd < 0 )
    {
        perror( "Test Driver Open" ) ;
        return fd ;
    }

    // DRV8835用コンテキスト変更
    if( ioctl( fd, DRV8835_SET_VALUES, &context ) < 0 )
    {
        perror( "ioctl error" ) ;
    }

    // ステッピングモータ励磁ON
    mode = DRV8835_MODE_EXCITE_ON ;
    write( fd, &mode, sizeof(mode) ) ;

    // ステッピングモータ100step正転
    lseek( fd, 100, SEEK_SET ) ;

    // DRV8835用コンテキスト変更（1-2相励磁制御に変更）
    context.phase_type = DRV8835_PHASE_TYPE_12 ;
    if( ioctl( fd, DRV8835_SET_VALUES, &context ) < 0 )
    {
        perror( "ioctl error" ) ;
    }

    // ステッピングモータ100step逆転
    lseek( fd, -100, SEEK_SET ) ;

    // ステッピングモータ励磁OFF
    mode = DRV8835_MODE_EXCITE_OFF ;
    write( fd, &mode, sizeof(mode) ) ;

    // デバイスドライバクローズ
    close( fd ) ;

    return EXIT_SUCCESS ;
}
