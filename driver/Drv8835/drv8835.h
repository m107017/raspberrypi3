//*******************************************************************************
//
// ファイル名   ：drv8835.h
// 説明         ：ステッピングモータドライバDRV8835用デバイスドライバ
//
//*******************************************************************************

#ifndef     DRV8835_H_INCLUDED
#define     DRV8835_H_INCLUDED

#include <linux/ioctl.h>

//==============================================================================
//  定数
//==============================================================================

// writeハンドラで書き込む値（動作モード）
#define DRV8835_MODE_EXCITE_OFF     0               // 励磁OFF
#define DRV8835_MODE_EXCITE_ON      1               // 励磁ON

// 相制御タイプ
#define DRV8835_PHASE_TYPE_2        0               // 2相励磁制御
#define DRV8835_PHASE_TYPE_12       1               // 1-2相励磁制御

// ioctl用コマンド(request, 第2引数)の定義
#define DRV8835_IOC_TYPE            'M'

// デバイスドライバに値を設定するコマンド
#define DRV8835_SET_VALUES          _IOW( DRV8835_IOC_TYPE, 1, struct drv8835_context )

// デバイスドライバの値を取得するコマンド
#define DRV8835_GET_VALUES          _IOR( DRV8835_IOC_TYPE, 2, struct drv8835_context )


//==============================================================================
//  構造体
//==============================================================================

// DRV8835用コンテキスト（ioctl用パラメータ 第3引数）
struct drv8835_context
{
    unsigned char       phase_type ;                // 相制御タイプ
    unsigned int        start_excite_tim ;          // 起動時ホールド時間[ms]
    unsigned int        motor_time ;                // 相切り替え時間[ms]
    unsigned int        stop_excite_tim ;           // 停止時ホールド時間[ms]
} ;

#endif          // DRV8835_H_INCLUDED